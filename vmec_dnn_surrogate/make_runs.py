"""
Python script to generate VMEC batch for training data.

Warning: this method to generate VMEC runs is deprecated, and the
Minerva framework should be used instead. Please refer to the project
README.md file to learn more about it.

Examples:
    $ python vmec_dnn_surrogate/make_runs.py --samples=1000
"""

import os
import logging
import argparse

import numpy as np
from sklearn.gaussian_process.kernels import RBF, ConstantKernel

import w7x

#  Default config
DATA_PATH = "data/"
SAMPLES = 1000

LENGTH_SCALE = 0.3
SCALING_VALUE = 5
CONSTANT_VALUE = SCALING_VALUE * 1e-3
current_profile_gp = w7x.batch.GaussianProcess(  # pylint: disable=invalid-name
    [0, 1],
    filters=[
        lambda x: (x >= 0).all(),  # Lower lim at 0
        lambda x: (x <= 1).all(),  # Upper lim at 1
        lambda x: ((x - np.append(x[1:], 0)) >= 0).all(),  # Monotonic
    ],
    kernel=SCALING_VALUE ** 2
    * RBF(length_scale=LENGTH_SCALE, length_scale_bounds="fixed")
    + ConstantKernel(constant_value=CONSTANT_VALUE, constant_value_bounds="fixed"),
)

#  Dataset distribution space
SPACE_FINITE_BETA = {
    "magnetic_config": (
        w7x.batch.Distribution(
            np.random.uniform,
            [13068 * i for i in [1.0, 0.6, 0.6, 0.6, 0.6, -1.0, -1.0, 0.0, 0.0]],
            [13068 * i for i in [1.0, 1.3, 1.3, 1.3, 1.3, 1.0, 1.0, 0.0, 0.0]],
        ),
        "A",
    ),
    "phiedge": w7x.batch.Distribution(np.random.uniform, -2.5, -1.6),
    "curtor": w7x.batch.Distribution(np.random.uniform, -5e4, 5e4),
    "current_profile": (
        w7x.batch.Distribution(current_profile_gp),
        "cubic_spline_i",
    ),
    "pressure_profile": (
        w7x.batch.Distribution(np.random.uniform, [1e-6, 2, 2], [2e5, 10, 20]),
        "two_power",
    ),
}

SPACE_NULL_BETA = {
    "magnetic_config": (
        w7x.batch.Distribution(
            np.random.uniform,
            [13068 * i for i in [1.0, 0.6, 0.6, 0.6, 0.6, -1.0, -1.0, 0.0, 0.0]],
            [13068 * i for i in [1.0, 1.3, 1.3, 1.3, 1.3, 1.0, 1.0, 0.0, 0.0]],
        ),
        "A",
    ),
    "phiedge": w7x.batch.Distribution(np.random.uniform, -2.5, -1.6),
    "curtor": 0,
    "current_profile": ([0, 0, 0, 0, 0], "sum_atan"),
    "pressure_profile": ([1e-6, -1e-6], "power_series"),
}

SPACE = {"null": SPACE_NULL_BETA, "finite": SPACE_FINITE_BETA}

#  VMEC runtime configuration
NITER = 80000


def make_runs(folder, size, config):
    """Set batch distribution and generate files."""

    logger = logging.getLogger()

    space = SPACE[config]

    #  Fix input parameters
    batch = w7x.batch.Vmec(folder=folder, cached=False)
    batch.size = size
    batch.magnetic_config = space["magnetic_config"]
    batch.pressure_profile = space["pressure_profile"]
    batch.current_profile = space["current_profile"]
    batch.phiedge = space["phiedge"]
    batch.curtor = space["curtor"]
    batch.niter = NITER

    batch.gen()
    logger.info("Batch %s has been builded", batch.batch_id)

    return batch.batch_id


def start(batch_id, folder):
    """Start generated batch."""

    logger = logging.getLogger()

    logger.info("Batch %s has been found", batch_id)
    logger.info("About to start runs...")

    batch = w7x.batch.Vmec(batch_id, folder=folder)
    batch.start()


def main():
    """Main function of the make_runs.py script."""

    #  Silent logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    parser = argparse.ArgumentParser(description="VMEC runs generation.")
    parser.add_argument(
        "-v", "--verbose", help="increase output verbosity", action="store_true"
    )
    parser.add_argument(
        "-b", "--batch", default=None, help="batch id to retrieve", type=str
    )
    parser.add_argument(
        "-c",
        "--config",
        default="null",
        help="beta configuration for dataset",
        type=str,
    )
    parser.add_argument(
        "-s",
        "--samples",
        default=SAMPLES,
        help="samples of training dataset",
        type=int,
    )
    args = parser.parse_args()

    logging.info("Generate VMEC training set...")

    #  Set values
    samples = args.samples
    batch = args.batch

    if samples == 0:
        raise RuntimeError("Number of runs per batch could not be zero")

    if batch is not None:
        batch_path = DATA_PATH + batch + ".pkl"
        if not os.path.isfile(batch_path):
            raise ValueError("{0} has not been found".format(batch))
    else:
        batch = make_runs(DATA_PATH, samples, args.config)

    logger.info("Start batch execution ...")
    start(batch, DATA_PATH)


if __name__ == "__main__":
    main()
