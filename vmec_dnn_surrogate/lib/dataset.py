"""Data reader for vmec runs."""

from os import listdir
from os.path import join, isfile
from netCDF4 import Dataset
import numpy as np
from sklearn.model_selection import train_test_split, KFold, RepeatedKFold
from sklearn.utils import resample
import tensorflow as tf
from w7x.vmec import FourierCoefficients
from vmec_dnn_surrogate.lib.config import (
    FIELD_COMPONENTS,
    NUM_OF_RADIAL_POINTS,
    NUM_OF_FOURIER_MODES,
    NUM_OF_PROFILE_USED_POINTS,
    FOURIER_SHAPE,
    NUM_OF_NYQUIST_FOURIER_COEFFS,
    NUM_OF_NYQUIST_FOURIER_MODES,
    NYQUIST_FOURIER_SHAPE,
)
from vmec_dnn_surrogate.lib.utils import read_file, get_num_fourier_modes_from_label


def string_mode(mode):
    """Return string associated with tensorflow estimator ModeKeys.

    Args:
        mode (tf.estimator.ModeKeys): The estimator ModeKeys.

    Returns:
        The associated string.
    """

    if mode == tf.estimator.ModeKeys.TRAIN:
        return "train"

    if mode == tf.estimator.ModeKeys.EVAL:
        return "val"

    if mode == tf.estimator.ModeKeys.PREDICT:
        return "test"

    raise ValueError("Given mode %s is not supported" % mode)


def get_run_files(folders):
    """Read the list of all vmec output files in `folders`.

    We expect to find vmec run folders inside the top `folders`.
    Only 'wout_*.nc' files are used to retrieve run's data.

    Args:
        folders (list): List of top folders where to read from.

    Returns:
        The list of discovered vmec output files.
    """

    return np.array(
        [
            join(folder, f, "wout_{}.nc".format(f))
            for folder in folders
            for f in listdir(folder)
            if isfile(join(folder, f, "wout_{}.nc".format(f)))
        ]
    )


def gen_runs(file_paths, variable_readers):
    """Yield vmec runs from `file_paths`.

    Args:
        file_paths (list): List of file paths to read.
        variable_readers (dict): A dictionary on how to read variables.

    Returns:
        A dictionary of read variables.
    """

    for file_id, file_path in enumerate(file_paths):

        run = Dataset(file_path, mode="r")
        run_variables = run.variables

        valid = True
        values = {}

        for var, reader in variable_readers.items():

            #  Check for missing keys
            if reader[0] not in run_variables:
                valid = False
                break

            value = reader[1](run[reader[0]])

            #  Check for masked arrays
            if np.ma.is_masked(value):
                valid = False
                break

            #  Check boundaries
            if reader[4] is not None:
                if (value <= reader[4][0]).any() or (value >= reader[4][1]).any():
                    valid = False
                    break

            #  Include variable
            if reader[5]:
                values[var] = value

        if not valid:
            continue

        values["file"] = file_id
        values["file_path"] = file_path

        yield values


def build_sample(sample, features, labels, modes=None):
    """Build a tuple of (dict(features), label) from sample.

    Sample is a dictionary of run variables, while a TensorFlow dataset
    requires data to be a tuple of (features, label).

    If modes is not None, stack field components and pad fourier coefficients
    in order to enforce a rectangular shape.

    Args:
        sample (dict): The dict of run variables.
        features (list): The list of feature variables.
        labels (list): The list of label variables.
        modes (tuple): The tuple of poloidal and toroidal modes to reshape.

    Returns:
        A tuple of (dict(features), label).
    """

    features = {f: sample[f] for f in features}

    label = tf.stack([sample[label_] for label_ in labels], axis=-1)
    if modes is not None:
        label = tf.concat(
            [tf.zeros((label.shape[0], modes[1], label.shape[2])), label], 1
        )
        label = tf.reshape(
            label, (label.shape[0], modes[0], modes[1] * 2 + 1, label.shape[2])
        )

    return (features, label)


def downsample_label_tensor(
    features,
    label,
    radial_indices=None,
    modes=None,
    orig_modes=None,
):
    """Downsample output label to given resolution parameters.

    The input label tensor is expected to have a shape as (s, m, n, j),
    where s is the radial coordinate, m the poloidal modes, n the toroidal modes,
    and j the component index.

    In case of non Fourier label, the expected shape is (s, j).

    The `modes` and `orig_modes` shoulw always be specified, otherwise
    no downsampling operation is performed.

    Args:
        features (dict): The features dict.
        label (tf.Tensor): The label tensor.
        radial_indices (array_like): Radial indices to keep.
        modes (tuple): Poloidal and toroidal Fourier modes to keep.

    Returns:
        A tuple of (dict(features), label).
    """

    #  Select Fourier modes
    if modes is not None and orig_modes is not None:
        label = label[
            :, : modes[0], orig_modes[1] - modes[1] : orig_modes[1] + modes[1] + 1, :
        ]

    #  Select radial points
    if radial_indices is not None:
        label = tf.gather(label, radial_indices, axis=0)

    return (features, label)


def apply_scale_fn_to_label(features, label, scale_fn):
    """Apply scale_fn to label.

    Args:
        features (dict): The features dict.
        lable (tf.Tensor): The label tensor.
        scale_fn (list): List of scale functions for labels.

    Returns:
        A tuple of (dict(features), label).
    """

    label = tf.stack(
        [scale_fn[i](label[..., i]) for i in range(label.shape[-1])], axis=-1
    )

    return (features, label)


def get_label_radial_indices(num_of_radial_points):
    """Get radial indices for the output labels.

    We use points spaced via a quadratic transformation. In any case we always
    include the magnetic axes, r=0, and the LCFS, r=NUM_OF_RADIAL_POINTS - 1.

    If only one radial points is requested, get LFCS.

    Args:
        num_of_radial_points (int): Number of raidal points to regress.

    Returns:
        The list of radial indices to take from the VMEC labels.
    """

    if num_of_radial_points is None or num_of_radial_points == NUM_OF_RADIAL_POINTS:
        return np.linspace(
            start=0, stop=NUM_OF_RADIAL_POINTS - 1, num=NUM_OF_RADIAL_POINTS, dtype=int
        )

    if num_of_radial_points == 1:
        return [NUM_OF_RADIAL_POINTS - 1]

    #  Use quadratic spacing
    x = list(
        map(
            lambda x: np.int(np.ceil(x)),
            np.linspace(start=0, stop=1, num=num_of_radial_points) ** 2
            * (NUM_OF_RADIAL_POINTS - 1),
        )
    )

    #  Fix duplicated indices
    for i in range(1, len(x)):
        x[i] = x[i - 1] + 1 if x[i - 1] >= x[i] else x[i]

    return x


def get_flux_points(num_of_radial_points):
    """Get the flux radial locations for the given number of radial points."""

    return np.linspace(0, 1, NUM_OF_RADIAL_POINTS)[
        get_label_radial_indices(num_of_radial_points)
    ]


def to_downsampled_shape(data, radial_points, modes=None):
    """Cast data to Fourier downsampled shape.

    Args:
        data (array-like): Data to be downsampled.
        radial_points (int): Number of radial points to retrieve.
        modes (int or tuple): Number of fourier modes to retrieve. If scalar, apply
            to both poloidal and toroidal modes.
    """

    radial_indices = get_label_radial_indices(radial_points)
    shape = data.shape

    orig_modes = NUM_OF_FOURIER_MODES
    #  Proxy for Nyquist resoulution
    if (len(shape) == 2 and shape[1] >= NUM_OF_NYQUIST_FOURIER_COEFFS) or (
        len(shape) > 2 and shape[1] >= NUM_OF_NYQUIST_FOURIER_MODES[0]
    ):
        orig_modes = NUM_OF_NYQUIST_FOURIER_MODES

    if modes is None:
        modes = orig_modes

    if isinstance(modes, int):
        modes = (modes, modes)

    if shape == FOURIER_SHAPE or shape == NYQUIST_FOURIER_SHAPE:
        #  Data does not have m=0, n<0 coefficients
        indeces = FourierCoefficients.get_indices(
            radial_indices,
            range(modes[0]),
            range(-modes[1], modes[1] + 1),
            modes=orig_modes,
        )

        return data.flatten()[indeces].reshape(len(radial_indices), -1)

    #  Data does not have Fourier components.
    if len(shape) == 1:
        return data[radial_indices].reshape(
            radial_points,
        )

    #  Data has m=0, n<0 coefficients
    return data[
        radial_indices,
        : modes[0],
        orig_modes[1] - modes[1] : orig_modes[1] + modes[1] + 1,
    ]


def _bootstrap_fn(samples, test_size, val_size, inner_cv, random_state):
    """Bootstrap partition function."""

    num_of_test_samples = int(len(samples) * test_size)
    num_of_val_samples = int(len(samples) * val_size)

    #  Adjust val size
    val_size = val_size / (1 - test_size)

    i = 0
    while True:

        test = resample(
            samples, replace=True, n_samples=num_of_test_samples, random_state=i
        )

        for j in range(inner_cv):

            train_val = [s for s in samples if s not in test]
            train, val = train_test_split(
                train_val, test_size=num_of_val_samples, random_state=j
            )

            yield {"train": train, "val": val, "test": test}


def _KFold_fn(samples, test_size, val_size, inner_cv, random_state):
    """KFold partition function."""

    outer_splits = int(1 / test_size)
    outer_kf = KFold(
        n_splits=outer_splits, shuffle=True if random_state is None else False
    )

    #  Adjust val size
    val_size = val_size / (1 - test_size)

    for train_val_ids, test_ids in outer_kf.split(samples):

        for i in range(inner_cv):

            train_ids, val_ids = train_test_split(
                train_val_ids, test_size=val_size, random_state=i
            )

            train = samples[train_ids]
            val = samples[val_ids]
            test = samples[test_ids]

            yield {"train": train, "val": val, "test": test}


def _repeated_KFold_fn(samples, test_size, val_size, inner_cv, random_state):
    """Repeated KFold partition function."""

    n_splits = int(1 / test_size)

    #  Adjust val size
    val_size = val_size / (1 - test_size)

    rkf = RepeatedKFold(
        n_splits=n_splits, n_repeats=inner_cv, random_state=random_state
    )

    for train_val_ids, test_ids in rkf.split(samples):

        train_ids, val_ids = train_test_split(train_val_ids, test_size=val_size)

        train = samples[train_ids]
        val = samples[val_ids]
        test = samples[test_ids]

        yield {"train": train, "val": val, "test": test}


def _same_fn(samples, test_size, val_size, inner_cv, random_state):
    """Same or static partition function."""

    #  Adjust val size
    val_size = val_size / (1 - test_size)

    train_val, test = train_test_split(
        samples, test_size=test_size, random_state=random_state
    )

    while True:
        for i in range(inner_cv):
            train, val = train_test_split(train_val, test_size=val_size, random_state=i)
            yield {"train": train, "val": val, "test": test}


def partition_fn(
    samples, validation_mode, test_size, val_size, inner_cv, random_state=42
):
    """Generate iterator which yields data set partitions.

    Data set partition yields samples over an outer and inner loop. This is usually
    done as in a cross-validation routine, but it is not limited to. The outer loop
    generally partitions the test from the training plus validation samples, while the
    inner loop partitions the train from the validation samples.

    Args:
        samples (list): List of all samples to partition.
        validation_mode (str): Define how we should yield data set partitions.
        test_size (float): Ratio of test samples.
        val_size (float): Ratio of validation samples.
        inner_cv (int): Number of inner partitions to yield.
        random_state (int): As in sklearn APIs.

    Yields:
        Dictionary of train, val and test samples.
    """

    if validation_mode == "bootstrap":
        return _bootstrap_fn(samples, test_size, val_size, inner_cv, random_state)

    if validation_mode == "KFold":
        return _KFold_fn(samples, test_size, val_size, inner_cv, random_state)

    if validation_mode == "repeatedKFold":
        return _repeated_KFold_fn(samples, test_size, val_size, inner_cv, random_state)

    if validation_mode == "same":
        return _same_fn(samples, test_size, val_size, inner_cv, random_state)

    raise ValueError("Validation mode %s is not supported" % validation_mode)


def two_power_fn(coef):
    """Returns a two_power profile from the given coefficients.

    Args:
        coef (list): List of profile coefficients.

    Returns:
        A callable two_power profile.
    """

    def _two_power(x):
        return coef[0] * (1 - np.array(x) ** coef[1]) ** coef[2]

    return _two_power


def sum_atan_fn(coef):
    """Returns a sum_atan profile from the given coefficients.

    Args:
        coef (list): List of profile coefficients.

    Returns:
        A callable sum_atan profile.
    """

    def _sum_atan(x):
        return coef[0] + 2 / np.pi * coef[1] * np.arctan(
            np.divide(
                coef[2] * np.array(x) ** coef[3],
                (1 - np.array(x)) ** coef[4],
            )
        )

    return _sum_atan


class VmecDataset:
    """A data reader for vmec runs."""

    def __init__(
        self,
        data,
        test_data=None,
        validation_mode="same",
        num_samples=None,
        test_size=0.1,
        val_size=0.1,
        scalers_file=None,
    ):
        """Instantiate a VmecDataset.

        Args:
            data (list): A list of folders with vmec runs.
            val_size (float): The ratio of validation runs.
            test_size (float): The ratio of test runs.
            features (list): A list of features to extract.
            label (str): The label to use.
            scalers_file (str): File path for the JSON scalers file.
        """

        if isinstance(data, str):
            data = [data]

        self._scalers_file = scalers_file
        self._data = data
        self._test_data = test_data

        self._files = get_run_files(self.data)
        if num_samples is not None and num_samples < len(self):
            self._files = np.random.choice(self.files, num_samples, replace=False)

        self._test_files = None
        if test_data is not None:
            self._test_files = get_run_files(self._test_data)

        self._validation_mode = validation_mode
        self._test_size = test_size
        self._val_size = val_size

        self._dataset = None
        self._cached_datasets = {}

        self._partition_counter = 0
        self.set_partition_fn()
        self.update_partition()

    @staticmethod
    def get_variable_readers(
        use_coil_currents_ratio=False,
        use_nyquist_resolution=True,
        pressure_profile_type=None,
        toroidal_current_profile_type=None,
    ):
        """Get variable readers dictionary on how to read vmec data.

        Args:
            use_coil_currents_ratio (bool): Use the relative coil currents ratio.
            use_nyquist_resolution (bool): Use the full nyquist Fourier resolution
                for supported labels (BCos, BSsub, Bsup).
            pressure_profile_type (str): How to read the pressure profile.
                If None, assume cubic_spline.
            toroidal_current_profile_type (str): How to read the current profile.
                If None, assume cubic_spline_i.

        Each dictionary key is a tuple of:
            name: name of the vmec variable
            read_fn: read function
            type: enforced tensorflow data type
            shape: enforced data shape
            boundaries: list of min and max values
            to_include: wheter to include the variable
        """

        coil_currents = (
            "extcur",
            lambda x: x[:7],
            tf.float32,
            (7),
            None,
            True,
        )
        if use_coil_currents_ratio:
            coil_currents = (
                "extcur",
                lambda x: x[1:7] / x[0],
                tf.float32,
                (6),
                None,
                True,
            )

        toroidal_current = (
            "ac_aux_f",
            lambda x: x[[get_label_radial_indices(NUM_OF_PROFILE_USED_POINTS)]],
            tf.float32,
            (NUM_OF_PROFILE_USED_POINTS),
            [
                np.zeros(NUM_OF_PROFILE_USED_POINTS),
                np.ones(NUM_OF_PROFILE_USED_POINTS),
            ],
            True,
        )
        if toroidal_current_profile_type == "sum_atan":
            toroidal_current = (
                "ac",
                lambda x: sum_atan_fn(coef=x[:])(
                    get_flux_points(NUM_OF_PROFILE_USED_POINTS)
                ),
                tf.float32,
                (NUM_OF_PROFILE_USED_POINTS),
                [
                    np.zeros(NUM_OF_PROFILE_USED_POINTS),
                    np.ones(NUM_OF_PROFILE_USED_POINTS),
                ],
                True,
            )

        pressure = (
            "am_aux_f",
            lambda x: x[[get_label_radial_indices(NUM_OF_PROFILE_USED_POINTS)]] / x[0],
            tf.float32,
            (NUM_OF_PROFILE_USED_POINTS),
            None,
            True,
        )
        if pressure_profile_type == "two_power":
            pressure = (
                (
                    "am",
                    lambda x: two_power_fn(coef=x[:])(
                        get_flux_points(NUM_OF_PROFILE_USED_POINTS)
                    )
                    / x[0],
                    tf.float32,
                    (NUM_OF_PROFILE_USED_POINTS),
                    None,
                    True,
                ),
            )

        return {
            "coilCurrents": coil_currents,
            "phiedge": ("phi", lambda x: x[:][-1], tf.float32, (), [-3.0, -1.0], True),
            "curtor": ("ctor", lambda x: x[0], tf.float32, (), [-1e4, 1e4], True),
            "pressure": pressure,
            "pressureScale": (
                "am_aux_f" if pressure_profile_type is None else "am",
                lambda x: x[0],
                tf.float32,
                (),
                [0.0, 3e5],
                True,
            ),
            "toroidalCurrent": toroidal_current,
            "plasmaVolume": (
                "volume_p",
                lambda x: x[:],
                tf.float32,
                (),
                [22.0, 38.0],
                False,
            ),
            "minorRadius": (
                "Aminor_p",
                lambda x: x[:],
                tf.float32,
                (),
                [0.45, 0.60],
                False,
            ),
            "iota": (
                "iotaf",
                lambda x: x[:],
                tf.float32,
                (NUM_OF_RADIAL_POINTS),
                None,
                True,
            ),
            "averageBeta": ("betatotal", lambda x: x[:], tf.float32, (), None, True),
            "axisBeta": ("betaxis", lambda x: x[:], tf.float32, (), None, True),
            "B0": ("b0", lambda x: x[:], tf.float32, (), None, True),
            "BCos": (
                "bmnc",
                lambda x: x[:],
                tf.float32,
                NYQUIST_FOURIER_SHAPE if use_nyquist_resolution else FOURIER_SHAPE,
                None,
                True,
            ),
            "RCos": ("rmnc", lambda x: x[:], tf.float32, FOURIER_SHAPE, None, True),
            "LambdaSin": (
                "lmns",
                lambda x: x[:],
                tf.float32,
                FOURIER_SHAPE,
                None,
                True,
            ),
            "ZSin": ("zmns", lambda x: x[:], tf.float32, FOURIER_SHAPE, None, True),
            "BsupUCos": (
                "bsupumnc",
                lambda x: x[:],
                tf.float32,
                NYQUIST_FOURIER_SHAPE if use_nyquist_resolution else FOURIER_SHAPE,
                None,
                True,
            ),
            "BsupVCos": (
                "bsupvmnc",
                lambda x: x[:],
                tf.float32,
                NYQUIST_FOURIER_SHAPE if use_nyquist_resolution else FOURIER_SHAPE,
                None,
                True,
            ),
            "BsubSSin": (
                "bsubsmns",
                lambda x: x[:],
                tf.float32,
                NYQUIST_FOURIER_SHAPE if use_nyquist_resolution else FOURIER_SHAPE,
                None,
                True,
            ),
            "BsubUCos": (
                "bsubumnc",
                lambda x: x[:],
                tf.float32,
                NYQUIST_FOURIER_SHAPE if use_nyquist_resolution else FOURIER_SHAPE,
                None,
                True,
            ),
            "BsubVCos": (
                "bsubvmnc",
                lambda x: x[:],
                tf.float32,
                NYQUIST_FOURIER_SHAPE if use_nyquist_resolution else FOURIER_SHAPE,
                None,
                True,
            ),
        }

    @property
    def partition(self):
        """Dictionary of train, val and test runs."""

        return self._partition

    @property
    def data(self):
        """List of all data set run folders."""

        return self._data

    @property
    def files(self):
        """List of all data set run files."""

        return self._files

    @property
    def test_files(self):
        """List of all test set run files."""

        return self._test_files

    @property
    def all_files(self):
        """List of all run files associated with this data set."""

        if self._test_files is not None:
            return np.concatenate((self._files, self._test_files))

        return self._files

    def __len__(self):
        """Number of runs in data set."""

        return len(self.files)

    def _clear_cache(self):
        """Clear cached dataset."""

        self._cached_datasets["train"] = None
        self._cached_datasets["val"] = None
        self._cached_datasets["test"] = None

    def set_partition_fn(self, inner_iterations=5):
        """Set partition_fn generator.

        Args:
            inner_iterations (int): Number of inner cross-validation iterations.
        """

        self._partition_fn = partition_fn(
            self.files,
            self._validation_mode,
            self._test_size,
            self._val_size,
            inner_iterations,
        )

        self._clear_cache()
        self._partition_counter = 0

    def update_partition(self):
        """Update data set partition based on validation mode."""

        self._partition = next(self._partition_fn)

        if self._test_files is not None:
            self._partition["test"] = self._test_files

        assert (
            set(self.partition["train"]) & set(self.partition["val"])
        ) == set(), "Train and val partitions intersection is not empty!"

        assert (
            set(self.partition["train"]) & set(self.partition["test"])
        ) == set(), "Train and test partitions intersection is not empty!"

        assert (
            set(self.partition["val"]) & set(self.partition["test"])
        ) == set(), "Val and test partitions intersection is not empty!"

        self._clear_cache()
        self._partition_counter += 1

    def get_scaler_fn(self, variable, mode, radial_points=None, fourier_modes=None):
        """Get scaler function for given dataset variable.

        Args:
            mode (str): Scaling or rescaling mode.
            variable (str): Variable for which retrieve the scaler function.
            radial_points (int): Number of radial points to retrieve.
            fourier_modes (tuple): Higher fourier mode to retrieve.

        Returns:
            The scaler function for the given variable.
        """

        if self._scalers_file is None:
            return lambda x: x

        if not isfile(self._scalers_file):
            return lambda x: x

        scalers = read_file(self._scalers_file)

        if variable in scalers.keys():

            parameters = tuple(
                map(lambda x: np.array(x, dtype=np.float32), scalers[variable]["data"])
            )

            #  Adapt scaler parameters shape to data
            #  Do not downsample data in case of PCA since we use the whole Fourier space.
            if scalers[variable]["name"] != "PCA":
                if radial_points is not None or fourier_modes is not None:
                    parameters = tuple(
                        map(
                            lambda x: to_downsampled_shape(
                                x, radial_points, fourier_modes
                            ),
                            parameters,
                        )
                    )

            if mode == "scale":

                if scalers[variable]["name"] == "StandardScaler":
                    return lambda x: tf.math.divide_no_nan(
                        tf.subtract(x, parameters[0]), parameters[1]
                    )

                if scalers[variable]["name"] == "RobustScaler":
                    return lambda x: tf.math.divide_no_nan(
                        tf.subtract(x, parameters[0]), parameters[1]
                    )

                if scalers[variable]["name"] == "MinMaxScaler":
                    #  Use feature_range = (-1, 1) by default
                    return (
                        lambda x: tf.math.divide_no_nan(
                            tf.subtract(x, parameters[0]),
                            tf.subtract(parameters[1], parameters[0]),
                        )
                        * 2.0
                        - 1.0
                    )

                if scalers[variable]["name"] == "BoxCox":
                    return lambda x: tf.math.divide_no_nan(
                        tf.math.subtract(tf.math.pow(x, parameters[0]), 1),
                        parameters[0],
                    )

                if scalers[variable]["name"] == "PCA":
                    _n_components = parameters[1].shape[0]
                    return lambda x: tf.tensordot(
                        tf.reshape(tf.math.subtract(x, parameters[0]), (1, -1)),
                        tf.reshape(tf.transpose(parameters[1]), (-1, _n_components)),
                        axes=1,
                    )

                if scalers[variable]["name"] == "MinMaxBoxCox":
                    return lambda x: tf.math.divide_no_nan(
                        tf.math.subtract(
                            tf.math.pow(
                                parameters[3]
                                + (parameters[4] - parameters[3])
                                * tf.math.divide_no_nan(
                                    tf.subtract(x, parameters[0]),
                                    tf.subtract(parameters[1], parameters[0]),
                                ),
                                parameters[2],
                            ),
                            1,
                        ),
                        parameters[2],
                    )

                raise ValueError(
                    "{} is not supported".format(scalers[variable]["name"])
                )

            if mode == "rescale":

                if scalers[variable]["name"] == "StandardScaler":
                    return lambda x: tf.math.add(
                        tf.math.multiply(x, parameters[1]), parameters[0]
                    )

                if scalers[variable]["name"] == "RobustScaler":
                    return lambda x: tf.math.add(
                        tf.math.multiply(x, parameters[1]), parameters[0]
                    )

                if scalers[variable]["name"] == "MinMaxScaler":
                    #  Use feature_range = (-1, 1) by default
                    return lambda x: tf.math.add(
                        tf.math.multiply(
                            (x + 1.0) / 2.0, tf.subtract(parameters[1], parameters[0])
                        ),
                        parameters[0],
                    )

                if scalers[variable]["name"] == "BoxCox":
                    return lambda x: tf.math.pow(
                        tf.math.add(tf.math.multiply(x, parameters[0]), 1),
                        tf.math.divide(1, parameters[0]),
                    )

                if scalers[variable]["name"] == "PCA":
                    _n_components = parameters[1].shape[0]
                    return lambda x: tf.math.add(
                        parameters[0],
                        tf.reshape(
                            tf.tensordot(
                                tf.reshape(x, (-1, _n_components)),
                                tf.reshape(parameters[1], (_n_components, -1)),
                                axes=1,
                            ),
                            parameters[0].shape,
                        ),
                    )

                if scalers[variable]["name"] == "MinMaxBoxCox":
                    return lambda x: tf.math.add(
                        tf.math.multiply(
                            (
                                tf.math.pow(
                                    tf.math.add(tf.math.multiply(x, parameters[2]), 1),
                                    tf.math.divide(1.0, parameters[2]),
                                )
                                - parameters[3]
                            )
                            / (parameters[4] - parameters[3]),
                            tf.subtract(parameters[1], parameters[0]),
                        ),
                        parameters[0],
                    )

                raise ValueError(
                    "{} is not supported".format(scalers[variable]["name"])
                )

            raise ValueError("{} mode is not supported".format(mode))

        return lambda x: x

    def get_feature_columns(
        self, features, use_coil_currents_ratio=False, use_nyquist_resolution=True
    ):
        """Extract feature columns.

        Args:
            features (list): The list of features to use.

        Returns:
            The list of feature columns.
        """

        readers = self.get_variable_readers(
            use_coil_currents_ratio, use_nyquist_resolution
        )

        feature_columns = []
        for feature in features:

            scale_fn = self.get_scaler_fn(feature, mode="scale")

            feature_columns.append(
                tf.feature_column.numeric_column(
                    feature, shape=readers[feature][3], normalizer_fn=scale_fn
                )
            )

        return feature_columns

    def input_fn(
        self,
        mode=[
            tf.estimator.ModeKeys.TRAIN,
            tf.estimator.ModeKeys.EVAL,
            tf.estimator.ModeKeys.PREDICT,
        ],
        shuffle=False,
        batch_size=32,
        features=None,
        label=None,
        metadata=None,
        radial_points=None,
        fourier_modes=None,
        scale_label=False,
        flatten=False,
        use_coil_currents_ratio=False,
        use_nyquist_resolution=True,
        pressure_profile_type=None,
        toroidal_current_profile_type=None,
        return_raw_runs=False,
        return_X_y=False,
        take_samples=-1,
    ):
        """Load a tf.data.Dataset from the current `VmecDataset`.

        Args:
            mode (tf.estimator.ModeKeys or list): The execution mode as defined in
                tf.estimator.ModeKeys.
            shuffle (bool): Shuffle dataset.
            batch_size (int): Batch size for the dataset.
            features (list): List of features.
            label (str): Label.
            radial_points (int): Number of radial points to retrieve.
            fourier_modes (int or tuple): Poloidal and toroidal Fourier mode to retrieve.
            scale_label (bool): Scale label.
            flatten (bool): Retrieve flatten view of the fourier coefficients.
            use_coil_currents_ratio (bool): Use ratio of coilCurrents with
                respect to i1.
            use_nyquist_resolution (bool): Use nyquist resolution for the supported labels.
            pressure_profile_type (str): How to read the pressure profile.
                If None, assume cubic_spline.
            toroidal_current_profile_type (str): How to read the current profile.
                If None, assume cubic_spline_i.
            return_raw_runs (bool): Return dataset of raw runs data.
            return_X_y (bool): If True, returns (features, label) data instead of
                tf.data.Dataset. It has no effect if return_raw_runs is False.
            take_samples (int): Number of samples to take to form partition.

        Returns:
            A tf.data.Dataset.
        """

        if not return_raw_runs and isinstance(mode, list):
            raise ValueError("Multiple mode could be used only for raw runs.")

        if isinstance(mode, list):
            partition_files = [f for m in mode for f in self.partition[string_mode(m)]]
        else:
            partition_files = [f for f in self.partition[string_mode(mode)]]

        partition_ids = np.array(
            [np.where(self.all_files == f) for f in partition_files]
        ).flatten()

        partition_ids = tf.convert_to_tensor(partition_ids, dtype=tf.int32)
        partition_len = tf.shape(partition_ids).numpy()

        try:
            labels = FIELD_COMPONENTS[label]
        except KeyError:
            labels = [label]

        #  Include metadata into the dataset as features
        if metadata:
            features = features + metadata

        if self._dataset is None or return_raw_runs:

            output_vars = features + labels
            vars_to_check = ["plasmaVolume", "minorRadius"]

            readers = {
                k: v
                for k, v in self.get_variable_readers(
                    use_coil_currents_ratio,
                    use_nyquist_resolution,
                    pressure_profile_type,
                    toroidal_current_profile_type,
                ).items()
                if k in output_vars + vars_to_check
            }
            #  Check if vars_to_check has to be read
            for v in vars_to_check:
                if v in features:
                    readers[v] = readers[v][:-1] + (True,)

            output_types = {
                var: reader[2] for var, reader in readers.items() if var in output_vars
            }
            output_shapes = {
                var: reader[3] for var, reader in readers.items() if var in output_vars
            }

            output_types["file"] = tf.int32
            output_shapes["file"] = ()
            output_types["file_path"] = tf.string
            output_shapes["file_path"] = ()

            #  Generate a tf.data.Dataset
            dataset = tf.data.Dataset.from_generator(
                generator=lambda: gen_runs(self.all_files, variable_readers=readers),
                output_types=output_types,
                output_shapes=output_shapes,
            )

            if return_raw_runs:
                dataset = dataset.filter(
                    lambda x: tf.reduce_any(
                        tf.equal(tf.repeat(x["file"], partition_len), partition_ids)
                    )
                )
                dataset = dataset.map(
                    lambda x: {k: v for k, v in x.items() if k != "file"},
                    num_parallel_calls=tf.data.experimental.AUTOTUNE,
                )

                if return_X_y:
                    X = map(
                        lambda x: np.hstack([v for k, v in x.items() if k in features]),
                        dataset.as_numpy_iterator(),
                    )
                    Y = map(
                        lambda x: np.stack(
                            [v for k, v in x.items() if k in labels],
                            axis=-1,
                        ),
                        dataset.as_numpy_iterator(),
                    )
                    return map(lambda x: np.array([x_ for x_ in x]), (X, Y))

                return dataset

            def build_sample_fn(x):
                #  Add file and file_path keys as a feature
                return build_sample(
                    x,
                    features=features + ["file", "file_path"],
                    labels=labels,
                    modes=get_num_fourier_modes_from_label(
                        label, use_nyquist_resolution
                    ),
                )

            dataset = dataset.map(
                build_sample_fn, num_parallel_calls=tf.data.experimental.AUTOTUNE
            )

            radial_indices = get_label_radial_indices(radial_points)

            if isinstance(fourier_modes, int):
                fourier_modes = (fourier_modes, fourier_modes)

            def downsample_output_fn(x, y):
                return downsample_label_tensor(
                    x,
                    y,
                    radial_indices=radial_indices,
                    modes=fourier_modes
                    if fourier_modes is not None
                    else get_num_fourier_modes_from_label(
                        label, use_nyquist_resolution
                    ),
                    orig_modes=get_num_fourier_modes_from_label(
                        label, use_nyquist_resolution
                    ),
                )

            dataset = dataset.map(
                downsample_output_fn, num_parallel_calls=tf.data.experimental.AUTOTUNE
            )

            scale_labels_fn = [
                self.get_scaler_fn(
                    label_,
                    mode="scale",
                    radial_points=radial_points,
                    fourier_modes=fourier_modes,
                )
                for label_ in labels
            ]

            def scale_fn(x, y):
                return apply_scale_fn_to_label(x, y, scale_fn=scale_labels_fn)

            if scale_label:
                dataset = dataset.map(
                    scale_fn, num_parallel_calls=tf.data.experimental.AUTOTUNE
                )

            if flatten:
                dataset = dataset.map(lambda x, y: (x, tf.keras.backend.flatten(y)))

            self._dataset = dataset.cache()

        dataset = self._dataset

        if self._cached_datasets[string_mode(mode)] is None:

            dataset = dataset.filter(
                lambda x, y: tf.reduce_any(
                    tf.equal(tf.repeat(x["file"], partition_len), partition_ids)
                ),
            )
            dataset = dataset.map(
                lambda x, y: ({k: v for k, v in x.items() if k != "file"}, y),
                num_parallel_calls=tf.data.experimental.AUTOTUNE,
            )

            dataset = dataset.take(take_samples)

            self._cached_datasets[string_mode(mode)] = dataset.cache()

        dataset = self._cached_datasets[string_mode(mode)]

        if shuffle:
            dataset = dataset.shuffle(10 * batch_size)

        dataset = dataset.batch(batch_size)
        dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)

        return dataset
