"""VMEC routines."""

import numpy as np


def compute_shear(iota, phiedge, to_iotas=False):
    """Compute magnetic shear based on iota profile and toroidal flux.

    The magnetic shear is defined as shear = d(iota)/d(phi).

    As computed by VMEC, the magnetic shear required the iota profile
    on half-mesh (iotas). If iotaf is given, set `to_iotas` to True to perform
    conversion.

    Routine as in `VMEC2000/Sources/Input_Output/mercier.f`.

    Notes:
        phi_real = 2 * np.pi * phips * sign_jac.
        shear is computed using iotas, not iotaf.
        How iotaf is computed based on iotas is in `General/add_fluxes.f90`.

    Args:
        iota (array_like): iota profile as a function of s.
        phiedge (float): toroidal flux at the edge.
        to_iotas (bool): cast iota profile to half-mesh.
    """

    #  Cast iota profile to half-mesh
    if to_iotas:
        iotas = np.zeros(iota.shape)
        iotas[1] = 0.5 * (iota[0] + iota[1])
        for i in range(2, len(iota)):
            iotas[i] = 2 * iota[i - 1] - iotas[i - 1]
    else:
        iotas = iota

    hs = 1 / (len(iota) - 1)

    shear = np.zeros(iotas.shape)
    shear[1:-1] = (iotas[2:] - iotas[1:-1]) / (hs * np.abs(phiedge))

    return shear
