"""Metric functions."""

from numpy import (
    divide,
    square,
    subtract,
    sqrt,
    absolute,
    zeros_like,
    quantile,
    array,
    multiply,
)
from sklearn.metrics import r2_score


def _handle_multioutput(values, multioutput):
    """Handle multioutput values for metrics evaluation.

    The `multioutput` arg is similar in signature to the relative sklearn one.

    Args:
        values (np.ndarray): Array object to handle.
        multioutput (str, array-like or int): String code on how to handle values,
            array-like to weight outputs, or
            axis where to compute the mean.
    """

    if multioutput == "raw_values":
        return values

    if multioutput == "uniform_average":
        return values.mean()

    if hasattr(multioutput, "__len__"):
        return (multiply(values, array(multioutput))).mean()

    return values.mean(axis=multioutput)


def mse(value, ref, multioutput="uniform_average"):
    """Compute the mean squared error metric between `value` and `ref`.

    Args:
        value (np.ndarray): Array object of (#samples, #features, ...).
        ref (np.ndarray): Array object of (#samples, #features, ...).
        multioutput : string in ["uniform_average", "raw_values"] or axis where to
            compute the mean, same API as numpy.

    Returns:
        The computed mean squared error between value and ref.
    """

    mse = square(subtract(value, ref)).mean(axis=0)

    return _handle_multioutput(mse, multioutput)


def mae(value, ref, multioutput="uniform_average"):
    """Compute the mean absolute error metric between `value` and `ref`.

    Args:
        value (np.ndarray): Array object of (#samples, #features, ...).
        ref (np.ndarray): Array object of (#samples, #features, ...).
        multioutput : string in ["uniform_average", "raw_values"] or axis where to
            compute the mean, same API as numpy.

    Returns:
        The computed mean absolute error between value and ref.
    """

    mae = abs(subtract(value, ref)).mean(axis=0)

    return _handle_multioutput(mae, multioutput)


def rmse(value, ref, multioutput="uniform_average"):
    """Compute the root mean squared error metric between `value` and `ref`.

    Args:
        value (np.ndarray): Array object of (#samples, #features, ...).
        ref (np.ndarray): Array object of (#samples, #features, ...).
        multioutput : string in ["uniform_average", "raw_values"] or axis where to
            compute the mean, same API as numpy.

    Returns:
        The computed root mean squared error between value and ref.
    """

    rmse = sqrt(mse(value, ref, multioutput="raw_values"))

    return _handle_multioutput(rmse, multioutput)


def rmsdiqr(value, ref, multioutput="uniform_average"):
    """Compute the normalized root mean square deviation between `value` and `ref`.

    This metric uses the difference between the .75 and .25 quantile
    as a scale. As it computes a scaled distance, it expects to receive
    a ndarray of shape (#samples, #features).

    Args:
        value (np.ndarray): Array object of (#samples, #features, ...).
        ref (np.ndarray): Array object of (#samples, #features, ...).
        multioutput : string in ["uniform_average", "raw_values"] or axis where to
            compute the mean, same API as numpy.

    Returns:
        The computed normalized mean squared deviation between value and ref.
    """

    rmse_ = rmse(value, ref, multioutput="raw_values")
    scale = abs(quantile(ref, 0.75, axis=0) - quantile(ref, 0.25, axis=0))
    rmsdiqr = divide(rmse_, scale, out=zeros_like(scale), where=scale != 0)

    return _handle_multioutput(rmsdiqr, multioutput)


def rmsdstd(value, ref, multioutput="uniform_average"):
    """Compute the normalized root mean square deviation between `value` and `ref`.

    This metric uses the standard deviation of the ref data
    as a scale. As it computes a scaled distance, it expects to receive
    a ndarray of shape (#samples, #features).

    Args:
        value (np.ndarray): Array object of (#samples, #features, ...).
        ref (np.ndarray): Array object of (#samples, #features, ...).
        multioutput : string in ["uniform_average", "raw_values"] or axis where to
            compute the mean, same API as numpy.

    Returns:
        The computed normalized mean squared deviation between value and ref.
    """

    rmse_ = rmse(value, ref, multioutput="raw_values")
    scale = ref.std(axis=0)
    rmsdstd = divide(rmse_, scale, out=zeros_like(scale), where=scale != 0)

    return _handle_multioutput(rmsdstd, multioutput)


def mape(value, ref, multioutput="uniform_average"):
    """Compute mean absolute percentage error between `value` and `ref`.

    Args:
        value (np.ndarray): Array object of (#samples, #features, ...).
        ref (np.ndarray): Array object of (#samples, #features, ...).
        multioutput : string in ["uniform_average", "raw_values"] or axis where to
            compute the mean, same API as numpy.

    Returns:
        The computed mean absolute percentage error between value and ref.
    """

    mape = absolute(
        divide(subtract(value, ref), ref, out=zeros_like(ref), where=ref != 0)
    ).mean(axis=0)

    return _handle_multioutput(mape, multioutput)


def r2score(value, ref, multioutput="uniform_average"):
    """Compute the r2 score between `value` and `ref`.

    This metric makes use of the sklearn implementation.

    Args:
        value (np.ndarray): Array object of (#samples, #features, ...).
        ref (np.ndarray): Array object of (#samples, #features, ...).
        multioutput (str): Multioutput string to be passed to the sklearn method.

    Returns:
        The computed r2 score between value and ref.
    """

    return r2_score(ref, value, multioutput=multioutput)


def get_metric(metric):
    """Build metric function from string."""

    if metric == "mse":
        return mse

    if metric == "mae":
        return mae

    if metric == "rmse":
        return rmse

    if metric == "mape":
        return mape

    if metric == "rmsdiqr":
        return rmsdiqr

    if metric == "rmsdstd":
        return rmsdstd

    if metric == "r2score":
        return r2score

    raise ValueError("%s metric is not supported" % metric)
