"""Config parameters for the `vmec_dnn_surrogate` research project."""

from numpy import linspace, pi

#  Project structure
DATA_PATH = "data"
FIGURES_PATH = "figures"
REPORTS_PATH = "reports"

#  W7-X parameters
NUM_FIELD_PERIODS = 5

#  Data parameters
NUM_OF_RADIAL_POINTS = 99
NUM_OF_FOURIER_MODES = [12, 12]
NUM_OF_NYQUIST_FOURIER_MODES = [19, 18]
NUM_OF_FOURIER_COEFFS = NUM_OF_FOURIER_MODES[0] ** 2 * 2
NUM_OF_NYQUIST_FOURIER_COEFFS = (
    NUM_OF_NYQUIST_FOURIER_MODES[0] * NUM_OF_NYQUIST_FOURIER_MODES[1] * 2
    + NUM_OF_NYQUIST_FOURIER_MODES[0]
    - NUM_OF_NYQUIST_FOURIER_MODES[1]
)
NUM_OF_PROFILE_USED_POINTS = 10
FOURIER_SHAPE = (NUM_OF_RADIAL_POINTS, NUM_OF_FOURIER_COEFFS)
NYQUIST_FOURIER_SHAPE = (NUM_OF_RADIAL_POINTS, NUM_OF_NYQUIST_FOURIER_COEFFS)

#  Field components
FIELD_COMPONENTS = {
    "Bsub": ["BsubSSin", "BsubUCos", "BsubVCos"],
    "Bsup": ["BsupUCos", "BsupVCos"],
    "Curr": ["CurrUCos", "CurrVCos"],
    "Space": ["RCos", "LambdaSin", "ZSin"],
    "BCos": ["BCos"],
}
NYQUIST_RESOLUTION_LABELS = ["BCos", "Bsub", "Bsup"]

#  Figures and reports
FILE_SEPARATOR = "-"

#  Poloidal and toroidal grid for real space evaluation
NUM_POLOIDAL_GRID_POINTS = 18
NUM_TOROIDAL_GRID_POINTS = 9
POLOIDAL_ANGLES = linspace(0, 2 * pi, NUM_POLOIDAL_GRID_POINTS, endpoint=False)
TOROIDAL_ANGLES = linspace(
    0, 2 * pi / NUM_FIELD_PERIODS, NUM_TOROIDAL_GRID_POINTS, endpoint=False
)
