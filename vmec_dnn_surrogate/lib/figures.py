"""Figures definition for the `vmec_dnn_surrogate` research project."""

import logging
import glob
import os
import random

import numpy as np
from scipy import interpolate
import pandas as pd
from matplotlib.ticker import AutoMinorLocator
from matplotlib.ticker import FuncFormatter
import matplotlib.pyplot as plt
import seaborn as sns
import tikzplotlib
from sklearn.metrics import r2_score
from sklearn.manifold import TSNE
from sklearn.preprocessing import MinMaxScaler
from hyperopt.plotting import main_plot_history, main_plot_vars

from rna.plotting import set_style
from w7x.vmec import (
    FourierCoefficients,
    SurfaceCoefficients,
    infer_fourier_modes_from_array,
)

from vmec_dnn_surrogate.lib.dataset import get_flux_points, get_label_radial_indices
from vmec_dnn_surrogate.lib.metrics import get_metric
from vmec_dnn_surrogate.lib.config import NUM_OF_RADIAL_POINTS, NUM_FIELD_PERIODS
from vmec_dnn_surrogate.lib.utils import (
    from_fourier_to_real,
    get_label_component,
    get_fourier_modes_from_label,
    to_values,
    commit_and_save,
    bootstrap_mean_and_ci,
)

sns.set()
sns.set_style("ticks")
set_style(style="rna")

#  Config
_CMAP = sns.diverging_palette(220, 10, as_cmap=True)


def compute_subplot_index(index, split_index):
    """Computes matplotlib subplot index given index `index`."""

    if index < split_index:
        return index * 2 + 1

    return (index - split_index + 1) * 2


def get_colormap(cmap):
    """Parses library defined colormap.

    It currently supports:
    * matplotlib

    Future:
    * seaborn

    Args:
        cmap: A colormap object or string identifier.

    Returns:
        The colormap object.
    """

    if isinstance(cmap, str):
        return plt.get_cmap(cmap)

    return cmap


def get_latex_string(label):
    """Builds the latex string of the given label.

    Args:
        label (str): The VMEC output label.

    Returns:
        The Latex formatted script of the given label.
    """

    if label.endswith("_orig"):
        return get_latex_string(label.replace("_orig", ""))

    if label.endswith("_pred"):
        return r"\hat{" + get_latex_string(label.replace("_pred", "")) + "}"

    #  TODO(@amerlo): Fix double superscript for controvariant components.
    latex_labels = {
        "RCos": "R",
        "LambdaSin": r"\lambda",
        "ZSin": "Z",
        "BCos": "B",
        "BsupUCos": r"Bu",
        "BsupVCos": r"Bv",
        "BsubSSin": r"B_s",
        "BsubUCos": r"B_u",
        "BsubVCos": r"B_v",
        "iota": r"\mathrel{\hbox{-}\mkern-6.55mu \iota}",
    }

    try:
        return latex_labels[label]
    except KeyError:
        return label


def get_label_unit(label, use_milli=False):
    """Return string for label unit.

    Args:
        label (str): The VMEC output label.
        use_milli (bool): Use 10^-3 version of the unit.

    Returns:
        The label unit as a string.
    """

    if label.endswith("_orig"):
        return get_label_unit(label.replace("_orig", ""), use_milli)

    if label.endswith("_pred"):
        return get_label_unit(label.replace("_pred", ""), use_milli)

    milli_units = {
        "RCos": r"\si{\milli\meter}",
        "LambdaSin": r"\si{\milli\radian}",
        "ZSin": r"\si{\milli\meter}",
        "BCos": r"\si{\milli\tesla}",
    }

    if use_milli:
        return milli_units.get(label, "")

    units = {
        "RCos": r"\si{\meter}",
        "LambdaSin": r"\si{\radian}",
        "ZSin": r"\si{\meter}",
        "BCos": r"\si{\tesla}",
        "shear": r"\si{\per\weber}",
    }

    return units.get(label, "")


def get_nrows(length):
    """Get number of rows for a square plot."""

    return int(np.ceil(np.sqrt(length)))


def get_poloidal_angles(num_poloidal_angles):
    """Get the poloidal angles."""
    return np.linspace(0, 2 * np.pi, num_poloidal_angles)


def get_toroidal_angles(num_toroidal_angles):
    return np.linspace(0, 2 * np.pi / NUM_FIELD_PERIODS, num_toroidal_angles)


class Figure:
    """A generic figure to plot."""

    def __init__(self, **kwargs):

        title = kwargs.pop("title", None)

        self.fig = plt.figure(**kwargs)
        self.fig.suptitle(title)

        self.data = None

    def establish_kwargs(self, kwargs):
        """Parses and sanitizes input plot keyword arguments into a common representation.

        Args:
            **kwargs: Arbitrary keyword arguments.
        """

        #  Figure kwargs
        self.kind = kwargs.pop("kind", "map")
        self.data_kind = kwargs.pop("data_kind", "plain")
        self.metric = kwargs.pop("metric", "mse")

        self.remove_ticks = kwargs.pop("remove_ticks", False)
        self.include_units = kwargs.pop("include_units", True)
        self.use_milli = kwargs.pop("use_milli", False)
        self.title = kwargs.pop("title", None)

        #  Data kwargs
        self.num_of_radial_points = kwargs.pop(
            "num_of_radial_points", NUM_OF_RADIAL_POINTS
        )
        self.modes = kwargs.pop("modes", None)

        #  Space kwargs
        self.space = kwargs.pop("space", None)
        self.radial_points = kwargs.pop("s", list(range(self.num_of_radial_points)))

        #  Real space
        self.poloidal_angles = kwargs.pop("poloidal_angles", 18)
        if isinstance(self.poloidal_angles, int):
            self.poloidal_angles = get_poloidal_angles(self.poloidal_angles)

        self.toroidal_angles = kwargs.pop("toroidal_angles", 5)
        if isinstance(self.toroidal_angles, int):
            self.toroidal_angles = get_toroidal_angles(self.toroidal_angles)

        #  Fourier space
        self.poloidal = kwargs.pop("m", None)
        self.toroidal = kwargs.pop("n", None)

    def plot(self, data, **kwargs):
        """Plots data into current figure.

        Args:
            data (pd.DataFrame | np.array): Data to plot.
            **kwargs: Additional keyboard arguments to pass to Figure plot method.
        """

        raise NotImplementedError("A Figure class should implement the `plot` method")

    def save_data(self, filepath):
        """Save figure data for later use."""

        if isinstance(self.data, pd.DataFrame):
            self.data.to_pickle(os.path.splitext(filepath)[0] + ".pkl")

    def save(self, path):
        """Saves figure to file.

        Args:
            path (str): File path where to save figure.
        """

        self.fig.tight_layout()

        self.fig.savefig(path)
        self.save_data(path)

        try:
            tikzplotlib.clean_figure()
        except ValueError:
            logger = logging.getLogger()
            logger.warning("TikzPlotLib was not able to clean the figure")

        try:
            #  Clean *.png artefacts
            file_list = glob.glob(os.path.splitext(path)[0] + "*.png")
            for file_ in file_list:
                if os.path.isfile(file_):
                    os.remove(file_)

            commit_and_save(
                tikzplotlib.get_tikz_code(), "{}.pgf".format(os.path.splitext(path)[0])
            )
        except Exception as exception:
            logger = logging.getLogger()
            logger.warning("TikzPlotLib has encountered an error: %s", exception)

        try:
            plt.close(self.fig)
        except TypeError as exception:
            logger = logging.getLogger()
            logger.warning("Figure has not been closed: %s", exception)


class Hist(Figure):
    """A generic histogram plot."""

    def plot(self, data, **kwargs):

        self.establish_kwargs(kwargs)

        #  Figure specific kwargs
        xlabels = kwargs.pop("xlabel", None)
        ylabel = kwargs.pop(
            "ylabel", "pdf" if kwargs.pop("kde", False) else "Number of samples"
        )
        xlim = kwargs.pop("xlim", None)
        fontsize = 12
        log = kwargs.pop("log", False)

        values = to_values(data)

        #  Get number of plots
        subplots = values.shape[1]
        nrows = get_nrows(subplots)

        #  Sanitize xlabels
        if xlabels is None:
            xlabels = [data.columns[0]]
            if subplots != 1:
                xlabels = [
                    "{}_{}".format(xlabels[0], index) for index in range(subplots)
                ]
        else:
            if not hasattr(xlabels, "__len__"):
                xlabels = [xlabels] * subplots
            elif len(xlabels) != subplots:
                xlabels = [xlabels[0]] * subplots

        if log:
            values = np.log10(
                values, where=values > 0, out=np.full_like(values, np.nan)
            )

        #  Extend xlim to all subplots if not list
        if xlim is not None and not isinstance(xlim[0], list):
            xlim = [xlim] * subplots

        for i in range(subplots):

            with sns.axes_style({"axes.spines.right": False, "axes.spines.top": False}):

                axes = self.fig.add_subplot(nrows, nrows, i + 1)
                sns.histplot(values[:, i], ax=axes, color=_CMAP(0.1), **kwargs)

                axes.set_ylabel(ylabel, fontsize=fontsize)
                axes.set_xlabel(xlabels[i], fontsize=fontsize)
                axes.tick_params(labelsize=fontsize)
                axes.get_xaxis().set_minor_locator(AutoMinorLocator())

                if xlim is not None:
                    axes.set_xlim(xlim[i][0], xlim[i][1])


class Closest(Hist):
    """Build a proxy for prediction data with closest runs and plot histogram
    of given `metric`.
    """

    def plot(self, data, **kwargs):

        self.establish_kwargs(kwargs.copy())

        #  Figure specific kwargs
        features = kwargs.pop("features", [])

        labels = list(set(data.columns) - set(features))

        #  Hack to retrieve metadata from data
        self.num_of_radial_points = data.iloc[0][labels[0]].shape[0]
        self.modes = infer_fourier_modes_from_array(data.iloc[0][labels[0]])

        fourier_indices = FourierCoefficients.get_indices(
            self.radial_points,
            self.poloidal,
            self.toroidal,
            self.num_of_radial_points,
            self.modes,
        )

        values = to_values(data, labels).reshape(len(data), -1, len(labels))
        values = values[:, fourier_indices, :]
        features_values = np.stack(data[features].values)

        #  For each sample, compute the index of the closest one in the feature space
        distances = np.ones((len(data), len(data)))
        for i, sample_i in enumerate(features_values):
            for j, sample_j in enumerate(features_values):
                if i != j:
                    distances[i, j] = get_metric("rmsdstd")(
                        np.hstack(sample_j), np.hstack(sample_i)
                    )

        closest_indices = [
            np.where(distances == d)[1][0] for d in np.amin(distances, axis=1)
        ]
        closest = np.array([values[index, ...] for index in closest_indices])

        #  Build trues and proxy for preds
        trues = values.reshape(values.shape[0], -1)
        preds = closest.reshape(closest.shape[0], -1)

        proxy_distances = get_metric(self.metric)(
            preds, trues, multioutput="raw_values"
        )
        super().plot(
            pd.DataFrame({self.metric: proxy_distances}),
            ylabel="Number of Fourier coefficients",
            **kwargs,
        )

        axes = self.fig.gca()
        axes.annotate(
            r"median ${} = {{{:.3f}}}$".format(self.metric, np.median(proxy_distances)),
            xy=(0.05, 0.90),
            xycoords="axes fraction",
        )


class Scatter(Figure):
    """A generic scatter plot."""

    def plot(self, data, **kwargs):

        #  Figure specific kwargs
        xlabel = kwargs.pop("xlabel", None)
        ylabel = kwargs.pop("ylabel", None)
        hue = kwargs.pop("hue", None)
        cmap = kwargs.pop("cmap", "Reds")
        cbar_label = kwargs.pop("cbar_label", None)
        cbar_limits = kwargs.pop("cbar_limits", None)
        kind = kwargs.pop("kind", "2d")
        annotate = kwargs.pop("annotate", None)

        #  t-SNE kwargs
        tsne_kws = {"n_iter": 5000}
        tsne_kws.update(kwargs.pop("tsne_kws", {}))

        #  If hue is not defined, take last column
        if not hue and len(data.columns) > 2:
            hue = data.columns[-1]

        apply_ = kwargs.pop("apply", None)
        if apply_ is not None:
            #  Avoid data corruption in other figures
            data = data.copy()
            for i, var in zip([0, 1, data.columns.get_loc(hue)], ["x", "y", "hue"]):
                if apply_[var] is not None:
                    data.iloc[:, i] = data.iloc[:, i].apply(eval(apply_[var]), 1)

        hue_data = np.stack(data.iloc[:, -1].values).reshape(len(data), -1)
        nrows = get_nrows(hue_data.shape[1])

        #  Fix cbar kws
        if isinstance(cbar_label, str):
            cbar_label = [cbar_label] * hue_data.shape[1]

        if isinstance(cbar_limits, tuple):
            cbar_limits = [cbar_limits] * hue_data.shape[1]

        #  Precompute t-SNE embedding
        if kind == "tsne":
            X = to_values(data, labels=data.columns[:-1], flatten=True)
            if tsne_kws.pop("scale", False):
                X = MinMaxScaler(feature_range=(-1, 1)).fit_transform(X)
            X_ = TSNE(n_components=2, verbose=1, **tsne_kws).fit_transform(X)

        for i in range(hue_data.shape[1]):

            axes = self.fig.add_subplot(nrows, nrows, i + 1)

            if kind == "tsne":

                axes = sns.scatterplot(
                    x=X_[:, 0],
                    y=X_[:, 1],
                    hue=hue_data[:, i],
                    palette=cmap,
                    ax=axes,
                    **kwargs,
                )

            elif kind == "3d":

                if i > 0:
                    raise ValueError("3D plot supports one axes only.")

                axes = self.fig.add_subplot(111, projection="3d")
                axes.scatter3D(
                    data.iloc[:, 0],
                    data.iloc[:, 1],
                    hue_data[:, -1],
                    c=hue_data[:, -1],
                    cmap=cmap,
                )

            else:

                sns.scatterplot(
                    x=data.iloc[:, 0],
                    y=data.iloc[:, 1],
                    hue=hue_data[:, i],
                    palette=cmap,
                    ax=axes,
                    **kwargs,
                )

            #  Replace legend with colorbar
            if hue:
                if cbar_limits:
                    norm = plt.Normalize(*cbar_limits[i])
                else:
                    norm = plt.Normalize(
                        np.quantile(hue_data[:, i], q=0.05),
                        np.quantile(hue_data[:, i], q=0.95),
                    )
                sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
                sm.set_array([])

                if axes.get_legend():
                    axes.get_legend().remove()
                cbar = axes.figure.colorbar(sm)

            if annotate and kind != "3d":
                axes.text(
                    0.05,
                    0.975,
                    s=annotate["fmt"].format(eval(annotate["apply"])(hue_data[:, i])),
                    transform=axes.transAxes,
                    verticalalignment="top",
                )

            if xlabel:
                axes.set_xlabel(xlabel)

            if ylabel:
                axes.set_ylabel(ylabel)

            if hue and cbar_label:
                cbar.ax.set_title(cbar_label[i])


class Line(Figure):
    """A generic line plot.

    In order to plot multiple lines, set them in the `y` args.
    """

    def plot(self, data, **kwargs):

        x = kwargs.pop("x", data.columns[0])
        y = kwargs.pop("y", [data.columns[1]])

        assert len(y) <= 2, "Number of lines to plot must be <= 2"

        colors = sns.color_palette("muted", n_colors=2)[:2]

        with sns.axes_style({"axes.grid": True}):

            axis = self.fig.add_subplot(1, 1, 1)
            sns.lineplot(
                x=x,
                y=y[0],
                data=data,
                ax=axis,
                color=colors[0],
                **kwargs,
            )

            if len(y) == 2:
                axis.set_ylabel(y[0], color=colors[0])
                twinx_axis = axis.twinx()
                twinx_axis.set_ylabel(y[1], color=colors[1])
                sns.lineplot(
                    x=x, y=y[1], data=data, ax=twinx_axis, color=colors[1], **kwargs
                )


class Profile(Figure):
    """A generic profile plot.

    For each data columns, profile will be structured as follow:

    (#number of samples, #number of profiles, #profiles)

    Where `profiles` is a single numyp.ndarray, `number of profiles`
    are the distinct type of profiles within each data column (a single
    fourier coefficient profile), and `number of samples` is the number of
    distinct vmec runs profile to plot.
    """

    def _plot_profile(
        self, data, indices, scale_index, label, interp, overlay, marker, axes, **kwargs
    ):
        """Plot multiple profiles on top of each other.

        TODO(@amerlo): This has to be cleaned.
        """

        data = data.reshape(len(data), -1)

        number_of_profiles = 1
        if self.space == "fourier":
            number_of_profiles = len(indices)

        for profile in range(number_of_profiles):

            #  Pick a color for each profile type
            color = sns.color_palette("deep", n_colors=number_of_profiles)[profile]

            for j in range(data.shape[0]):

                if self.space == "fourier":
                    sample_data = data[j, indices[profile]]
                else:
                    if data.shape[1] == NUM_OF_RADIAL_POINTS:
                        sample_data = data[
                            j,
                            get_label_radial_indices(self.num_of_radial_points),
                        ]
                    else:
                        sample_data = data[j, :]

                grid = get_flux_points(len(sample_data))

                #  Scale data
                if scale_index is not None:
                    if scale_index == "max":
                        sample_data = np.divide(
                            sample_data,
                            np.absolute(sample_data).max(),
                            where=np.absolute(sample_data).max() != 0,
                        )
                    else:
                        sample_data = sample_data / sample_data[scale_index]

                #  Interpolate profile with cubic poly
                if interp:
                    profile = interpolate.interp1d(grid, sample_data, kind="cubic")
                    grid = np.linspace(0, 1, 100)
                    sample_data = profile(grid)

                if overlay:
                    kwargs["marker"] = marker

                sns.lineplot(
                    x=grid,
                    y=sample_data,
                    ax=axes,
                    label=label[profile] if (j == 0 and label is not None) else None,
                    color=color,
                    **kwargs,
                )

    def _plot_aggregated_profile(self, data, indices, axes, overlay, **kwargs):
        """Transforms data in long-format and plots with ci.

        `data` is expected to be of shape:

        (num_samples, radial_points, ...)

        If multidimensional data are provided, take the mean along last axis.
        """

        def _bootstrap_pi(x):
            """Compute bootstrap pi of the input values."""

            _, min_, max_ = bootstrap_mean_and_ci(
                x, ci=0.95, n_boot=10000, return_quantiles=True
            )
            return min_, max_

        grid = get_flux_points(data.shape[1])

        #  Build long form dataframe
        samples_df = []
        for i in range(len(data)):
            #  TODO(@amerlo): Could we improve this solution?
            if overlay and indices is not None:
                for k in indices:
                    samples_df.append(
                        pd.DataFrame(
                            {
                                "sample": [i] * len(grid),
                                "x": grid,
                                r"phi": self.toroidal_angles[k],
                                "y": data[i, :, k].reshape(len(grid), -1).mean(axis=-1),
                            }
                        )
                    )
            else:
                samples_df.append(
                    pd.DataFrame(
                        {
                            "sample": [i] * len(grid),
                            "x": grid,
                            "y": data[i].reshape(len(grid), -1).mean(axis=-1),
                        }
                    )
                )

        long_form_df = pd.concat(samples_df, ignore_index=True)
        sns.lineplot(
            data=long_form_df,
            x="x",
            y="y",
            hue="phi" if overlay else None,
            errorbar=_bootstrap_pi if not overlay else None,
            ax=axes,
            **kwargs,
        )

    def plot(self, data, **kwargs):

        if not isinstance(data, pd.DataFrame):
            raise ValueError("Input data must be a pandas DataFrame")

        self.establish_kwargs(kwargs)

        #  Figure specific kwargs
        aggregate = kwargs.pop("aggregate", False)
        overlay = kwargs.pop("overlay", False)
        label = kwargs.pop("label", None)
        interp = kwargs.pop("interp", False)
        scale_index = kwargs.pop("scale_index", None)

        sample = kwargs.pop("sample", None)
        num_samples = kwargs.pop("samples", len(data))

        if sample is not None:
            data = data.iloc[[sample]]
        else:
            data = data.sample(num_samples)

        #  Save only samples to plot
        self.data = data

        nrows = len(data.columns)

        xlabel = kwargs.pop("xlabel", r"$s$")
        ylabel = kwargs.pop("ylabel", list(data.columns))
        if not isinstance(ylabel, list):
            ylabel = [ylabel] * nrows

        if self.space == "fourier":
            #  Plot different FCs
            indices = [
                FourierCoefficients.get_indices(
                    range(self.num_of_radial_points), m, n, modes=self.modes
                )
                for m in self.poloidal
                for n in self.toroidal
            ]
            label = [
                [
                    "$" + get_latex_string(c) + r"_{{{}{}}}$".format(m, n)
                    for m in self.poloidal
                    for n in self.toroidal
                ]
                for c in data.columns
            ]
        else:
            #  Plot different toroidal angles
            indices = self.toroidal

        if not isinstance(scale_index, list):
            scale_index = [scale_index] * nrows

        axeses = []

        for i in range(nrows):

            if overlay:
                actual_nrows = int(nrows / 2)
                if i >= actual_nrows:
                    axes = axeses[i - actual_nrows]
                else:
                    axes = self.fig.add_subplot(actual_nrows, 1, i + 1)
                    axeses.append(axes)
            else:
                axes = self.fig.add_subplot(nrows, 1, i + 1)

            row_data = to_values(data, data.columns[i])

            if aggregate:
                Profile._plot_aggregated_profile(
                    self,
                    data=row_data,
                    indices=indices,
                    overlay=overlay,
                    axes=axes,
                    **kwargs,
                )
            else:
                Profile._plot_profile(
                    self,
                    data=row_data,
                    indices=indices,
                    label=label[i] if label is not None else None,
                    scale_index=scale_index[i],
                    interp=interp,
                    overlay=overlay,
                    marker="^" if i >= int(nrows / 2) else "s",
                    axes=axes,
                    **kwargs,
                )

            data_label = (
                data.columns[i] if not overlay else data.columns[i - actual_nrows]
            )

            ylabel_ = ylabel[i] if not overlay else ylabel[i - actual_nrows]
            if self.include_units:
                ylabel_ = ylabel_ + " [" + get_label_unit(data_label) + "]"
            axes.set_ylabel(ylabel_)
            axes.set_xlabel(xlabel)

            if not self.title:
                title = "$" + get_latex_string(data_label) + "$"
            else:
                title = self.title[i] if isinstance(self.title, list) else self.title
            axes.set_title(label=title, loc="left")

            if self.remove_ticks:
                axes.set_yticks([])


class Map(Figure):
    """A generic 2D map."""

    def plot(self, data, **kwargs):
        def get_labelsize(modes):
            """Establish labelsize based on number of fourier modes."""

            #  TODO(@amerlo): Add look-up table entries here.
            if modes <= 4:
                return 10

            return 8

        def _black_or_white(x):
            """Use lighter color if value is above threshold."""

            return "black" if x < 0.6 else "white"

        self.establish_kwargs(kwargs)
        self.data = data

        #  Figure specific kwargs
        split = kwargs.pop("split", False)
        cbar = kwargs.pop("cbar", True)
        cbar_label = kwargs.pop("cbar_label", None)
        hspace = kwargs.pop("hspace", 0.25)
        cmap = kwargs.pop("cmap", None)
        vmax = kwargs.pop("vmax", None)
        vmin = kwargs.pop("vmin", None)
        levels = kwargs.pop("levels", None)
        annotate = kwargs.pop("annotate", False)
        annotate_fmt = kwargs.pop("annotate_fmt", "{:.2f}")
        sample = kwargs.pop("sample", random.randint(0, len(data) - 1))

        kwargs["cmap"] = get_colormap(cmap)

        cbar_format = kwargs.pop("cbar_format", None)
        if self.use_milli:
            cbar_format = FuncFormatter(
                lambda x_val, tick_pos: "{:.0f}".format(x_val * 1e3)
            )

        if self.space == "real":
            #  TODO(@amerlo): This could be handled in a better way.
            #  Hack to understand if values are already in real space.
            if len(data.iloc[sample, 0].shape) == 2:
                components = [
                    from_fourier_to_real(
                        coeffs[self.radial_points, :],
                        poloidal_angles=self.poloidal_angles,
                        toroidal_angles=self.toroidal_angles,
                        component=get_label_component(col),
                        modes=get_fourier_modes_from_label(col, self.modes),
                    )[0, ...]
                    for col, coeffs in data.iloc[sample].items()
                ]

            else:
                #  Plot in real space with data already in real space
                components = [
                    values[self.radial_points[-1], ...] for values in data.iloc[sample]
                ]

                #  Build grid from data
                self.poloidal_angles = get_poloidal_angles(components[0].shape[0])
                self.toroidal_angles = get_toroidal_angles(components[0].shape[1])

        elif self.space == "fourier":
            #  Plot in Fourier space
            components = [
                FourierCoefficients.from_array(
                    coeffs.reshape(self.num_of_radial_points, -1)[
                        self.radial_points, :
                    ],
                    fourier_modes=self.modes,
                )
                for coeffs in data.iloc[sample]
            ]

        else:
            raise ValueError("%s space is not supported" % self.space)

        cols = 2 if split else 1
        rows = int(len(components) / cols)

        #  Select fontsize and text kwargs
        if self.space == "fourier":
            fontsize = get_labelsize(int(len(components[0].poloidalModeNumbers) / 2))
            annotate_color = _black_or_white
            kwargs["annotate_kws"] = {
                "fontsize": fontsize,
                "ha": "center",
                "va": "center",
            }
        else:
            fontsize = 8

        if self.num_of_radial_points != 1:
            rho = get_flux_points(self.num_of_radial_points)[self.radial_points[0]]
        else:
            rho = 1.0

        #  Sanitize cbar label
        if not isinstance(cbar_label, list):
            cbar_label = [cbar_label] * len(components)
        if len(cbar_label) != len(components):
            raise ValueError(
                "Given cbar label length, {}, does not match rows length, {}".format(
                    len(cbar_label), len(components)
                )
            )

        #  Sanitize vmax and vmin
        if not isinstance(vmax, list) or not vmax:
            vmax = [vmax] * len(components)
        if not isinstance(vmin, list) or not vmin:
            vmin = [vmin] * len(components)

        for i, component in enumerate(components):

            index = i + 1 if cols == 1 else compute_subplot_index(i, rows)
            axes = self.fig.add_subplot(rows, cols, index)

            #  Build cbar label
            if not cbar_label[i]:
                cbar_label_ = "$" + get_latex_string(data.columns[i]) + "$"
            else:
                cbar_label_ = cbar_label[i]
            cbar_label_ += r"$(s = {:.2f})$".format(rho)
            if self.include_units:
                cbar_label_ += (
                    " ["
                    + get_label_unit(data.columns[i], use_milli=self.use_milli)
                    + "]"
                )

            #  Plot in Fourier space
            if self.space == "fourier":

                artist = component.plot(
                    axes=axes,
                    cbar_label=cbar_label_,
                    annotate=annotate,
                    annotate_fmt=annotate_fmt,
                    vmax=vmax[i],
                    vmin=vmin[i],
                    annotate_color=annotate_color,
                    **kwargs,
                )

                axes.grid(False)
                axes.set_ylabel(ylabel="m")
                axes.set_xlabel(xlabel="n")

            else:

                #  Fix color and levels
                if vmin[i] is not None and vmax[i] is not None:
                    kwargs["levels"] = np.linspace(vmin[i], vmax[i], levels)

                #  Plot in real space
                artist = axes.contourf(
                    self.toroidal_angles / (2 * np.pi),
                    self.poloidal_angles / (2 * np.pi),
                    component,
                    **kwargs,
                )

                #  Annotate average value
                if annotate:
                    axes.text(
                        0.05,
                        0.95,
                        annotate_fmt.format(component.mean()),
                        transform=axes.transAxes,
                        fontsize=fontsize,
                        verticalalignment="top",
                        bbox=dict(boxstyle="round", facecolor="wheat", alpha=0.7),
                    )

                axes.set_ylabel(ylabel=r"$\frac{\theta}{2\pi}$")
                axes.set_xlabel(xlabel=r"$\frac{\varphi}{2\pi}$")
                axes.xaxis.set_major_formatter(lambda x, pos: "{:.2f}".format(x))

            cbar = plt.colorbar(
                artist,
                orientation="horizontal" if rows == 1 else "vertical",
                aspect=40 if rows == 1 else 20,
                format=cbar_format,
            )
            cbar.set_label(
                cbar_label_,
                rotation=0 if rows == 1 else 90,
                labelpad=10,
            )

        plt.subplots_adjust(hspace=hspace)


class MetricFigure(Figure):
    """A generic figure with a computed metric on top.

    The metric_fn is evaluated between the first and second sets of columns.
    """

    def plot(self, data, **kwargs):

        if len(data.columns) == 1:
            raise ValueError("Please provide at least two columns")

        self.establish_kwargs(kwargs.copy())

        #  Parse specific kwargs
        index = kwargs.pop("index", "index")

        if index in data.columns:
            keys = set(data[index].values)
        else:
            data[index] = "0"
            keys = ["0"]

        #  Compute metric
        columns = data.columns[: int(len(data.columns) / 2)]
        values = dict.fromkeys(columns)

        for i, col in enumerate(columns):

            #  Compute metrics per key
            _values = []
            for k in keys:

                indices = data[data[index] == k].index
                trues, preds = map(
                    to_values, [data.iloc[indices]] * 2, [i, i + len(columns)]
                )

                if self.space == "real":
                    #  Compute real space value
                    trues = from_fourier_to_real(
                        trues[:, self.radial_points, :],
                        poloidal_angles=self.poloidal_angles,
                        toroidal_angles=self.toroidal_angles,
                        component=get_label_component(col),
                        modes=get_fourier_modes_from_label(col, self.modes),
                    )
                    preds = from_fourier_to_real(
                        preds[:, self.radial_points, :],
                        poloidal_angles=self.poloidal_angles,
                        toroidal_angles=self.toroidal_angles,
                        component=get_label_component(col),
                        modes=self.modes,
                    )

                _values.append(
                    np.maximum(
                        np.zeros(trues[0].shape),
                        get_metric(self.metric)(
                            preds.reshape(trues.shape[0], -1),
                            trues.reshape(trues.shape[0], -1),
                            multioutput="raw_values",
                        ).reshape(trues[0].shape),
                    )
                )

            values[col] = _values

        if self.kind == "map":
            Map.plot(self, pd.DataFrame(values, index=range(len(keys))), **kwargs)
        elif self.kind == "hist":
            values = [{k: v_} for k, v in values.items() for v_ in v[0].flatten()]
            Hist.plot(self, pd.DataFrame(values), **kwargs)
        elif self.kind == "profile":
            Profile.plot(self, pd.DataFrame(values, index=range(len(keys))), **kwargs)
        else:
            raise RuntimeError("%s requested figure kind is not supported")


class FnFigure(Figure):
    """A figure which maps data with the given map_fn function.

    The given `map_fn` should take as input a ndarray of (#samples, #features),
    and output a ndarray of shape (#features).
    """

    def plot(self, data, **kwargs):

        self.establish_kwargs(kwargs.copy())

        #  Figure specific kwargs
        map_fn = kwargs.pop("map_fn")

        values = to_values(data)

        #  Get parameters to compute real space values
        if self.space == "real":
            #  Placeholder for real space values
            values_ = np.zeros(
                (
                    values.shape[0],
                    len(self.radial_points),
                    len(self.poloidal_angles),
                    len(self.toroidal_angles),
                    values.shape[-1],
                )
            )
            for i, col in enumerate(data.columns):
                values_[..., i] = from_fourier_to_real(
                    values[..., i][:, self.radial_points, :],
                    self.poloidal_angles,
                    self.toroidal_angles,
                    get_label_component(col),
                    modes=get_fourier_modes_from_label(col, None),
                )
            values = values_

        #  Apply map_fn
        values = {
            data.columns[i]: [
                map_fn(values[..., i].reshape(len(data), -1)).reshape(
                    values[..., i].shape[1:]
                )
            ]
            for i in range(values.shape[-1])
        }

        if self.kind == "map":
            kwargs["sample"] = 0
            Map.plot(
                self, pd.DataFrame(values, index=[0], columns=data.columns), **kwargs
            )
        elif self.kind == "profile":
            Profile.plot(
                self, pd.DataFrame(values, index=[0], columns=data.columns), **kwargs
            )
        else:
            raise RuntimeError("%s requested figure kind is not supported")


class CorrelationMap(Map):
    """A fourier `Map` filled with correlation values.

    We assume that the first column in the DataFrame is the feature to
    be correlated with, while the others are the fourier components.
    """

    def plot(self, data, **kwargs):

        self.establish_kwargs(kwargs.copy())

        if len(self.radial_points) != 1:
            raise ValueError("Only 1 radial points could be evaluated")

        correlation = {}
        for col in data.columns[1:]:

            values = to_values(data, col)

            #  Compute correlation for each Fourier coefficient
            col_correlation = np.empty(values.shape[2])
            for i in range(values.shape[2]):
                col_correlation[i] = np.corrcoef(
                    data.iloc[:, 0], values[:, self.radial_points[0], i]
                )[0, 1]

            correlation[col] = [col_correlation]

        #  Set keyword arguments for the Map figure
        kwargs["num_of_radial_points"] = len(self.radial_points)
        kwargs["sample"] = 0
        kwargs["log"] = False

        super(CorrelationMap, self).plot(pd.DataFrame(correlation), **kwargs)


class Surface(Figure):
    """A generic 2D surface figure."""

    def plot(self, data, **kwargs):

        self.establish_kwargs(kwargs)

        #  Figure specific kwargs
        sample = kwargs.pop("sample", random.randint(0, len(data) - 1))
        labels = kwargs.pop("labels", [None])
        xlim = kwargs.pop("xlim", None)
        ylim = kwargs.pop("ylim", None)
        markers = kwargs.pop("markers", ["o"])
        colors = kwargs.pop("colors", ["blue", "red"])
        fillstyles = kwargs.pop("fillstyles", ["full", "full"])
        linestyles = kwargs.pop("linestyles", ["--", "--"])

        rows = int(np.sqrt(len(self.toroidal_angles)))
        cols = int(np.ceil(len(self.toroidal_angles) / rows))

        for i, phi in enumerate(self.toroidal_angles):

            with sns.axes_style({"axes.grid": True}):

                axes = self.fig.add_subplot(rows, cols, i + 1)

                for j in range(int(len(data.columns) / 2)):

                    #  We assume to use original full resolution data as trues
                    rcos = FourierCoefficients.from_array(
                        data.iloc[sample, 2 * j].reshape(self.num_of_radial_points, -1),
                        fourier_modes=None if j == 0 else self.modes,
                    )
                    zsin = FourierCoefficients.from_array(
                        data.iloc[sample, 2 * j + 1].reshape(
                            self.num_of_radial_points, -1
                        ),
                        fourier_modes=None if j == 0 else self.modes,
                    )

                    SurfaceCoefficients(RCos=rcos, ZSin=zsin).plot(
                        phi,
                        linestyle=linestyles[j],
                        marker=markers[j],
                        markersize=5,
                        axes=axes,
                        label=labels[j],
                        color=colors[j],
                        fillstyle=fillstyles[j],
                    )

                axes.set_xlabel(r"$R$ [m]")
                axes.set_ylabel(r"$Z$ [m]")

                if self.title is not None:
                    title = (
                        self.title[i] if isinstance(self.title, list) else self.title
                    )
                else:
                    title = r"$\varphi = {{{:.2f}}}$".format(phi)
                axes.set_title(title, loc="left")

                if xlim:
                    axes.set_xlim(xlim)

                if ylim:
                    axes.set_ylim(ylim)


class Correlation(Figure):
    """A generic heatmap correlation figure."""

    def plot(self, data, **kwargs):

        if not isinstance(data, pd.DataFrame):
            raise ValueError("Input data should be a pandas dataframe")

        correlation = data.corr()

        sns.heatmap(
            correlation,
            mask=np.triu(np.ones_like(correlation, dtype=np.bool)),
            square=True,
            **kwargs,
        )


class Pairplot(Figure):
    """A generic seaborn pairplot figure.

    In case of plotting the relationship between fourier coefficients,
    the user must specify which coefficients to plot via the `fourier`
    kwargs in the plot method.
    """

    def plot(self, data, **kwargs):

        if not isinstance(data, pd.DataFrame):
            raise ValueError("Input data should be a pandas dataframe")

        self.establish_kwargs(kwargs)

        #  Figure specific kwargs
        to_plot = kwargs.pop("to_plot", None)
        hue = kwargs.get("hue", None)

        if self.space == "fourier":

            tags = [
                (r, m, n)
                for r in self.radial_points
                for m in self.poloidal
                for n in self.toroidal
                if not (m == 0 and n < 0)
            ]
            indices = FourierCoefficients.get_indices(
                self.radial_points, self.poloidal, self.toroidal
            )

            fourier_feature_string = "{}_s{}_m{}_n{}"
            for col in list(data.columns):
                for tag, index in zip(tags, indices):
                    data[
                        fourier_feature_string.format(col, tag[0], tag[1], tag[2])
                    ] = data[col].apply(lambda x: x.flatten()[index])

                data.drop(col, axis=1, inplace=True)

        if to_plot is not None:
            #  Add default hue parameter if needed
            if hue is not None:
                data[hue] = ["data"] * len(data)
            data = pd.concat([data, pd.DataFrame(to_plot)])

        self.fig = sns.pairplot(
            data=data,
            **kwargs,
        )


class R2Scatter(Figure):
    """A multi axis figure to compare two sets of data.

    It is generally used to compare true and predicted values for a given data set.
    """

    def plot(self, data, **kwargs):
        """Plot true and predicted values over multiple axis.

        It currently supports only Fourier coefficients as input data, thus
        you must provide which radial points and fouerier modes you would
        like to plot.
        """

        if not isinstance(data, pd.DataFrame):
            raise ValueError("Input data should be a pandaa dataframe")

        self.establish_kwargs(kwargs)

        #  Figure specific kwargs
        hue = kwargs.pop("hue", None)
        bins = kwargs.pop("bins", 4)

        if hue is not None:
            data.iloc[:, data.columns.get_loc(hue)] = pd.cut(
                data[hue].copy(), bins=bins
            )

        #  Plots define what has to be plotted
        plots = [
            (s, m, n)
            for s in self.radial_points
            for m in self.poloidal
            for n in self.toroidal
        ]

        if self.space == "fourier":
            #  Remove requested Fourier coefficients not in range
            plots = [
                p
                for p in plots
                if p[1] < self.modes[0] and np.abs(p[2]) <= self.modes[1]
            ]

        if self.kind == "joint" and len(plots) != 1:
            raise ValueError(
                "For the jointplot we support only plots. "
                + "Number of plots requested is %d",
                len(plots),
            )

        #  Square figure grid
        nrows = get_nrows(len(plots))

        trues, preds = map(to_values, [data] * 2, [0, 1])

        if self.space == "real":
            #  Compute real space value
            trues = from_fourier_to_real(
                trues[:, self.radial_points, :],
                poloidal_angles=self.poloidal_angles,
                toroidal_angles=self.toroidal_angles,
                component=get_label_component(data.columns[0]),
                modes=get_fourier_modes_from_label(data.columns[0], self.modes),
            )
            preds = from_fourier_to_real(
                preds[:, self.radial_points, :],
                poloidal_angles=self.poloidal_angles,
                toroidal_angles=self.toroidal_angles,
                component=get_label_component(data.columns[0]),
                modes=self.modes,
            )

        i = 0
        for srad, mpol, ntor in plots:

            if mpol == 0 and ntor < 0:
                continue

            if self.space == "fourier":
                index = FourierCoefficients.get_indices(
                    0, mpol, ntor, modes=self.modes
                )[0]
                trues_ = trues[:, srad, index]
                preds_ = preds[:, srad, index]
            elif self.space == "real":
                trues_ = trues[:, srad, mpol, ntor]
                preds_ = preds[:, srad, mpol, ntor]
            else:
                trues_ = trues[:, srad, 0]
                preds_ = preds[:, srad, 0]

            ref = np.linspace(min(trues_), max(trues_), len(trues_))
            ylabel = "$observed$"
            xlabel = "$predicted$"

            if self.data_kind == "qq":
                p = np.linspace(0, 100, 100)
                trues_ = np.percentile(trues_, p)
                preds_ = np.percentile(preds_, p)
                ref = np.linspace(min(trues_), max(trues_), len(trues_))
                ylabel = r"$observed \ quantiles$"
                xlabel = r"$predicted \ quantiles$"

            if self.data_kind == "residuals":
                trues_ = preds_ - trues_
                ylabel = r"$residuals$"

            data_ = pd.DataFrame({"trues": trues_, "preds": preds_})
            data_ = pd.concat([data_, data.iloc[:, 2:]], axis=1)

            if self.kind == "joint":

                self.fig = sns.jointplot(
                    data=data_,
                    x="preds",
                    y="trues",
                    hue=hue,
                    height=8,
                    space=0.4,
                    palette=sns.color_palette("deep", n_colors=bins),
                    joint_kws={"color": _CMAP(0.1), **kwargs},
                )
                axes = self.fig.ax_joint
                axes.plot(
                    ref,
                    ref
                    if not self.data_kind == "residuals"
                    else np.zeros(ref.shape[0]),
                    "--",
                    color=_CMAP(0.9),
                    linewidth=1.5,
                )

            else:

                axes = self.fig.add_subplot(nrows, nrows, i + 1)
                axes.plot(
                    ref,
                    ref
                    if not self.data_kind == "residuals"
                    else np.zeros(ref.shape[0]),
                    "--",
                    color=_CMAP(0.9),
                    linewidth=1.5,
                )
                sns.scatterplot(
                    data=data_,
                    x="preds",
                    y="trues",
                    ax=axes,
                    color=_CMAP(0.1),
                    **kwargs,
                )

            if not self.data_kind == "residuals":
                axes.annotate(
                    r"$R^2 = {{{:.3f}}}$".format(r2_score(trues_, preds_)),
                    xy=(0.05, 0.90),
                    xycoords="axes fraction",
                )

            #  Build latex axes title
            rho = get_flux_points(trues.shape[1])[srad]

            if self.space == "fourier":
                title = r"$X^{{{}{}}}(\rho = {:.2f})$".format(mpol, ntor, rho)
            elif self.space == "real":
                title = r"$X({:.2f},{:.2f},{:.2f})$".format(
                    rho, self.poloidal_angles[mpol], self.toroidal_angles[ntor]
                )
            else:
                title = r"$X({:.2f}$".format(rho)

            axes.set_title(title, loc="left")
            axes.set_xlabel(xlabel)
            axes.set_ylabel(ylabel)

            i += 1

        plt.subplots_adjust(hspace=0.3, wspace=0.25)


class BoxPlot(Figure):
    """A wrapper for the seaborn boxplot."""

    def plot(self, data, **kwargs):

        if not isinstance(data, pd.DataFrame):
            raise ValueError("Input data should be a pandas dataframe")

        with sns.axes_style({"axes.grid": True}):
            sns.boxplot(data=data, **kwargs)
            plt.gca().get_yaxis().set_minor_locator(AutoMinorLocator())


class PointPlot(Figure):
    """A wrapper for the seaborn pointplot."""

    def plot(self, data, **kwargs):

        if not isinstance(data, pd.DataFrame):
            raise ValueError("Input data should be a pandas dataframe")

        with sns.axes_style({"axes.grid": True}):
            ax = sns.pointplot(data=data, **kwargs)
            ax.get_yaxis().set_minor_locator(AutoMinorLocator())


class Hyperopt(Figure):
    """A wrapper figure for hyperopt default ones."""

    def _plot_hp(self, data, **kwargs):
        """Plot impact of all hps."""

        include_swarmplot = kwargs.pop("include_swarmplot", False)
        sc_kwargs = kwargs.pop("sc_kwargs", {})
        sw_kwargs = kwargs.pop("sw_kwargs", {})

        exclude = kwargs.pop("exclude", [])
        y = kwargs.pop("y", "loss")
        xs = [c for c in list(data.columns) if c != y and c not in exclude]

        cv_iteration = kwargs.pop("cv_iteration", None)
        if cv_iteration is not None:
            data = data[data["cv_iteration"] == cv_iteration]
            xs = [c for c in xs if c != "cv_iteration"]

        ncols = 3
        nrows = int(np.ceil(len(xs) / ncols))

        for i, x in enumerate(xs):

            axes = self.fig.add_subplot(nrows, ncols, i + 1)

            if isinstance(data[x].iloc[0], float):
                sns.scatterplot(data=data, x=x, y=y, ax=axes, **sc_kwargs)
            else:
                sns.boxplot(data=data, x=x, y=y, ax=axes, **kwargs)
                if include_swarmplot:
                    sns.swarmplot(
                        data=data,
                        x=x,
                        y=y,
                        ax=axes,
                        **sw_kwargs,
                        **kwargs,
                    )

        plt.subplots_adjust(hspace=0.3)

    def plot(self, data, **kwargs):
        """Data is suppose to be a trials object."""

        kind = kwargs.pop("kind", "hp")

        with sns.axes_style({"axes.grid": True}):

            if kind == "history":
                main_plot_history(data, do_show=False)
                plt.gca().get_yaxis().set_minor_locator(AutoMinorLocator())
            elif kind == "vars":
                main_plot_vars(data, do_show=False)
            elif kind == "hp":
                self._plot_hp(data, **kwargs)
            else:
                raise RuntimeError("%s kind plot is not supported" % kind)


def get_figure(figure):
    """Returns figure class from `figure` string.

    Args:
        figure (str): The figure string name.

    Returns:
        The Figure class.
    """

    figures = {
        "R2Scatter": R2Scatter,
        "Map": Map,
        "MetricFigure": MetricFigure,
        "Surface": Surface,
        "Hist": Hist,
        "Scatter": Scatter,
        "Profile": Profile,
        "Closest": Closest,
        "Correlation": Correlation,
        "CorrelationMap": CorrelationMap,
        "Pairplot": Pairplot,
        "BoxPlot": BoxPlot,
        "Line": Line,
        "Hyperopt": Hyperopt,
        "FnFigure": FnFigure,
        "PointPlot": PointPlot,
    }

    return figures[figure]
