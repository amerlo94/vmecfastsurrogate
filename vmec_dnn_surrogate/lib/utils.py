"""Utily functions for the `vmec_dnn_surrogate` research project."""

import logging
import json
import yaml
import subprocess
from os import listdir, makedirs
from os.path import expanduser, isdir, join, isfile, dirname
from csv import writer

from numpy import ones, stack, ravel, vectorize, quantile, concatenate
from numpy.random import choice
from pandas import read_pickle, read_parquet, DataFrame

from w7x.vmec import FourierCoefficients
from vmec_dnn_surrogate.lib.config import (
    FIELD_COMPONENTS,
    NUM_OF_FOURIER_MODES,
    NUM_OF_NYQUIST_FOURIER_MODES,
    NYQUIST_RESOLUTION_LABELS,
)

#  Error strings to match
SLURM_TIMEOUT = "Job step aborted:"

#  Standard log error strings
RUN_FILE_NOT_FOUND = "%s: %s file has not been found"


def get_all_labels():
    """Retrieve a list of all vmec output labels.

    Returns:
        The list of string values for all vmec output labels.
    """

    return [label for labels in FIELD_COMPONENTS.values() for label in labels]


def get_label_component(label):
    """Retrieve if label is a Cos or Sin component.

    Args:
        label (str): The given label.

    Returns:
        The component string in ["cos", "sin"].
    """

    return "cos" if "Cos" in label else "sin"


def get_fourier_modes_from_label(label, modes):
    """If original data is used, leave None since Fourier modes will be inferred."""

    return None if "_orig" in label else modes


def get_num_fourier_modes_from_label(label, use_nyquist_resolution):
    """Return the number of Fourier modes used for each label."""

    if label not in FIELD_COMPONENTS:
        return None

    if not use_nyquist_resolution:
        return NUM_OF_FOURIER_MODES

    if label in NYQUIST_RESOLUTION_LABELS:
        return NUM_OF_NYQUIST_FOURIER_MODES

    return NUM_OF_FOURIER_MODES


def fast_batch_check(folder):
    """Fast check for non-completed runs.

    Args:
        folder (str): The folder where to search for runs.
    """

    if not isdir(folder):
        raise RuntimeError("{} does not exist".format(folder))

    runs = listdir(folder)
    if runs is None:
        raise RuntimeError("No runs found in {}".format(folder))

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("Checking in %s...", folder)

    for run in runs:

        input_file = expanduser(join(folder, run, "input." + run))
        if not isfile(input_file):
            logger.warning(RUN_FILE_NOT_FOUND, run, "input")
            continue

        threed1_file = expanduser(join(folder, run, "threed1." + run))
        if not isfile(threed1_file):
            logger.info(RUN_FILE_NOT_FOUND, run, "threed1")
            continue

        wout_file = expanduser(join(folder, run, "wout_" + run + ".nc"))
        if isfile(wout_file):
            continue

        logger.info("%s: has not been completed", run)

        #  Search for errors
        log_file = expanduser(join(folder, run, "log.txt"))
        with open(log_file, "r") as log:
            log_output = log.read()
            if SLURM_TIMEOUT in log_output:
                logger.info("%s: has been halted due to slurm timeout reached", run)


def read_dataset(dataset, folder, columns=None):
    """Read given dataset based on file format.

    Args:
        dataset: The batch/dataset id to read.
        folder: Where the dataset is stored.
        columns: List of columns to read.

    Returns:
        The read back dataset.
    """

    if isfile(join(folder, dataset + ".df.pkl")):
        return read_pickle(join(folder, dataset + ".df.pkl"))

    if isfile(join(folder, dataset + ".df.parquet")):
        return read_parquet(
            join(folder, dataset + ".df.parquet"), columns=columns, use_threads=False
        )

    raise RuntimeError("{} dataset has not been found".format(dataset))


def store_dataset(dataset, file_path):
    """Store pandas dataframe according to the given format.

    Args:
        dataset: The input dataset.
        file_path: The output file path.
    """

    if file_path.endswith(".pkl"):
        dataset.to_pickle(file_path)
    elif file_path.endswith(".parquet"):
        dataset.to_parquet(file_path)
    elif file_path.endswith(".feather"):
        dataset.to_feather(file_path)
    else:
        raise RuntimeError("File path extension not supported")


def store_dict(dictionary, file_path):
    """Store dictionary as csv file.

    Args:
        dictionary (dict): The input dictionary.
        file_path (str): The output file path.
    """

    with open(file_path, "w") as csvfile:
        wrt = writer(csvfile)
        wrt.writerow(dictionary.keys())
        for val in zip(*dictionary.values()):
            wrt.writerow(val)


def update_dict(d, u):
    """Update `d` dictionary with `u` values."""

    for k, v in d.items():
        if isinstance(v, dict):
            update_dict(d[k], u)
        else:
            if u.get(k, None) is not None:
                d[k] = u[k]

    return d


def read_file(file_path):
    """Read back file from `file_path` based on file extension."""

    if file_path.endswith(".json"):
        with open(file_path, "r") as filename:
            return json.load(filename)
    if file_path.endswith(".yaml") or file_path.endswith(".yml"):
        with open(file_path, "r") as filename:
            return yaml.load(filename, Loader=yaml.FullLoader)

    raise ValueError("File extension not supported")


def write_file(data, file_path):
    """Write data to file based on file extension."""

    makedirs(dirname(file_path), exist_ok=True)

    if file_path.endswith(".json"):
        with open(file_path, "w") as filename:
            json.dump(data, filename)
    else:
        raise ValueError("File extension not supported")


def to_dataframe(*args):
    """Build a Pandas DataFrame from `args`.

    The given `args` are expected to yields dict values to be stored in
    a single dataframe.

    Args:
        args (generator): Generators which yield dict values.

    Returns:
        A pandas DataFrame.
    """

    temp_dict = {}

    #  Build dict first
    for generator in zip(*args):
        for sample in generator:
            for k, v in sample.items():
                if k in temp_dict.keys():
                    temp_dict[k].extend([v])
                else:
                    temp_dict[k] = [v]

    return DataFrame(temp_dict)


def get_fourier_mask(shape, label):
    """Return Fourier mask with zeros at:

        * m=0, n<0
        * r=0, m!=0

    Args:
        shape (tuple): Required output label shape.

    Returns:
        np.ndarray: The fourier mask.
    """

    mask = ones(shape)
    toroidal_modes = int((shape[2] - 1) / 2)

    #  m=0, n<0 coefficients
    mask[:, 0, :toroidal_modes, :] = 0

    #  r=0, m!=0 coefficients
    mask[0, 1:] = 0

    if label == "Space":
        mask[0, 0, :, 1] = 0  # LambdaSin
        mask[:, 0, toroidal_modes, 1:] = 0  # LambdaSin and ZSin

    if label == "Bsup":
        mask[0] = 0  # BsupUCos and BsupVCos on axes

    if label == "Bsub":
        mask[:, 0, toroidal_modes, 0] = 0  # BsubSSin
        mask[0, :, :, 1:] = 0  # BsubUCos and BsubVCos on axes

    if label == "BCos":
        mask[0, :, :, :] = 0  # BCos on axes

    return mask


def from_fourier_to_real(
    coeffs, poloidal_angles, toroidal_angles, component, modes=None
):
    """Evaluate fourier coefficients in real space.

    Args:
        coeffs (np.array): The fourier coefficients to evaluate.
        modes (int or tuple): Number of Fourier modes for coeffs.
        poloidal_angles (np.array): The poloidal angles where to evaluate the coefficients.
        toroidal_angles (np.array): The toroidal angles where to evaluate the coefficients.
        component (str): The fourier component, 'cos' or 'sin'.

    Returns:
        A numpy array with a shape of (#radial_points, #poloidal_angles, #toroidal_angles).
    """

    def to_real_fn(x):
        return FourierCoefficients.from_array(x, modes)(
            range(x.shape[0]), poloidal_angles, toroidal_angles, component
        ).reshape(x.shape[0], len(poloidal_angles), len(toroidal_angles))

    #  Check if coeffs has more then 1 samples
    if len(coeffs.shape) == 3:
        return vectorize(to_real_fn, signature="(n,m)->(i,j,h)")(coeffs)

    return to_real_fn(coeffs)


def to_values(df, labels=None, flatten=False):
    """Extract values from dataframe and stack along last axis.

    Args:
        df (pd.DataFrame): A pandas DataFrame.
        labels (str, int or list): Select the columns to extract.
        flatten (bool): Whether to flatten the values into a 2D arrays.

    Returns:
        The extracted values as numpy array.
    """

    if labels is None:
        labels = df.columns

    if isinstance(labels, int):
        labels = df.columns[labels]

    if isinstance(labels, str):
        return stack(ravel(df[labels].values))

    values = [stack(ravel(df[label].values)) for label in labels]

    if flatten:
        return concatenate([v.reshape(len(df), -1) for v in values], axis=1)

    return stack(values, axis=-1)


def parse_bool(arg):
    """Parse bool string into python bool object."""

    return arg in ["True", "true"]


def parse_none(arg):
    """Return None is `None` string is received."""

    if arg == "None":
        return None
    return arg


def bootstrap_mean_and_ci(a, ci=0.95, n_boot=1000, return_quantiles=False):
    """Compute mean and confidence interval of the input values a.

    Args:
        a (array like): The input data.
        ci (float): The desired confidence interval, float between 0 and 1.
        n_boot (int): The number of bootstrap samples to consider.
        return_quantiles (bool): True if quantiles have to be returned.

    Args:
        The mean, lower and upper range of the ci of the input data.
    """

    samples = choice(a, size=(n_boot, len(a)))

    mean = samples.mean()

    b_q_upper = quantile(samples, axis=1, q=1 - (1 - ci) / 2)
    b_q_lower = quantile(samples, axis=1, q=(1 - ci) / 2)

    q_upper = b_q_upper.mean()
    q_lower = b_q_lower.mean()

    if return_quantiles:
        return mean, q_upper, q_lower

    return mean, (q_upper - q_lower) / 2


def get_git_revision_hash():
    return subprocess.check_output(["git", "rev-parse", "HEAD"]).decode("ascii").strip()


def get_git_revision_short_hash():
    return (
        subprocess.check_output(["git", "rev-parse", "--short", "HEAD"])
        .decode("ascii")
        .strip()
    )


def commit_and_save(arg, filepath):
    """Save object to file, append current commit.

    Args:
        arg (string): String to write to file.
        filepath (string): File path where to write to.
    """

    git_revision = "% commit " + get_git_revision_hash()
    with open(filepath, "w") as file_handle:
        file_handle.write(git_revision + "\n" + arg)


def is_imported(module):
    return module in dir()
