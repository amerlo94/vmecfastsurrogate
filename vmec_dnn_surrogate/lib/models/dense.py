"""A fully connected model."""

from numpy import prod, array, expand_dims
import tensorflow as tf
from vmec_dnn_surrogate.lib.models.model import Model


class DeepDense(Model):
    """A simple fully connected model."""

    def __init__(
        self,
        dense_feature_columns,
        conv_feature_columns,
        output_mask,
        hidden_units,
        hidden_layers=None,
        batch_norm=False,
        dropout=None,
        l2_regularization_factor=0.0,
        activation_fn="relu",
        output_activation_fn="linear",
    ):

        super(DeepDense, self).__init__(
            dense_feature_columns=dense_feature_columns,
            conv_feature_columns=conv_feature_columns,
        )

        #  Save model config
        self.dense_feature_columns = dense_feature_columns
        self.conv_feature_columns = conv_feature_columns
        self.label_mask = output_mask
        self.hidden_units = hidden_units
        self.hidden_layers = hidden_layers
        self.batch_norm = batch_norm
        self.dropout = dropout
        self.l2_regularization_factor = l2_regularization_factor
        self.activation_fn = activation_fn
        self.output_activation_fn = output_activation_fn

        #  Half units at each layer
        if not isinstance(hidden_units, list) and hidden_layers:
            hidden_units_ = [
                hidden_units * 0.5 ** layer for layer in range(hidden_layers)
            ]
        else:
            hidden_units_ = hidden_units

        #  Regression tree
        for i, layer_size in enumerate(hidden_units_):
            self._add_layer(
                tf.keras.layers.Dense(
                    units=layer_size,
                    use_bias=False if self.batch_norm else True,
                    activation="linear",
                    kernel_regularizer=tf.keras.regularizers.l2(
                        self.l2_regularization_factor
                    ),
                    kernel_initializer=tf.compat.v1.glorot_uniform_initializer(),
                    bias_initializer=tf.keras.initializers.Zeros(),
                ),
                "l{}_dense".format(i),
                "tree",
            )

            if self.batch_norm:
                self._add_layer(
                    tf.keras.layers.BatchNormalization(), "l{}_norm".format(i), "tree"
                )

            if self.activation_fn == "leaky_relu":
                self._add_layer(
                    tf.keras.layers.LeakyReLU(), "l{}_act".format(i), "tree"
                )
            else:
                self._add_layer(
                    tf.keras.layers.Activation(self.activation_fn),
                    "l{}_act".format(i),
                    "tree",
                )

            if self.dropout is not None:
                self._add_layer(
                    tf.keras.layers.Dropout(rate=self.dropout),
                    "l{}_drop".format(i),
                    "tree",
                )

        #  Model output
        self.output_dense = tf.keras.layers.Dense(
            units=int(prod(array(output_mask.shape))),
            kernel_regularizer=tf.keras.regularizers.l2(self.l2_regularization_factor),
            kernel_initializer=tf.compat.v1.glorot_uniform_initializer(),
            bias_initializer=tf.keras.initializers.Zeros(),
        )

        if self.output_activation_fn == "leaky_relu":
            self.output_act = tf.keras.layers.LeakyReLU()
        else:
            self.output_act = tf.keras.layers.Activation(self.output_activation_fn)

        #  Mask
        self.mask = tf.constant(
            expand_dims(output_mask.flatten(), axis=0),
            dtype="float32",
        )
        self.out_mask = tf.keras.layers.Multiply()

    def call(self, inputs, training=False):

        #  Model input
        x = self.input_features(inputs)

        #  Regression tree
        for layer_name in self._layers_names["tree"]:
            x = self._get_layer(layer_name)(x, training=training)

        #  Model output
        x = self.output_dense(x)
        x = self.output_act(x)

        #  Apply mask
        x = self.out_mask([x, self.mask])

        return x

    def get_config(self):
        return {
            "dense_feature_columns": self.dense_feature_columns,
            "conv_feature_columns": self.conv_feature_columns,
            "output_mask": self.label_mask,
            "hidden_units": self.hidden_units,
            "hidden_layers": self.hidden_layers,
            "dropout": self.dropout,
            "batch_norm": self.batch_norm,
            "l2_regularization_factor": self.l2_regularization_factor,
            "activation_fn": self.activation_fn,
            "output_activation_fn": self.output_activation_fn,
        }
