"""A 3D Convolutional model."""

import logging
import numpy as np
import tensorflow as tf
from vmec_dnn_surrogate.lib.models.model import Model


def get_num_of_filters(num_of_layers, num_of_filters, factor=0.5):
    """Build list of upsampler filters.

    Args:
        num_of_layers (int): the number of layers.
        num_of_filters (int or list): the number of filters per layer.
        factor (float): the scaling factor to apply at each layer.

    Returns:
        The list of filters per layer.
    """

    if not isinstance(num_of_filters, list):
        return [int(num_of_filters * factor ** i) for i in range(num_of_layers)]

    return num_of_filters


def update_shape(input_shape, kernel, strides):
    """Compute new layer shape after upsampler block.

    Args:
        input_shape (tuple): Input shape.
        kernel (tuple): The kernel size.
        strides (tuple): The strides size.

    Returns:
        The output shape.
    """

    dim = len(input_shape)

    if isinstance(kernel, int):
        kernel = (kernel,) * dim

    if isinstance(strides, int):
        strides = (strides,) * dim

    def compute_fn(t):
        """Compute output shape dimension given tuple of parameters."""
        return t[0] * t[2] + t[1] - t[2]

    return tuple(map(compute_fn, zip(input_shape, kernel, strides)))


class Conv3D(Model):
    """A 3D convolutional model."""

    def __init__(
        self,
        dense_feature_columns,
        conv_feature_columns,
        conv_input_shape,
        output_mask,
        upsampler_layers,
        conv_layers=1,
        conv_filters=8,
        conv_kernel=2,
        conv_strides=1,
        conv_dropout=None,
        conv_activation_fn="relu",
        link_shape=[1, 1, 1],
        upsampler_filters=8,
        upsampler_kernel=3,
        upsampler_strides=1,
        upsampler_dropout=None,
        upsampler_activation_fn="linear",
        use_crop_pad=False,
        batch_norm=False,
        l2_regularization_factor=0.0,
        raise_exception_on_misshape=True,
    ):

        super(Conv3D, self).__init__(
            dense_feature_columns=dense_feature_columns,
            conv_feature_columns=conv_feature_columns,
        )

        #  Save model config
        self.dense_feature_columns = dense_feature_columns
        self.conv_feature_columns = conv_feature_columns

        self.conv_layers = conv_layers
        self.conv_filters = conv_filters
        self.conv_kernel = conv_kernel
        self.conv_strides = conv_strides
        self.conv_dropout = conv_dropout
        self.conv_activation_fn = conv_activation_fn

        self.link_shape = link_shape

        self.upsampler_layers = upsampler_layers
        self.upsampler_filters = upsampler_filters
        self.upsampler_kernel = upsampler_kernel
        self.upsampler_strides = upsampler_strides
        self.upsampler_dropout = upsampler_dropout
        self.upsampler_activation_fn = upsampler_activation_fn

        self.l2_regularization_factor = l2_regularization_factor

        #  Conv tree
        if conv_feature_columns is not None:

            conv_filters = get_num_of_filters(
                self.conv_layers, self.conv_filters, factor=2
            )

            self._add_layer(
                tf.keras.layers.Reshape(conv_input_shape),
                "c0_reshape",
                "input",
            )

            for i in range(self.conv_layers):
                self._add_conv_block(
                    filters=conv_filters[i],
                    kernel=self.conv_kernel,
                    strides=self.conv_strides,
                    batch_norm=batch_norm,
                    activation_fn=self.conv_activation_fn,
                    l2_regularization_factor=self.l2_regularization_factor,
                    dropout=self.conv_dropout,
                    prefix="c{}".format(i),
                    layer_list="input",
                )

            #  Flatten before concatenation
            self._add_layer(
                tf.keras.layers.Flatten(),
                "c0_flatten",
                "input",
            )

        self._add_layer(
            tf.keras.layers.Reshape((*tuple(self.link_shape), -1)), "l1_reshape", "tree"
        )

        output_shape = output_mask.shape
        layer_shape = self.link_shape
        upsampler_filters = get_num_of_filters(
            self.upsampler_layers, self.upsampler_filters
        )

        #  Upsampler
        for i in range(self.upsampler_layers):

            activation_fn, batch_norm_, filters, dropout = (
                ("linear", False, output_shape[-1], None)
                if i == self.upsampler_layers - 1 and use_crop_pad
                else (
                    self.upsampler_activation_fn,
                    batch_norm,
                    upsampler_filters[i],
                    self.upsampler_dropout,
                )
            )

            self._add_upsampler_block(
                filters=filters,
                kernel=self.upsampler_kernel,
                strides=self.upsampler_strides,
                batch_norm=batch_norm_,
                activation_fn=activation_fn,
                l2_regularization_factor=self.l2_regularization_factor,
                dropout=dropout,
                prefix="u{}".format(i),
                layer_list="tree",
            )

            layer_shape = update_shape(
                layer_shape,
                kernel=self.upsampler_kernel,
                strides=self.upsampler_strides,
            )

        #  Crop/Pad or Conv3D layer
        if use_crop_pad:
            for i, (out_dim, in_dim) in enumerate(zip(output_shape[:-1], layer_shape)):
                self._add_dim_resize(
                    out_dim=out_dim,
                    in_dim=in_dim,
                    dim=i,
                    exception=raise_exception_on_misshape,
                )
        else:
            #  Match output shape with kernel
            kernel = tuple(
                map(
                    lambda x, y: y - x + 1 if y >= x else 1,
                    output_shape[:-1],
                    layer_shape,
                )
            )
            self._add_layer(
                tf.keras.layers.Conv3D(
                    output_shape[-1],
                    kernel,
                    strides=1,
                    padding="valid",
                    kernel_regularizer=tf.keras.regularizers.l2(
                        self.l2_regularization_factor
                    ),
                    kernel_initializer=tf.initializers.GlorotUniform(),
                    bias_initializer=tf.keras.initializers.Zeros(),
                    use_bias=True,
                ),
                "u{}_conv".format(self.upsampler_layers),
                "tree",
            )

        #  Mask
        self.mask = tf.constant(np.expand_dims(output_mask, axis=0), dtype="float32")
        self.out_mask = tf.keras.layers.Multiply()

    def call(self, inputs, training=False):

        #  Model input
        if self._layers_names["input"] != []:

            x_dense, x_conv = self.input_features(inputs)

            #  Input tree
            for layer_name in self._layers_names["input"]:
                x_conv = self._get_layer(layer_name)(x_conv, training=training)

            #  Concatenate branches
            x = tf.keras.layers.concatenate([x_dense, x_conv])

        else:
            x = self.input_features(inputs)

        #  Pad tensor to match link_shape
        if self.link_shape != [1, 1, 1]:
            x = tf.pad(
                x, [[0, 0], [0, self.upsampler_filters - x.shape[1]]], "CONSTANT"
            )

        #  Model tree
        for layer_name in self._layers_names["tree"]:
            x = self._get_layer(layer_name)(x, training=training)

        #  Model output
        x = self.out_mask([x, self.mask])

        return x

    def _add_dim_resize(self, out_dim, in_dim, dim, exception):
        """Add cropping or padding layer for given dimension."""

        if out_dim == in_dim:
            return

        layer = tf.keras.layers.Cropping3D
        suffix = "_crop"
        if out_dim > in_dim:

            if exception:
                raise ValueError(
                    "%d dimension is smaller then output one, %d < %d"
                    % (dim, in_dim, out_dim)
                )

            layer = tf.keras.layers.ZeroPadding3D
            suffix = "_pad"

            #  Warn user, but hopefully things still work
            logger = logging.getLogger(__name__)
            logger.info(
                "{} dimension is smaller then output one, {} < {}".format(
                    dim, in_dim, out_dim
                )
            )

        #  Radial dimension
        if dim == 0:
            diff = ((np.absolute(out_dim - in_dim), 0), 0, 0)
        elif dim == 1:
            diff = (0, (0, np.absolute(out_dim - in_dim)), 0)
        elif dim == 2:
            total_diff = np.absolute(out_dim - in_dim)
            half_diff = int(total_diff / 2.0)
            diff = (0, 0, (half_diff, total_diff - half_diff))

        self._add_layer(layer(diff), "o{}{}".format(dim, suffix), "tree")

    def _add_reducer_block(
        self,
        size,
        batch_norm,
        activation_fn,
        l2_regularization_factor,
        dropout,
        prefix,
        layer_list,
    ):
        """Add a reducer block to the tree.

        Args:
            size (int): Layer size.
            batch_norm (bool): Add a Batch Normalization layer.
            activation_fn (str): The activation function.
            l2_regularization_factor (float): L2 regularization factor.
            dropout (float): The dropout rate.
            prefix (str): The layer name prefix.
            layer_list (str): The list where to append the layer.
        """

        self._add_layer(
            tf.keras.layers.Dense(
                units=size,
                use_bias=not batch_norm,
                activation="linear",
                kernel_regularizer=tf.keras.regularizers.l2(l2_regularization_factor),
                kernel_initializer=tf.compat.v1.glorot_uniform_initializer(),
                bias_initializer=tf.keras.initializers.Zeros(),
            ),
            "{}_dense".format(prefix),
            layer_list,
        )

        if batch_norm:
            self._add_layer(
                tf.keras.layers.BatchNormalization(),
                "{}_norm".format(prefix),
                layer_list,
            )

        if activation_fn == "leaky_relu":
            self._add_layer(
                tf.keras.layers.LeakyReLU(), "{}_act".format(prefix), layer_list
            )
        else:
            self._add_layer(
                tf.keras.layers.Activation(activation_fn),
                "{}_act".format(prefix),
                layer_list,
            )

        if dropout is not None:
            self._add_layer(
                tf.keras.layers.Dropout(rate=dropout),
                "{}_drop".format(prefix),
                layer_list,
            )

    def _add_upsampler_block(
        self,
        filters,
        kernel,
        strides,
        batch_norm,
        activation_fn,
        l2_regularization_factor,
        dropout,
        prefix,
        layer_list,
    ):
        """Add an upsampler block to the tree.

        Args:
            filters (tuple/int): Number of filters to use.
            kernel (tuple/int): Convolutional kernel size.
            strides (tuple/int): Convolutional strides size.
            batch_norm (bool): Add a Batch Normalization layer.
            activation_fn (str): The layer activation function.
            l2_regularization_factor (float): L2 regularization factor.
            dropout (float): The layer dropout rate.
            prefix (str): The layer name prefix.
            layer_list (str): The list where to append the layer.
        """

        self._add_layer(
            tf.keras.layers.Conv3DTranspose(
                filters,
                kernel,
                strides=strides,
                padding="valid",
                kernel_regularizer=tf.keras.regularizers.l2(l2_regularization_factor),
                kernel_initializer=tf.initializers.GlorotUniform(),
                bias_initializer=tf.keras.initializers.Zeros(),
                use_bias=not batch_norm,
            ),
            "{}_conv".format(prefix),
            layer_list,
        )

        if batch_norm:
            self._add_layer(
                tf.keras.layers.BatchNormalization(),
                "{}_norm".format(prefix),
                layer_list,
            )

        if activation_fn == "leaky_relu":
            self._add_layer(
                tf.keras.layers.LeakyReLU(), "{}_act".format(prefix), layer_list
            )
        else:
            self._add_layer(
                tf.keras.layers.Activation(activation_fn),
                "{}_act".format(prefix),
                layer_list,
            )

        if dropout is not None:
            self._add_layer(
                tf.keras.layers.SpatialDropout3D(rate=dropout),
                "{}_drop".format(prefix),
                layer_list,
            )

    def _add_conv_block(
        self,
        filters,
        kernel,
        strides,
        batch_norm,
        activation_fn,
        l2_regularization_factor,
        dropout,
        prefix,
        layer_list,
    ):
        """Add a convolutional block to the tree.

        Args:
            filters (tuple/int): Number of filters to use.
            kernel (tuple/int): Convolutional kernel size.
            strides (tuple/int): Convolutional strides size.
            batch_norm (bool): Add a Batch Normalization layer.
            activation_fn (str): The layer activation function.
            l2_regularization_factor (float): L2 regularization factor.
            dropout (float): The layer dropout rate.
            prefix (str): The layer name prefix.
            layer_list (str): The list where to append the layer.
        """

        self._add_layer(
            tf.keras.layers.Conv1D(
                filters,
                kernel,
                strides=strides,
                padding="valid",
                kernel_regularizer=tf.keras.regularizers.l2(l2_regularization_factor),
                kernel_initializer=tf.initializers.GlorotUniform(),
                bias_initializer=tf.keras.initializers.Zeros(),
                use_bias=not batch_norm,
            ),
            "{}_conv".format(prefix),
            layer_list,
        )

        if batch_norm:
            self._add_layer(
                tf.keras.layers.BatchNormalization(),
                "{}_norm".format(prefix),
                layer_list,
            )

        if activation_fn == "leaky_relu":
            self._add_layer(
                tf.keras.layers.LeakyReLU(), "{}_act".format(prefix), layer_list
            )
        else:
            self._add_layer(
                tf.keras.layers.Activation(activation_fn),
                "{}_act".format(prefix),
                layer_list,
            )

        if dropout is not None:
            self._add_layer(
                tf.keras.layers.Dropout(rate=dropout),
                "{}_drop".format(prefix),
                layer_list,
            )

    def get_config(self):
        return {
            "dense_feature_columns": self.dense_feature_columns,
            "conv_feature_columns": self.conv_feature_columns,
            "link_shape": self.link_shape,
            "upsampler_layers": self.upsampler_layers,
            "upsampler_filters": self.upsampler_filters,
            "upsampler_kernel": self.upsampler_kernel,
            "upsampler_strides": self.upsampler_strides,
            "upsampler_dropout": self.upsampler_dropout,
            "upsampler_activation_fn": self.upsampler_activation_fn,
        }
