"""An abstract keras model."""

import tensorflow as tf


def _count_params(weights):
    """Count the total number of parameters in weights."""

    return int(sum([tf.keras.backend.count_params(w) for w in weights]))


class Model(tf.keras.Model):
    """An abstract keras model."""

    def __init__(self, dense_feature_columns, conv_feature_columns=None):

        super(Model, self).__init__()

        #  Model input
        self.dense_features = tf.keras.layers.DenseFeatures(
            feature_columns=dense_feature_columns
        )

        self.conv_features = None
        if conv_feature_columns is not None:
            self.conv_features = tf.keras.layers.DenseFeatures(
                feature_columns=conv_feature_columns
            )

        self._layers_names = {"input": [], "tree": []}

    def input_features(self, inputs):
        """Input layers which handle dense and conv inputs."""

        if self.conv_features is None:
            return self.dense_features(inputs)

        def _get_features_inputs(inputs, features):
            """Returns the inputs for the given Densefeatures layer."""
            return {
                k: v
                for k, v in inputs.items()
                if k in [f.key for f in features._feature_columns]
            }

        dense_inputs, conv_inputs = map(
            _get_features_inputs,
            (inputs, inputs),
            (self.dense_features, self.conv_features),
        )

        return self.dense_features(dense_inputs), self.conv_features(conv_inputs)

    @classmethod
    def from_config(cls, config):
        return cls(**config)

    def _add_layer(self, layer, layer_name, layer_list):
        """Add layer to model."""

        setattr(self, layer_name, layer)
        self._layers_names[layer_list].append(layer_name)

    def _get_layer(self, layer_name):
        """Retrieve layer by name."""

        return getattr(self, layer_name)

    def _get_model(self):
        """Retrieve model via the 'Functional API'."""

        inputs = {
            f.key: tf.keras.Input(f.shape, dtype=f.dtype) for f in self.feature_columns
        }
        return tf.keras.Model(inputs=inputs, outputs=self.call(inputs))

    def summary(self):
        """Call keras Model summary on 'Functional API' model."""

        self._get_model().summary()

    def plot_model(self, **kwargs):
        """Plot model to file."""

        tf.keras.utils.plot_model(model=self._get_model(), **kwargs)

    @property
    def feature_columns(self):
        """All the feature columns for the mode."""

        if self.conv_features is None:
            return self.dense_features._feature_columns

        return (
            self.dense_features._feature_columns + self.conv_features._feature_columns
        )

    @property
    def trainable_params(self):
        """Number of trainable parameters."""

        return _count_params(self.trainable_weights)

    @property
    def non_trainable_params(self):
        """Number of non trainable parameters."""

        return _count_params(self.non_trainable_params)

    @property
    def total_params(self):
        """Total number of parameters."""

        return self.trainable_params + self.non_trainable_params
