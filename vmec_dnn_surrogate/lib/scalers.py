"""Custom scalers with the sklearn.preprocessing APIs."""

import numpy as np
from sklearn.preprocessing import MinMaxScaler, PowerTransformer


class MinMaxPowerTransformer:
    """A PowerTransformer scaler on top of a MinMaxScaler."""

    #  TODO(@amerlo): Check these ranges
    def __init__(self, feature_range=(1.0, 2.0), method="box-cox"):
        """The init function."""

        self._feature_range = feature_range

        self._minmax = MinMaxScaler(feature_range=feature_range)
        self._power = PowerTransformer(method=method)

    def fit(self, X):
        """Compute the minimum, maximum and lambda values user for later scaling."""

        self._minmax.fit(X)

        X_ = self._minmax.transform(X)
        self._indices = np.all(X_ == X_[0, :], axis=0)

        self._power.fit(X_[:, np.logical_not(self._indices)])

        return self

    @property
    def data_min_(self):
        """Per feature minimum seen in the data."""

        return self._minmax.data_min_

    @property
    def data_max_(self):
        """Per feature maximum seen in the data."""

        return self._minmax.data_max_

    @property
    def lambdas_(self):
        """The parameters of the power transformation for the selected features."""

        lambdas = np.ones(self._indices.shape[0], dtype=np.float32)
        lambdas[np.logical_not(self._indices)] = self._power.lambdas_

        return lambdas

    @property
    def min_(self):
        """The min value used for MinMaxScaler."""

        return np.ones(len(self._indices), dtype=np.float32) * self._feature_range[0]

    @property
    def max_(self):
        """The max value used for MinMaxScaler."""

        return np.ones(len(self._indices), dtype=np.float32) * self._feature_range[1]
