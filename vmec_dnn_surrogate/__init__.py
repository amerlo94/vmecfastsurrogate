"""Top-level package of vmec_dnn_surrogate."""

__author__ = """Andrea Merlo"""
__email__ = "amerlo@ipp.mpg.de"
__version__ = "0.0.1"
