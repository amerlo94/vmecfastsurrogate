"""An `Experiment` object to keep track and reproduce experiments."""

import time
from os import makedirs, system
from os.path import join, isdir, isfile
from shutil import rmtree
import numbers
import logging
import glob

import numpy as np
from sklearn.preprocessing import (
    StandardScaler,
    MinMaxScaler,
    RobustScaler,
    PowerTransformer,
)
from sklearn.decomposition import PCA
from scipy.interpolate import interp1d
from pandas import DataFrame, read_pickle, read_csv, concat
import tensorflow as tf
from hyperopt import fmin, hp, tpe, space_eval, Trials, STATUS_OK, STATUS_FAIL
from hyperopt.pyll import scope

from vmec_dnn_surrogate.make_figures import get_figure
from vmec_dnn_surrogate.lib.models.dense import DeepDense
from vmec_dnn_surrogate.lib.models.conv3d import Conv3D
from vmec_dnn_surrogate.lib.dataset import (
    VmecDataset,
    get_label_radial_indices,
    get_flux_points,
)
from vmec_dnn_surrogate.lib.vmec import compute_shear
from vmec_dnn_surrogate.lib.scalers import MinMaxPowerTransformer
from vmec_dnn_surrogate.lib.metrics import get_metric
from vmec_dnn_surrogate.lib.config import (
    FIELD_COMPONENTS,
    POLOIDAL_ANGLES,
    TOROIDAL_ANGLES,
    NUM_OF_RADIAL_POINTS,
    NUM_OF_PROFILE_USED_POINTS,
)
from vmec_dnn_surrogate.lib.utils import (
    update_dict,
    write_file,
    to_dataframe,
    get_fourier_mask,
    from_fourier_to_real,
    get_label_component,
    to_values,
    get_num_fourier_modes_from_label,
    bootstrap_mean_and_ci,
    commit_and_save,
    is_imported,
)

#  Initialize horovod
_LOCAL_RANK = 0
_RANK = 0
try:
    import horovod.tensorflow.keras as hvd

    hvd.init()
    _LOCAL_RANK = hvd.local_rank()
    _RANK = hvd.rank()
except ImportError as e:
    print(e)

gpus = tf.config.experimental.list_physical_devices("GPU")
for gpu in gpus:
    try:
        tf.config.experimental.set_memory_growth(gpu, True)
    except (ValueError, RuntimeError, IndexError) as e:
        print(e)

if gpus:
    tf.config.experimental.set_visible_devices(gpus[_LOCAL_RANK], "GPU")

#  Keras verbosity
_KERAS_VERBOSE = 0


#  Disable tf warning.
tf.get_logger().setLevel("ERROR")


def _get_hp_config_values(config, hp):
    """Extract hp values from config."""

    return config["_".join(["hp", hp])]


def get_hidden_units_hp(num_of_layers, *args):
    """Build hp parameter for hidden layers.

    Args:
        num_of_layers (int): Number of hidden unit layers.
        *args (int): Arguments for the hp distribution.

    Returns:
        A dictionary of number of nodes in layer hp.
    """

    params = {}
    for n in range(num_of_layers):
        params["hidden_units_{}".format(n)] = scope.int(
            hp.quniform("hidden_units_{}_{}".format(num_of_layers, n), *args)
        )

    return params


def get_dense_hp_space(hp_values):
    """Build hp space for Dense model.

    Args:
        hp_values (dict): A dict for hp config.

    Returns:
        The hp dict.
    """

    min_hidden_layers = 2
    max_hidden_layers = 5

    space = {}

    if "hp_hidden_units_dict" in hp_values:
        space["hidden_units_dict"] = hp.choice(
            "hidden_units_dict",
            [
                get_hidden_units_hp(
                    n, *_get_hp_config_values(hp_values, "hidden_units_dict")
                )
                for n in range(min_hidden_layers, max_hidden_layers)
            ],
        )

    if "hp_hidden_units" in hp_values:
        space["hidden_units"] = scope.int(
            hp.quniform(
                "hidden_units",
                *_get_hp_config_values(hp_values, "hidden_units"),
            )
        )

    if "hp_hidden_layers" in hp_values:
        space["hidden_layers"] = scope.int(
            hp.quniform(
                "hidden_layers",
                *_get_hp_config_values(hp_values, "hidden_layers"),
            )
        )

    if "hp_l2_regularization_factor" in hp_values:
        space["l2_regularization_factor"] = hp.uniform(
            "l2_regularization_factor",
            *_get_hp_config_values(hp_values, "l2_regularization_factor"),
        )

    if "hp_activation_fn" in hp_values:
        space["activation_fn"] = hp.choice(
            "activation_fn",
            _get_hp_config_values(hp_values, "activation_fn"),
        )

    if "hp_output_activation_fn" in hp_values:
        space["output_activation_fn"] = hp.choice(
            "output_activation_fn",
            _get_hp_config_values(hp_values, "output_activation_fn"),
        )

    return space


def get_conv3d_hp_space(hp_values):
    """Build hp space for Conv3D model.

    Args:
        hp_values (dict): A dict for hp config.

    Returns:
        The hp dict.
    """

    max_hidden_layers = 2

    space = {}

    if "hp_hidden_units_dict" in hp_values:
        space["hidden_units_dict"] = hp.choice(
            "hidden_units_dict",
            [
                get_hidden_units_hp(
                    n, *_get_hp_config_values(hp_values, "hidden_units_dict")
                )
                for n in range(max_hidden_layers)
            ],
        )

    if "hp_link_shape" in hp_values:
        space["link_shape"] = hp.choice(
            "link_shape",
            _get_hp_config_values(hp_values, "link_shape"),
        )

    if "hp_upsampler_layers" in hp_values:
        space["upsampler_layers"] = scope.int(
            hp.quniform(
                "upsampler_layers",
                *_get_hp_config_values(hp_values, "upsampler_layers"),
            )
        )

    if "hp_upsampler_filters" in hp_values:
        space["upsampler_filters"] = scope.int(
            hp.quniform(
                "upsampler_filters",
                *_get_hp_config_values(hp_values, "upsampler_filters"),
            )
        )

    if "hp_upsampler_kernel" in hp_values:
        space["upsampler_kernel"] = scope.int(
            hp.quniform(
                "upsampler_kernel",
                *_get_hp_config_values(hp_values, "upsampler_kernel"),
            )
        )

    if "hp_upsampler_strides" in hp_values:
        space["upsampler_strides"] = scope.int(
            hp.quniform(
                "upsampler_strides",
                *_get_hp_config_values(hp_values, "upsampler_strides"),
            )
        )

    if "hp_reducer_activation_fn" in hp_values:
        space["reducer_activation_fn"] = hp.choice(
            "activation_fn", _get_hp_config_values(hp_values, "reducer_activation_fn")
        )

    if "hp_upsampler_activation_fn" in hp_values:
        space["upsampler_activation_fn"] = hp.choice(
            "output_activation_fn",
            _get_hp_config_values(hp_values, "upsampler_activation_fn"),
        )

    if "hp_l2_regularization_factor" in hp_values:
        space["l2_regularization_factor"] = hp.uniform(
            "l2_regularization_factor",
            *_get_hp_config_values(hp_values, "l2_regularization_factor"),
        )

    if "hp_reducer_dropout" in hp_values:
        space["reducer_dropout"] = hp.uniform(
            "reducer_dropout",
            *_get_hp_config_values(hp_values, "reducer_dropout"),
        )

    if "hp_upsampler_dropout" in hp_values:
        space["upsampler_dropout"] = hp.uniform(
            "upsampler_dropout",
            *_get_hp_config_values(hp_values, "upsampler_dropout"),
        )

    if "hp_conv_layers" in hp_values:
        space["conv_layers"] = scope.int(
            hp.quniform(
                "conv_layers",
                *_get_hp_config_values(hp_values, "conv_layers"),
            )
        )

    if "hp_conv_filters" in hp_values:
        space["conv_filters"] = scope.int(
            hp.quniform(
                "conv_filters",
                *_get_hp_config_values(hp_values, "conv_filters"),
            )
        )

    if "hp_conv_kernel" in hp_values:
        space["conv_kernel"] = scope.int(
            hp.quniform(
                "conv_kernel",
                *_get_hp_config_values(hp_values, "conv_kernel"),
            )
        )

    if "hp_conv_strides" in hp_values:
        space["conv_strides"] = scope.int(
            hp.quniform(
                "conv_strides",
                *_get_hp_config_values(hp_values, "conv_strides"),
            )
        )

    if "hp_conv_activation_fn" in hp_values:
        space["conv_activation_fn"] = hp.choice(
            "conv_activation_fn",
            _get_hp_config_values(hp_values, "conv_activation_fn"),
        )

    if "hp_conv_dropout" in hp_values:
        space["conv_dropout"] = hp.uniform(
            "conv_dropout",
            *_get_hp_config_values(hp_values, "conv_dropout"),
        )

    return space


def get_hp_space(config):
    """Build hp space.

    Args:
        config (dict): Experiment config.

    Returns:
        A dict of hp parameters.
    """

    space = {}

    if "hp_learning_rate" in config["hp"]:
        space["learning_rate"] = hp.uniform(
            "learning_rate", *_get_hp_config_values(config["hp"], "learning_rate")
        )

    if "hp_decay_rate" in config["hp"]:
        space["decay_rate"] = hp.uniform(
            "decay_rate", *_get_hp_config_values(config["hp"], "decay_rate")
        )

    if "hp_decay_steps" in config["hp"]:
        space["decay_steps"] = scope.int(
            hp.quniform(
                "decay_steps", *_get_hp_config_values(config["hp"], "decay_steps")
            )
        )

    if "hp_early_stopping_patience" in config["hp"]:
        space["early_stopping_patience"] = scope.int(
            hp.quniform(
                "early_stopping_patience",
                *_get_hp_config_values(config["hp"], "early_stopping_patience"),
            )
        )

    if "hp_batch_size" in config["hp"]:
        space["batch_size"] = scope.int(
            hp.quniform(
                "batch_size", *_get_hp_config_values(config["hp"], "batch_size")
            )
        )

    if "hp_optmizer" in config["hp"]:
        space["optimizer"] = hp.choice(
            "optimizer", _get_hp_config_values(config["hp"], "optimizer")
        )

    #  Model specific parameters
    if config["estimator"]["estimator_name"] == "DeepDense":
        space = {**space, **get_dense_hp_space(config["hp"]["DeepDense"])}

    if config["estimator"]["estimator_name"] == "Conv3D":
        space = {**space, **get_conv3d_hp_space(config["hp"]["Conv3D"])}

    return space


def space_update(space):
    """Update hp space to be used in experiment."""

    #  DeepDense model
    if "hidden_units_dict" in space:
        space["hidden_units"] = list(space["hidden_units_dict"].values())

    #  Conv3D model
    if "hidden_units" in space:
        space["reducer_hidden_units"] = space["hidden_units"]

    return space


def from_trials_to_df(space, trials, to_include=None):
    """Build pandas DataFrame from list of hyperopt trials object."""

    trials_list = []

    #  Hack to be able to evaluate space hp
    for t in trials:
        trial = {key: val[0] for key, val in t["misc"]["vals"].items() if val != []}

        #  Eval space hp
        trial = space_eval(space, trial)

        #  Unravel dicts
        for k, v in trial.copy().items():
            if isinstance(v, dict):
                del trial[k]
                trial = {**trial, **v}

        if to_include is not None:
            for result in to_include:
                if result in t["result"]:
                    trial[result] = t["result"][result]
                else:
                    trial[result] = None

        trials_list.append(trial)

    #  This is a hack to fix hidden_units dict
    hidden_units_keys = set(
        [k for t in trials_list for k in t.keys() if k.startswith("hidden_units_")]
    )
    for t in trials_list:
        for k in hidden_units_keys:
            if k not in t.keys():
                t[k] = 0

    columns = trials_list[0].keys()
    df = {c: [t[c] for t in trials_list] for c in columns}

    return DataFrame(df)


def get_hp_from_trials_df(data, hp=None):
    """Extract hp columns from dataframe.

    Clean some hack and provide dataframe with selected hps.

    Args:
        data (pd.DataFrame): Pandas Dataframe which stores the trials.
        hps (list): List of hp strings to extract.

    Returns:
        The Pandas DataFrame with the selected hps.
    """

    #  Clean the hack to handle hidden units dict.
    hidden_units_cols = [
        c for c in sorted(data.columns) if c.startswith("hidden_units_")
    ]
    data["hidden_units_dict"] = data[hidden_units_cols].values.tolist()
    data["hidden_units_dict"] = data["hidden_units_dict"].apply(
        lambda x: [int(v) for v in x if (not np.isnan(v)) and v != 0]
    )

    #  Hack to parse link_shape as int values.
    if "link_shape" in data:
        data["link_shape"] = data["link_shape"].apply(lambda x: eval(x))

    if hp is None:
        hp = data.columns

    data = data[hp]

    if "hidden_units_dict" in data.columns:
        data.loc[:, "hidden_units"] = data.pop("hidden_units_dict")

    return data


def get_dataset(dataset):
    """Get dataset instance."""

    if dataset == "VmecDataset":
        return VmecDataset

    raise ValueError("{} dataset is not supported".format(dataset))


def get_optimizer(optimizer, learning_rate):
    """Build keras optimizer.

    Args:
        optimizer (str): Optimizer to build.
        learning_rate (double): The learning rate.

    Returns:
        A tf.keras.optimizers object.
    """

    #  Adjust learning rate
    if is_imported("hvd"):
        learning_rate = learning_rate * hvd.size()

    optimizers = {
        "Adam": tf.keras.optimizers.Adam(learning_rate=learning_rate),
        "Adagrad": tf.keras.optimizers.Adagrad(learning_rate=learning_rate),
        "RMSProp": tf.keras.optimizers.RMSprop(learning_rate=learning_rate),
        "SGD": tf.keras.optimizers.SGD(learning_rate=learning_rate),
    }

    if is_imported("hvd"):
        return hvd.DistributedOptimizer(optimizers[optimizer])

    return optimizers[optimizer]


def get_loss(loss, weights=None):
    """Get keras loss.

    If loss is a Keras loss function identifier, pass as it is.

    Args:
        loss (str): Loss function identifier.
        weights (tf.Tensor): Optional loss weights.

    Returns:
        The loss function.
    """

    def weighted_mse(y_true, y_pred):

        return tf.python.keras.backend.mean(
            tf.python.ops.math_ops.multiply(
                tf.python.ops.math_ops.squared_difference(y_pred, y_true),
                weights,
            )
        )

    if loss == "weighted_mse":
        return weighted_mse

    return loss


def get_scaler(scaler, **kwargs):
    """Get scaler object given scaler name."""

    if scaler == "StandardScaler":
        return StandardScaler()

    if scaler == "MinMaxScaler":
        return MinMaxScaler(feature_range=(-1, 1))

    if scaler == "RobustScaler":
        return RobustScaler()

    if scaler == "BoxCox":
        return PowerTransformer(method="box-cox")

    if scaler == "MinMaxBoxCox":
        return MinMaxPowerTransformer(method="box-cox")

    if scaler == "PCA":
        n_components = kwargs.pop("n_components", 60)
        return PCA(n_components=n_components)

    raise ValueError("Requested scaler %s is not supported" % scaler)


def get_scaler_fit_parameters(scaler):
    """Get scaler fit parameters.

    Args:
        scaler (sklearn.preprocessing): The input sklearn scaler.

    Returns:
        A tuple of array like scaler fit parameters.
    """

    if isinstance(scaler, MinMaxScaler):
        return (scaler.data_min_, scaler.data_max_)

    if isinstance(scaler, StandardScaler):
        return (scaler.mean_, scaler.scale_)

    if isinstance(scaler, RobustScaler):
        return (scaler.center_, scaler.scale_)

    if isinstance(scaler, PowerTransformer):
        return scaler.lambdas_

    if isinstance(scaler, MinMaxPowerTransformer):
        return (
            scaler.data_min_,
            scaler.data_max_,
            scaler.lambdas_,
            scaler.min_,
            scaler.max_,
        )

    if isinstance(scaler, PCA):
        return scaler.mean_, scaler.components_

    raise ValueError("Requested scaler %s is not supported" % scaler)


def make_figure(figure, data, figure_kwargs, plot_kwargs, file_path):
    """Generate figure.

    Args:
        figure (make_figures.class): Figure class in make_figure.
        data (object): Data object to pass to figure plot method.
        figure_kwargs (dict): Figure kwargs.
        plot_kwargs (dict): Figure plot kwargs.
        file_path (str): File path where to save figure.
    """

    fig = figure(**figure_kwargs)
    fig.plot(data, **plot_kwargs)
    fig.save(file_path)


def yield_predictions(data, batch_size=None):
    """Yield predictions dict as for tf.estimator.

    If batch_size is None, yield single samples.

    Args:
        data (numpy.ndarrays): The predicted numpy arrays.
        batch_size (int): The batch size to use.

    Yields:
        Predictions dictionary.
    """

    if batch_size is None:
        for i in range(data.shape[0]):
            yield {"predictions": data[i]}
    else:
        num_of_batches = int(np.ceil(data.shape[0] / batch_size))

        for i in range(num_of_batches):
            if i == num_of_batches - 1:
                yield {"predictions": data[i * batch_size :, :]}
            else:
                yield {"predictions": data[i * batch_size : (i + 1) * batch_size, :]}


def add_suffix_to_keys(sample, suffix):
    """Add `suffix` to each key of sample dict."""

    return {k + suffix: v for k, v in sample.items()}


def get_n_estimator_hp(ensemble, n):
    """Extract n-th estimator hp from ensemble config.

    Ensemble hp values are stored in dict with `ensemble_<key>` format.

    Args:
        ensemble (dict): The input dict of list values.
        n (int): The index of the elements to extract.

    Returns:
        A dict with a single element per `ensemble` keys.
    """

    config = {}

    for k, v in ensemble.items():
        config[k.split("_", 1)[1]] = v[n]

    return config


def get_fourier_weights(shape):
    """Get scalar coefficients to weight the loss contribution of model outputs.

    Args:
        shape (tuple): Model output shape.

    Returns:
        A list of weights mapping 1:1 model outputs.
    """

    weights = np.ones(shape)

    return tf.constant(weights, dtype=tf.float32)


def get_label_on_full_grid(label, ns=NUM_OF_RADIAL_POINTS):
    """Interpolate predicted label on full grid."""
    s = get_flux_points(label.shape[0])
    f = interp1d(s, label.flatten(), kind="cubic")
    label_on_full_grid = f(get_flux_points(ns))
    return label_on_full_grid


class TimingCallback(tf.keras.callbacks.Callback):
    """Keras callback to monitor timing for each epoch."""

    def __init__(self, logs=None):
        self.logs = []

    def on_epoch_begin(self, epoch, logs=None):
        self.starttime = time.time()

    def on_epoch_end(self, epoch, logs=None):
        self.logs.append(time.time() - self.starttime)


class LRTensorBoardCallback(tf.keras.callbacks.Callback):
    """Keras callback to monitor learning rate into TensorBoard."""

    def __init__(self, model, logs=None):
        self._model = model

    def on_epoch_end(self, epoch, logs=None):
        tf.summary.scalar(
            "learning_rate",
            data=tf.python.keras.backend.eval(self._model.optimizer.lr),
            step=epoch,
        )


class Experiment:
    """An `Experiment` object."""

    def __init__(
        self,
        config,
        task,
        dataset,
        estimator,
        hp,
        reports,
        figures,
    ):
        """Initialize an `Experiment` object.

        Args:
            config (dict): The experiment configuration.
            task (dict): The task definition.
            estimator (dict): The estimator configuration.
            hp (dict): The hyperparameters configuration.
            dataset (dict): The dataset configuration.
            reports (dict): Dictionary of reports to generate.
            figures (dict): Dictionary of figures to generate.
        """

        self._logger = logging.getLogger(__name__)

        self._test_df = None
        self._assess_df = None
        self._trials_df = None
        self._ensemble_df = None

        self._action = "train"  # default action

        self._config = config
        self._task = task
        self._hp = hp

        self._reports = reports
        self._figures = figures

        self._dataset_config = dataset
        self._dataset = get_dataset(dataset["dataset_name"])(
            **dataset["dataset_kwargs"],
            num_samples=self._config["num_samples"],
            test_size=self._config["test_size"],
            val_size=self._config["val_size"],
            validation_mode=self._config["validate"]["validation_mode"],
        )

        self._estimator_config = estimator
        self._current_estimator_index = 0
        self._current_ensemble_indices = list(range(self.num_estimators))
        self._estimator = self._build_estimator()

        self._populate_dirs()

        self._logger.info("Experiment for task %s has been built.", self.task_string)
        self._logger.info("Dataset includes %d total runs.", len(self.dataset))

    def _populate_dirs(self):
        """Build experiment directories and dump experiment config."""

        for folder in [self.reports_dir, self.figures_dir]:
            if not isdir(folder):
                makedirs(folder)

        write_file(
            self.experiment_config,
            join(self.reports_dir, self.task_string + ".json"),
        )

    def _make_model_dir(self):
        """Make model dir."""

        if self._config["resume"]:
            if not isdir(self.model_dir):
                self._logger.warning("%s model folder does not exist", self.model_dir)
        else:
            rmtree(self.model_dir, ignore_errors=True)
            makedirs(self.model_dir)

    def _build_estimator(self):
        """Build estimator from config.

        An experiment object supports both tensorflow estimators and
        keras models. Due to documentation and legacy with
        previous works, a keras models is not converted into an estimator,
        but use it as it is.

        Returns:
            A tf.estimator or tf.keras.Model.
        """

        #  Clear Keras session to release memory before building a new model.
        tf.keras.backend.clear_session()

        estimator_kwargs = self._estimator_config["estimator_kwargs"].copy()

        if self._estimator_config["estimator_name"] != "BaselineRegressor":
            estimator_kwargs[
                "dense_feature_columns"
            ] = self.dataset.get_feature_columns(
                features=self.dense_features,
                use_coil_currents_ratio=self._estimator_config[
                    "use_coil_currents_ratio"
                ],
                use_nyquist_resolution=self.use_nyquist_resolution,
            )
            if self.conv_features != []:
                estimator_kwargs[
                    "conv_feature_columns"
                ] = self.dataset.get_feature_columns(
                    features=self.conv_features,
                    use_coil_currents_ratio=self._estimator_config[
                        "use_coil_currents_ratio"
                    ],
                    use_nyquist_resolution=self.use_nyquist_resolution,
                )
            else:
                estimator_kwargs["conv_feature_columns"] = None

        estimator_kwargs["optimizer"] = get_optimizer(
            self._estimator_config["optimizer"], self._estimator_config["learning_rate"]
        )

        run_config = tf.estimator.RunConfig(
            model_dir=self.model_dir, log_step_count_steps=1000
        )

        if self._estimator_config["estimator_name"] == "DNNRegressor":
            estimator_kwargs["label_dimension"] = int(
                np.prod(np.array(self.label_shape))
            )
            return tf.estimator.DNNRegressor(
                **estimator_kwargs, model_dir=self.model_dir, config=run_config
            )

        if self._estimator_config["estimator_name"] == "BaselineRegressor":
            return tf.estimator.BaselineRegressor(
                **estimator_kwargs, model_dir=self.model_dir, config=run_config
            )

        #  Keras models
        optimizer = estimator_kwargs.pop("optimizer")

        if self.fourier_modes is not None:
            estimator_kwargs["output_mask"] = get_fourier_mask(
                self.label_shape, self.label
            )
        else:
            estimator_kwargs["output_mask"] = np.ones(self.label_shape)

        if self._estimator_config["estimator_name"] == "DeepDense":
            model = DeepDense(**estimator_kwargs)

        if self._estimator_config["estimator_name"] == "Conv3D":
            estimator_kwargs["conv_input_shape"] = (
                NUM_OF_PROFILE_USED_POINTS,
                len(self.conv_features),
            )
            model = Conv3D(**estimator_kwargs)

        loss_weights = get_fourier_weights(self.label_shape)
        loss = get_loss(self._config["loss"], loss_weights)

        model.compile(optimizer=optimizer, loss=loss)

        return model

    def _is_keras_model(self):
        """Evaluate if saved self._estimator is a keras model.

        Returns:
            True is self._estimator is a tf.keras.Model object.
        """

        return isinstance(self._estimator, tf.keras.Model)

    def perform(self, action):
        """Perform action."""

        self._logger.info("Performing %s", action)

        #  TODO(@amerlo): Hack to not update model_dir based on action
        if action != "evaluate":
            self._action = action

        return getattr(self, action)()

    def run(self):
        """Run all `actions`."""

        for action in self._config["actions"]:
            if action not in self.actions:
                raise RuntimeError("%s action is not supported" % action)

            self.perform(action)

    def get_input_fn(self, mode, shuffle=False, take_samples=-1):
        """Retrieve input_fn from dataset with config parameters."""

        return lambda: self.dataset.input_fn(
            mode=mode,
            shuffle=shuffle,
            batch_size=self.batch_size,
            features=self.features,
            label=self.label,
            metadata=self.metadata,
            radial_points=self.radial_points,
            fourier_modes=self.fourier_modes,
            scale_label=self._estimator_config["scale_label"],
            flatten=self._is_label_flatten(),
            use_coil_currents_ratio=self._estimator_config["use_coil_currents_ratio"],
            use_nyquist_resolution=self.use_nyquist_resolution,
            take_samples=take_samples,
        )

    def _train_estimator(self):
        """Train and evaluate estimator."""

        train_hooks = [
            tf.estimator.CheckpointSaverHook(self.model_dir, save_steps=5000)
        ]

        train_spec = tf.estimator.TrainSpec(
            input_fn=self.train_input_fn, hooks=train_hooks
        )
        eval_spec = tf.estimator.EvalSpec(input_fn=self.eval_input_fn)

        tf.estimator.train_and_evaluate(
            estimator=self.estimator, train_spec=train_spec, eval_spec=eval_spec
        )

        #  TODO(@amerlo): Return val_loss
        return {"status": STATUS_OK, "loss": None}

    def _train_model(self):
        """Train and evaluate keras model."""

        checkpoint_file_path = join(self.model_dir, self.task_string + ".h5")

        file_writer = tf.summary.create_file_writer(join(self.model_dir, "train"))
        file_writer.set_as_default()

        timing_callback = TimingCallback()
        callbacks = [
            tf.keras.callbacks.EarlyStopping(
                monitor="val_loss",
                verbose=_KERAS_VERBOSE,
                patience=self._estimator_config["early_stopping_patience"],
                min_delta=0.0001,
                restore_best_weights=True,
            ),
            tf.keras.callbacks.ReduceLROnPlateau(
                monitor="val_loss",
                factor=self._estimator_config["decay_rate"],
                min_lr=self._estimator_config["min_lr"],
                patience=self._estimator_config["decay_steps"],
                verbose=_KERAS_VERBOSE,
            ),
            tf.keras.callbacks.TensorBoard(
                log_dir=self.model_dir, histogram_freq=10, profile_batch=0
            ),
            timing_callback,
            LRTensorBoardCallback(model=self.estimator),
        ]

        if is_imported("hvd"):
            callbacks.append(hvd.callbacks.BroadcastGlobalVariablesCallback(0))

        if _RANK == 0:
            callbacks.append(
                tf.keras.callbacks.ModelCheckpoint(
                    checkpoint_file_path,
                    monitor="val_loss",
                    verbose=_KERAS_VERBOSE,
                    save_best_only=True,
                    save_weights_only=True,
                )
            )

        self._logger.info("Training model at %s ..." % self.model_dir)

        history = self.estimator.fit(
            self.train_input_fn(),
            epochs=self.num_epochs,
            verbose=_KERAS_VERBOSE,
            callbacks=callbacks,
            validation_data=self.eval_input_fn(),
        )

        self.estimator.plot_model(
            to_file=join(self.model_dir, self.task_string + ".png"),
            show_shapes=True,
            expand_nested=True,
            dpi=192,
        )

        return {
            "status": STATUS_OK,
            "loss": np.min(history.history["val_loss"]),
            "time": sum(timing_callback.logs),
            "epochs": len(timing_callback.logs),
            "params": self.estimator.trainable_params,
        }

    def train(self, log_stdout=True, log_json=True):
        """Train and evaluate estimator/model.

        Args:
            log_stdout (bool): Log training metrics to stdout.
            log_json (bool): Log training metrics to json file.
        """

        if self.experiment_config["estimator"]["ensemble"]:
            metrics = self._train_ensemble()
        else:
            metrics = self._train_single()

        if log_stdout:
            for metric, loss in metrics.items():
                if isinstance(loss, float):
                    self._logger.info("{:30s}: {:.4e}".format(metric, loss))
                else:
                    self._logger.info("{:30s}: {}".format(metric, loss))

        if log_json:
            write_file(
                metrics,
                join(self.reports_dir, self._config["train_file_name"]),
            )

        return metrics

    def _train_ensemble(self):
        """Train and evaluate ensemble of estimators/models."""

        metrics = []
        ensemble_hp = self.experiment_config["estimator"]["ensemble_hp"]

        current_estimator_index = self._current_estimator_index

        for i, index in enumerate(self.current_ensemble_indices):

            self._logger.info("Training %d nth estimator from ensemble...", i)

            self._current_estimator_index = index + current_estimator_index
            self._make_model_dir()
            self._set(get_n_estimator_hp(ensemble_hp, i))
            metrics.append(self._train_single())

        #  TODO(@amerlo): This is an hack to validate an ensemble model.
        self._current_estimator_index = current_estimator_index

        return {
            "status": STATUS_OK,
            "loss": np.array([m["loss"] for m in metrics]).mean(),
            "time": np.array([m["time"] for m in metrics]).mean(),
            "epochs": np.array([m["epochs"] for m in metrics]).mean(),
        }

    def _train_single(self):
        """Train and evaluate single estimator/model."""

        self._make_model_dir()

        if self._is_keras_model():
            return self._train_model()

        return self._train_estimator()

    def _get_trues_and_preds(self, df, space):
        """Retrieve data from test dataframe represented in the given space.

        Args:
            df (pandas.DataFrame): Dataframe where to take the samples.
            space (str): The space where to represent the data.

        Returns:
            Tuple of trues and preds data.
        """

        dataframe = df.copy()

        if space == "fourier" or not self.fourier_modes:

            return map(
                to_values, [dataframe, dataframe], [self.labels, self.labels_pred]
            )

        #  Compute value in real space
        if space == "real":

            #  Use original full resolution data for real space
            trues, preds = map(
                to_values, [dataframe, dataframe], [self.labels_orig, self.labels_pred]
            )

            trues = np.stack(
                [
                    from_fourier_to_real(
                        trues[..., i],
                        POLOIDAL_ANGLES,
                        TOROIDAL_ANGLES,
                        get_label_component(col),
                        modes=None,
                    )
                    for i, col in enumerate(self.labels)
                ],
                axis=-1,
            )
            preds = np.stack(
                [
                    from_fourier_to_real(
                        preds[..., i],
                        POLOIDAL_ANGLES,
                        TOROIDAL_ANGLES,
                        get_label_component(col),
                        modes=self.fourier_modes,
                    )
                    for i, col in enumerate(self.labels)
                ],
                axis=-1,
            )

            return trues, preds

        raise ValueError("Space %s is not supported" % space)

    def evaluate(self, data=None, save_df=True, log_stdout=True, log_json=True):
        """Evaluate model over test data.

        Args:
            data (pd.DataFrame): Data to be evaluated.
            save_df (bool): Save test dataframe to file.
            log_stdout (bool): Log evaluate results to stdout.
            log_json (bool): Log evaluate results to json file.
        """

        if not data:
            data = self.test_df

        metrics = self._evaluate(
            data=data, eval_spec=self._config["evaluate"]["eval_spec"]
        )

        if save_df:
            data.to_pickle(join(self.model_dir, "test.pkl"))

        if log_stdout:
            for metric, loss in metrics.items():
                self._logger.info("{:30s}: {:.4e}".format(metric, loss))

        if log_json:
            write_file(
                {k: float(v) for k, v in metrics.items()},
                join(self.reports_dir, self._config["evaluate"]["eval_file_name"]),
            )

        return metrics

    def _evaluate(self, data, eval_spec):
        """Evaluate model over input data.

        Returns the loss value & metrics values for the model given the data from
        input_fn.

        `eval_spec` must be dictionary of spaces and metrics. The supported
        spaces are:

            * data    : the data on which the model is trained on.
            * fourier : the data scaled back to match vmec outputs.
            * real    : if supported by the label, the real space values.

        Args:
            input_fn (pd.DataFrame): Input data.
            eval_spec (dict): Dictionary of spaces and metrics to be evaluated.

        Returns:
            Dictionary of the evaluated metrics values.
        """

        if not self._is_keras_model():
            raise RuntimeError(
                "The model evaluation is supported for Keras models only."
            )

        values = {}

        for space, metrics in eval_spec.items():

            #  TODO(@amerlo): Fix eval model in data space for ensemble
            if space == "data" and not self.experiment_config["estimator"]["ensemble"]:

                #  TODO(@amerlo): Fix data according to data arg.
                values.update(
                    {
                        "test_loss": self.estimator.evaluate(
                            self.test_input_fn(), verbose=_KERAS_VERBOSE
                        )
                    }
                )

            else:

                #  Retrieve data values.
                trues, preds = self._get_trues_and_preds(data, space)

                values.update(
                    {
                        "{}_{}".format(space, metric): get_metric(metric)(
                            preds.reshape(trues.shape[0], -1),
                            trues.reshape(trues.shape[0], -1),
                        )
                        for metric in metrics
                    }
                )

        #  Include mean residuals over target space
        residual_space = self.experiment_config["config"]["evaluate"]["residual_space"]
        if residual_space is not None:
            trues, preds = map(
                lambda x: x.reshape(len(data), -1),
                self._get_trues_and_preds(data, space=residual_space),
            )
            scale = trues.std(axis=0)[None, :]
            data["residuals"] = np.divide(
                np.abs(np.subtract(preds, trues)),
                scale,
                out=np.zeros_like(trues),
                where=scale != 0,
            ).mean(axis=1)

        return values

    def _predict_single_model(self, dataset):
        """Predict single keras model estimator."""

        predictions = self.estimator.predict(dataset, verbose=_KERAS_VERBOSE)
        return yield_predictions(predictions)

    def _predict_ensemble_model(self, input_fn):
        """Predict ensemble of keras models."""

        ensemble_hp = self.experiment_config["estimator"]["ensemble_hp"]
        current_estimator_index = self._current_estimator_index

        #  Use small batch size to reduce memory usage.
        data = input_fn().unbatch().batch(8)

        predictions = []

        for i, index in enumerate(self.current_ensemble_indices):

            self._logger.info("Warm-up model...")
            self._set(get_n_estimator_hp(ensemble_hp, i))
            self.estimator.evaluate(data, verbose=_KERAS_VERBOSE)

            self._current_estimator_index = index + current_estimator_index
            if not isdir(self.model_dir):
                raise RuntimeError(
                    "Model %d has not been found in %s" % (i, self.model_dir)
                )

            checkpoint_file_path = join(self.model_dir, self.task_string + ".h5")

            self._logger.info(
                "Loading %d nth estimator from %s", i, checkpoint_file_path
            )
            self.estimator.load_weights(checkpoint_file_path)

            predictions.append(self.estimator.predict(data, verbose=_KERAS_VERBOSE))

        #  TODO(@amerlo): This is an hack to validate and esemble model.
        self._current_estimator_index = current_estimator_index

        return yield_predictions(np.average(predictions, axis=0))

    def predict(self, input_fn):
        """Generates output predictions for test data set.

        The output predictions are yielded as single examples.

        Yields:
            The output predicted samples.
        """

        if self._is_keras_model():

            if self.experiment_config["estimator"]["ensemble"]:
                return self._predict_ensemble_model(input_fn)

            return self._predict_single_model(input_fn())

        if self.experiment_config["estimator"]["ensemble"]:
            raise RuntimeError("Estimator does not yet support ensemble learning.")

        return self.estimator.predict(input_fn=input_fn, yield_single_examples=True)

    def get_task_hp(self):
        """Get pandas DataFrame of task hp."""

        def _parse_function(func):
            """Parse activation function into plain text string."""

            _strings = {"selu": "SeLU", "relu": "ReLU", "leaky_relu": "Leaky ReLU"}
            return _strings.get(func, func)

        def _parse_kernel(size):
            """Parse kernel and stride into string."""

            if isinstance(size, int):
                size = [size] * 3
            return "$" + r" \times ".join(map(str, size)) + "$"

        estimator_config = self.experiment_config["estimator"]
        estimator_kwargs = estimator_config["estimator_kwargs"]

        #  Extract hps
        hp = []
        if estimator_config["estimator_name"] == "DeepDense":
            hp += [
                ("dense layers", estimator_kwargs["hidden_layers"]),
                ("first layer hidden units", estimator_kwargs["hidden_units"]),
                (
                    "activation function",
                    _parse_function(estimator_kwargs["activation_fn"]),
                ),
            ]

        if estimator_config["estimator_name"] == "Conv3D":
            if "conv_features" in estimator_config:
                hp += [
                    ("encoder layers", estimator_kwargs["conv_layers"]),
                    ("encoder first layer filters", estimator_kwargs["conv_filters"]),
                    (
                        "encoder kernel size",
                        _parse_kernel(estimator_kwargs["conv_kernel"]),
                    ),
                    (
                        "encoder stride",
                        _parse_kernel(estimator_kwargs["conv_strides"]),
                    ),
                    (
                        "encoder activation function",
                        _parse_function(estimator_kwargs["conv_activation_fn"]),
                    ),
                    ("encoder dropout", estimator_kwargs["conv_dropout"]),
                ]

            hp += [
                ("decoder layers", estimator_kwargs["upsampler_layers"]),
                ("decoder first layer filters", estimator_kwargs["upsampler_filters"]),
                (
                    "decoder kernel size",
                    _parse_kernel(estimator_kwargs["upsampler_kernel"]),
                ),
                (
                    "decoder stride",
                    _parse_kernel(estimator_kwargs["upsampler_strides"]),
                ),
                (
                    "decoder activation function",
                    _parse_function(estimator_kwargs["upsampler_activation_fn"]),
                ),
                ("decoder dropout", estimator_kwargs["upsampler_dropout"]),
            ]

        hp += [
            ("batch size", estimator_config["batch_size"]),
            ("learning rate", estimator_config["learning_rate"]),
            ("learning rate decay rate", estimator_config["decay_rate"]),
            ("learning rate decay steps", estimator_config["decay_steps"]),
            (
                r"$L^2$ regularization factor",
                estimator_kwargs["l2_regularization_factor"],
            ),
            (
                "early stopping patience epochs",
                estimator_config["early_stopping_patience"],
            ),
        ]

        return DataFrame.from_records(hp, columns=["HP", "value"])

    def summary(self):
        """Generate task summary stats."""

        def _format(arg):
            if isinstance(arg, str):
                return arg
            return r"\num{" + str(arg) + "}"

        def _float_format(arg):
            return r"\num{" + "{:.4e}".format(arg) + "}"

        hp_table = self.get_task_hp().to_latex(
            index=False,
            formatters={"value": _format},
            float_format=_float_format,
            escape=False,
            column_format="lc",
        )
        commit_and_save(
            hp_table, join(self.reports_dir, "hp-" + self.task_string + ".tex")
        )

    def _make_scalers(self):
        """Fit scalers to data."""

        dataset = self.raw_ds(
            [
                tf.estimator.ModeKeys.TRAIN,
                tf.estimator.ModeKeys.EVAL,
                tf.estimator.ModeKeys.PREDICT,
            ]
        )
        dataframe = to_dataframe(dataset.as_numpy_iterator())

        return {
            var: {
                "name": self._config["scalers"][var],
                "scaler": get_scaler(
                    self._config["scalers"][var],
                    n_components=self.experiment_config["estimator"].get(
                        "num_components", None
                    ),
                ).fit(np.stack(data.values).reshape(len(dataframe), -1)),
            }
            for var, data in dataframe.iteritems()
            if var in self._config["scalers"]
        }

    def make_scalers(self):
        """Fit data set and save scalers fit parameters."""

        def _reshape_scaler_data(data, variable, features, scaler):
            """Helper function to reshape tuple data from scaler parameters.

            Scaler fit parameters are flatten 1d array, where we would like to
            store 2d array in order to be used in the scale_fn. Thus reshape
            here the 2d array and return lists to be JSON serializable.

            Args:
                data (tuple): The input tuple of scaler parameters.
                variable (str): The scaler variable.
                features (list): List of feature variables.
                scaler (str): Scaler name.

            Returns:
                A tuple of list scaler parameters.
            """

            value_to_insert = 0 if scaler != "MinMaxBoxCox" else 1

            def _reshape_feature_fn(data):
                """Reshape feature data function."""
                return data.tolist()

            def _reshape_label_fn(data):
                """Reshape and pad label data function."""

                #  Hack to handle non Fourier labels.
                if self.fourier_modes is None:
                    return _reshape_feature_fn(data)

                data = data.reshape(self.orig_label_shape[0], -1)
                data = np.insert(
                    data,
                    [0]
                    * get_num_fourier_modes_from_label(
                        self.label, self.use_nyquist_resolution
                    )[1],
                    value_to_insert,
                    axis=1,
                )
                return data.reshape(self.orig_label_shape).tolist()

            def _reshape_pca_fn(data):
                """Reshape data for the PCA decomposition."""

                if len(data.shape) == 1:
                    return _reshape_label_fn(data)

                return np.apply_along_axis(_reshape_label_fn, 1, data).tolist()

            if variable in features:
                reshape_fn = _reshape_feature_fn
            elif scaler == "PCA":
                reshape_fn = _reshape_pca_fn
            else:
                reshape_fn = _reshape_label_fn

            return tuple(map(reshape_fn, data))

        scalers = self._make_scalers()

        scalers_parameters = {
            v: {
                "name": scaler["name"],
                "data": _reshape_scaler_data(
                    get_scaler_fit_parameters(scaler["scaler"]),
                    v,
                    self.features,
                    scaler["name"],
                ),
            }
            for v, scaler in scalers.items()
        }

        #  Dump scalers to json file
        scalers_file_path = self._dataset_config["dataset_kwargs"].get(
            "scalers_file",
            join(self.reports_dir, self._task["beta_scenario"], "scalers.json"),
        )
        write_file(scalers_parameters, scalers_file_path)

        self._logger.info("Scalers file has been written to %s.", scalers_file_path)

        return scalers

    def _parse_exp_obj(self, obj):
        """Parse string into experiment object."""

        if isinstance(obj, list):
            return list(map(lambda x: self._parse_exp_obj(x), obj))

        if not isinstance(obj, str):
            return obj

        #  TODO(@amerlo): Use regexp!
        #  Catch regular string
        #  Catch f-string to format number
        if not ("{" in obj and "}" in obj) or ":" in obj:
            return obj

        try:
            parsed = obj.format(
                radial_points=self.radial_points,
                fourier_modes=self.fourier_modes,
                features=self.features,
                label=self.label,
                labels=self.labels,
                labels_pred=self.labels_pred,
                labels_orig=self.labels_orig,
                best=self.best_test_sample,
                median=self.median_test_sample,
                worst=self.worst_test_sample,
            )
        except IndexError:
            return None
        except KeyError:
            return obj

        #  Try to parse obj
        try:
            if "[" in parsed and "]" in parsed:
                return list(map(int, parsed.strip("][").split(", ")))
            return int(parsed)
        except ValueError:
            return parsed

    def make_figures(self):
        """Generate figures as from experiment config.

        Figures to be generated are specified in the experiment.yaml file,
        in the `figures` field.

        Figure classes and documentations are in the `lib/figures.py` file.
        """

        figures_to_plot = self._config["figures_to_plot"]
        figures_name = list(map(lambda x: self._parse_exp_obj(x), self._figures.keys()))

        #  Check for not supported figures
        if figures_to_plot is not None:
            for fig in figures_to_plot:
                if fig not in figures_name:
                    raise RuntimeError("Requested figure %s is not available" % fig)

        for name, config in self._figures.items():

            #  Check if we can skip figure
            if figures_to_plot is None and not config.pop("to_plot", True):
                continue

            #  Check if figure has been requested
            figure_name = self._parse_exp_obj(name)
            if figures_to_plot is not None and figure_name not in figures_to_plot:
                continue

            data = eval(config["figure_data"])

            if data is not None:

                if "data_columns" in config:
                    data = data[
                        [
                            self._parse_exp_obj(c)
                            for c in config["data_columns"]
                            if self._parse_exp_obj(c) is not None
                        ]
                    ]

                plot_kwargs = {
                    k: self._parse_exp_obj(v) for k, v in config["plot_kwargs"].items()
                }
                figure_name = self._parse_exp_obj(name)

                make_figure(
                    get_figure(config["figure"]),
                    data,
                    config["figure_kwargs"],
                    plot_kwargs,
                    join(self.figures_dir, "{}.pdf".format(figure_name)),
                )

    def _is_label_flatten(self):
        """Check if dataset output label has to be flatten."""

        if self._estimator_config["estimator_name"] == "DeepDense":
            return True

        if self._estimator_config["estimator_name"] == "DNNRegressor":
            return True

        return False

    def _set(self, hp):
        """Set hp values into experiment."""

        #  Update hp view
        if hp is not None:
            hp = space_update(hp)
            config = update_dict(self.experiment_config, hp)
        else:
            config = self.experiment_config

        self._config = config["config"]

        self._estimator_config = config["estimator"]
        self._estimator = self._build_estimator()

        self._test_df = None

    def _set_and_train(self, hp):
        """Set hp values and train."""

        try:
            self._set(hp)
            return self.train(log_stdout=False, log_json=False)
        except ValueError as e:
            self._logger.info(
                "Model has not been set due to some exception:\n {}".format(e)
            )
            return {"status": STATUS_FAIL}

    def _set_and_evaluate(self, hp=None, log_stdout=False, log_json=False):
        """Set hp values, train and evaluate."""

        train_metrics = self._set_and_train(hp)

        #  Do not evaluate model if it has not been built.
        if train_metrics["status"] == STATUS_FAIL:
            return train_metrics

        return {
            **train_metrics,
            **self.evaluate(log_stdout=log_stdout, log_json=log_json),
        }

    def _search(self, hp, iteration):
        """Perform a single search iteration."""

        self._logger.info("Search best model in %d-th outer iteration...", iteration)

        def _search_fn(hp):
            """Inner search function.

            Each point in the hp space is evaluated over a different bootstrapped
            split for the train plus val samples.
            """

            self.dataset.update_partition()
            self._current_estimator_index += 1

            return {
                **self._set_and_evaluate(hp),
                "estimator_index": self._current_estimator_index,
            }

        trials = Trials()
        best = fmin(
            _search_fn,
            hp,
            algo=tpe.suggest,
            trials=trials,
            max_evals=self._config["search"]["search_iterations"],
        )
        best = space_eval(hp, best)

        #  Update with loss result
        best_hp = {**best, **trials.best_trial["result"]}

        self._logger.info(
            "Best hyperparameters configuration for %d iteration:", iteration
        )
        for hyperparam, value in best_hp.items():
            self._logger.info("{:30s}: {}".format(hyperparam, value))

        trials = [t for t in trials if t["result"]["status"] != "fail"]

        trials_df = from_trials_to_df(
            hp,
            trials,
            to_include=[
                "loss",
                "params",
                "epochs",
                "time",
                "test_loss",
                "fourier_rmsdstd",
                "fourier_rmsdiqr",
                "fourier_r2score",
                "estimator_index",
            ],
        )

        #  Add experiment info
        trials_df["cv_iteration"] = [iteration] * len(trials_df)
        trials_df["estimator"] = [self._estimator_config["estimator_name"]] * len(
            trials_df
        )
        trials_df["resolution"] = [self.label_shape_string] * len(trials_df)
        trials_df["task"] = [self.task_string] * len(trials_df)

        return best, trials_df

    def search(self):
        """Search for best configuration in hp space.

        There are two search modes supported:
        * same: in this mode, the same data set partition will be used for all the
                model evaluations.
        * KFold: in this mode, kfold-nth splits will be performed on the data set,
                 and an optimized model for each splits will be searched. Thus
                 the actual number of search iterations is
                 "search_iterations" * kfold-splits (usually 5)

        Args:
            evaluate_best (bool): If True, set best configuration as experiment estimator
                evaluate it.
        """

        hp_space = get_hp_space(self.experiment_config)

        if self._config["search"]["search_mode"] == "KFold":
            cv_iterations = int(1 / self._config["test_size"])
        elif self._config["search"]["search_mode"] == "same":
            #  Use only first cross-validation split
            cv_iterations = 1
            self.dataset._validation_mode = "KFold"

        #  Reset data set partitions generator
        self.dataset.set_partition_fn(
            inner_iterations=self._config["search"]["search_iterations"]
        )

        searches = []
        search_file_path = join(self.reports_dir, "search.csv")

        for i in range(cv_iterations):

            best, trials_df = self._search(hp_space, i)
            searches.append(trials_df)

            #  Make sure we consumed the generator.
            assert (
                self.dataset._partition_counter
                == (i + 1) * self._config["search"]["search_iterations"]
            )

            #  Save search results at each cross-validation iteration
            concat(searches, sort=False).to_csv(
                search_file_path, index_label="inner_iteration"
            )

        self._trials_df = concat(searches, sort=False)

    def validate(self, log_stdout=True, log_json=True):
        """Validate estimator with given validation mode.

        Args:
            log_stdout (bool): Log validate results to stdout.
            log_json (bool): Log validate results to json file.
        """

        cv_iterations = self._config["validate"]["validation_iterations"]
        inner_iterations = self._config["validate"]["inner_iterations"]

        #  Read back models from search
        trials_hp = None
        if self.experiment_config["config"]["validate"]["validation_model"] == "best":

            if (
                self.experiment_config["config"]["validate"]["validation_mode"]
                == "repeatedKFold"
            ):
                raise ValueError(
                    "The best validataion model is not compatible "
                    "with the repeated KFold validation strategy."
                )

            trials_hp = get_hp_from_trials_df(
                data=self.trials_df.copy(),
                hp=list(get_hp_space(self.experiment_config).keys())
                + ["cv_iteration", "loss"],
            )

        #  Reset data set partitions generator
        self.dataset.set_partition_fn(inner_iterations=inner_iterations)

        #  In case of repeatedKFold, outer and inner iterations are swapped
        if (
            self.experiment_config["config"]["validate"]["validation_mode"]
            == "repeatedKFold"
        ):
            cv_iterations, inner_iterations = (inner_iterations, cv_iterations)

        iterations = []
        for i in range(cv_iterations):

            self._logger.info("Evaluating model over %d outer cv iteration...", i)

            #  Pick best model to validate
            hp = None
            if trials_hp is not None:
                cv_trials = trials_hp[trials_hp["cv_iteration"] == i]
                hp = cv_trials.loc[cv_trials["loss"].idxmin()].to_dict()
                #  Hack to fix numpy.int64 to int
                for k in ["upsampler_kernel", "conv_kernel"]:
                    if k in hp:
                        hp[k] = int(hp[k])
                hp.pop("loss")
                self._logger.info(f"Using model with hp: {hp}")

            for k in range(inner_iterations):

                self._logger.info("Evaluating model over %d inner cv iteration...", k)

                self._current_estimator_index = (
                    i * inner_iterations + k
                ) * self.num_estimators

                self.dataset.update_partition()

                iterations.append(
                    {
                        **self._set_and_evaluate(hp=hp),
                        "cv_iteration": i,
                        "inner_iteration": k,
                        "task": self.task_string,
                    }
                )

            #  Make sure we consumed the generator.
            assert self.dataset._partition_counter == (i + 1) * inner_iterations

        self._logger.info(
            "Model evaluated over %d iterations.", cv_iterations * inner_iterations
        )

        validation_df = self._get_evaluation_df(iterations)
        validation_df.to_csv(join(self.reports_dir, "validation.csv"))

        #  Extract all numeric keys
        metrics = [
            metric
            for metric, val in iterations[0].items()
            if isinstance(val, numbers.Number)
        ]

        metric_json = {}
        for metric in metrics:
            mean, ci = bootstrap_mean_and_ci(
                [iteration[metric] for iteration in iterations], ci=0.95, n_boot=10000
            )
            metric_json[metric] = {
                "mean": float(mean),
                "ci": float(ci),
            }

            if log_stdout:
                self._logger.info("{:30s}: {:.4e} +/- {:.4e}".format(metric, mean, ci))

        if log_json:
            write_file(
                metric_json,
                join(
                    self.reports_dir,
                    self._config["validate"]["validation_file_name"],
                ),
            )

        return validation_df

    def _get_evaluation_df(self, evaluations):
        """Build experiment evaluation dataframe.

        Args:
            evaluations (list): List of model evaluate metrics.

        Returns:
            A DataFrame with experiment evaluation metrics.
        """

        df = DataFrame(evaluations, columns=evaluations[0].keys())

        #  Add experiment metadata
        df["estimator"] = [self._estimator_config["estimator_name"]] * len(evaluations)
        df["resolution"] = [self.label_shape_string] * len(evaluations)

        return df

    def _label_to_dict(self, label):
        """Generate dict from label data.

        Args:
            label (np.ndarray): Label data.

        Returns:
            A dictionary of self.labels data.
        """

        data = label.reshape(self.label_shape)

        return {l: data[..., i] for i, l in enumerate(self.labels)}

    def _sample_to_dict(self, sample):
        """Generate sample dict view from dataset view.

        Args:
            sample (tuple): Tuple of feautres and label.

        Returns:
            A dictionary of sample data.
        """

        features_dict = {f: v.numpy() for f, v in sample[0].items()}
        labels_dict = self._label_to_dict(sample[1].numpy())

        return {**features_dict, **labels_dict}

    def _build_df(
        self,
        input_fn,
        to_vmec_shape=True,
        include_originals=False,
        include_predictions=False,
    ):
        """Build dataframe from mode data set.

        Args:
            input_fn (function): Function to return tf.data.Dataset.
            to_vmec_shape (bool): Where to slice m=0, n<0 fourier modes.
            include_originals (bool): Include oroginal values in dataframe.
            include_predictions (bool): Include predicted values in dataframe.

        Returns:
            A pandas dataframe with data set samples.
        """

        def _rescale_sample(sample, rescale_fn):
            """Rescale sample data.

            Args:
                sample (dict): Dictionary of sample data.
                rescale_fn (dict): Dictionary of rescale functions.

            Returns:
                A dictionary of rescaled sample data.
            """
            return {
                k: rescale_fn[k](v) if k in rescale_fn else v for k, v in sample.items()
            }

        dataset = input_fn().unbatch()
        dataset = map(self._sample_to_dict, dataset)

        rescale_fn = {
            v: self.get_rescale_fn(v, to_vmec_shape)
            for v in self.features + self.labels
        }
        dataset = map(lambda x: _rescale_sample(x, rescale_fn), dataset)

        dataset_to_dataframe = [dataset]

        #  TODO(@amerlo): Evaluate where to place this call.
        if include_originals:
            originals = self.raw_ds(tf.estimator.ModeKeys.PREDICT)
            originals = map(
                lambda x: {
                    k: v.numpy()[get_label_radial_indices(self.radial_points), :]
                    for k, v in x.items()
                    if k in self.labels
                },
                originals,
            )
            originals = map(lambda x: add_suffix_to_keys(x, "_orig"), originals)
            dataset_to_dataframe.append(originals)

        #  TODO(@amerlo): Evaluate where to place this call.
        if include_predictions:
            predictions = self.predict(input_fn)
            predictions = map(
                lambda x: self._label_to_dict(x["predictions"]), predictions
            )
            predictions = map(lambda x: _rescale_sample(x, rescale_fn), predictions)
            predictions = map(lambda x: add_suffix_to_keys(x, "_pred"), predictions)
            dataset_to_dataframe.append(predictions)

        return to_dataframe(*dataset_to_dataframe)

    def get_rescale_fn(self, variable, to_vmec_shape):
        """Get rescale function for variable from data set.

        Features have not to be rescaled since the scale operations is
        performed within the DenseFeatures layer which each model has.
        While the labels has to be rescaled here in order to be compared
        with data set values. The rescale function is computed
        given the scaler which has been used, and the function operates
        on ndarray of self.lables[:-1] shape.

        Args:
            variable (str): The data set variable for which retrieve
                the function.
            to_vmec_shape (bool): Where to slice m=0, n<0 fourier modes.

        Returns:
            A function.
        """

        def _reshape_and_rescale_label_fn(label, in_shape, out_shape, rescale_fn):
            """Reshape and apply rescale function.

            Args:
                label (nd.array): Label array data.
                in_shape (tuple): Shape to use before applying `rescale_fn`.
                out_shape (tuple): Shape to apply after `rescale_fn`.
                rescale_fn (func): Rescale function to apply.

            Returns:
                The scaled label array data.
            """

            label = label.reshape(in_shape)
            label = np.array(rescale_fn(label))

            return label.reshape(out_shape)

        def _do_nothing_fn(x):
            return x

        if variable in self.features:
            return _do_nothing_fn

        in_shape = self.label_shape[:-1]
        out_shape = (self.radial_points, -1)
        rescale_fn = self.dataset.get_scaler_fn(
            variable,
            mode="rescale",
            radial_points=self.radial_points,
            fourier_modes=self.fourier_modes,
        )

        if not self._estimator_config["scale_label"]:
            rescale_fn = _do_nothing_fn

        if to_vmec_shape:
            return lambda x: _reshape_and_rescale_label_fn(
                x, in_shape, out_shape, rescale_fn
            )[:, self.fourier_modes[1] :]

        return lambda x: _reshape_and_rescale_label_fn(
            x, in_shape, out_shape, rescale_fn
        )

    def assess(self):
        """Train model with an increasing number of dataset samples."""

        iterations = self._config["assess"]["assess_iterations"]
        mode = self._config["assess"]["assess_mode"]

        #  Prepare assess list
        num_samples = [self._config["num_samples"]] * iterations
        test_size = [self._config["test_size"]] * iterations
        validation_mode = self._config["validate"]["validation_mode"]

        if mode == "shrink":
            num_samples = [
                int(len(self.dataset) / iterations * (i + 1)) for i in range(iterations)
            ]
        elif mode == "holdout":
            #  TODO(@amerlo): Could we do it better here?
            max_test_size = 0.8
            min_test_size = self._config["test_size"]
            test_size = [
                max_test_size - (max_test_size - min_test_size) / iterations * i
                for i in range(iterations)
            ]
            #  Fix test runs with 'same' validation mode
            validation_mode = "same"
        else:
            raise ValueError("%s assess mode is not supported" % mode)

        metrics = []
        for i in range(iterations):

            #  TODO(@amerlo): dataset object has to be reset, could we avoid it?
            self._dataset = self._dataset = get_dataset(
                self._dataset_config["dataset_name"]
            )(
                **self._dataset_config["dataset_kwargs"],
                num_samples=num_samples[i],
                test_size=test_size[i],
                val_size=self._config["val_size"],
                validation_mode=validation_mode,
            )

            num_train_samples = len(self.dataset.partition["train"])
            num_test_samples = len(self.dataset.partition["test"])

            self._logger.info(
                "Validating model with %d training samples and %d test samples...",
                num_train_samples,
                num_test_samples,
            )

            validation_metrics = self.validate(log_json=False)

            validation_metrics["samples"] = [num_train_samples] * len(
                validation_metrics
            )
            metrics.append(validation_metrics)

        self._assess_df = concat(metrics, sort=False)
        self.assess_df.to_csv(join(self.reports_dir, "assess.csv"))

    def explain(self):
        """Compute the shap values for estimator and add them to test_df."""

        import shap

        #  Disable shap logging
        logging.getLogger("shap").setLevel(logging.WARNING)

        def get_numpy_values(num_samples=None):
            """Return numpy array of all test samples data."""

            if num_samples is None:
                num_samples = len(self.test_df)

            return np.array(
                [
                    np.hstack(s)
                    for s in np.stack(
                        self.test_df.sample(num_samples)[self.features].values
                    )
                ]
            )

        #  Take only 100 samples to compute background
        background_samples = 100
        background = get_numpy_values(background_samples).mean(axis=0).reshape(1, -1)

        #  Hack to get data as requested by self.estimator
        features_lengths = [
            self.dataset.get_variable_readers(
                use_coil_currents_ratio=self._estimator_config[
                    "use_coil_currents_ratio"
                ],
                use_nyquist_resolution=self.use_nyquist_resolution,
            )[f][3]
            for f in self.features
        ]
        features_lengths = [len_ if len_ != () else 1 for len_ in features_lengths]
        features_indices = list(range(sum(features_lengths)))
        indices = []
        j = 0
        for len_ in features_lengths:
            indices.append(features_indices[j : j + len_])
            j = j + len_

        def model_fn(x):
            x_ = tf.data.Dataset.from_tensor_slices(
                {self.features[i]: list(x[:, ids]) for i, ids in enumerate(indices)}
            ).batch(32)
            return self.estimator.predict(x_).reshape(x.shape[0], -1)

        #  Use KernelExplainer
        explainer = shap.KernelExplainer(model_fn, background)
        shap_values = explainer.shap_values(get_numpy_values())
        shap_values = np.array(shap_values).reshape(
            (*self.label_shape, *shap_values[0].shape)
        )

        #  Save figures to file
        #  TODO(@amerlo): Add fouerier coefficients in yaml file.
        #                 Now we select s=-1, m=0, n=0, first component
        import matplotlib.pyplot as plt

        shap_to_plot = shap_values[-1, 0, 4, 0, :, :]

        plt.close("all")
        shap.summary_plot(shap_to_plot, get_numpy_values(), show=False)
        plt.gcf().savefig(join(self.figures_dir, "shap_summary.pdf"))

        plt.close("all")
        shap.dependence_plot(4, shap_to_plot, get_numpy_values(), show=False)
        plt.gcf().savefig(join(self.figures_dir, "shap_dependence.pdf"))

    def raw_ds(self, mode):
        """Get raw samples.

        Args:
            mode (tf.estimator.ModeKeys or list): samples to retrieve.
        """

        return self.dataset.input_fn(
            mode=mode,
            features=self.features,
            label=self.label,
            return_raw_runs=True,
            use_coil_currents_ratio=self._estimator_config["use_coil_currents_ratio"],
            use_nyquist_resolution=self.use_nyquist_resolution,
        )

    def make_stages(self):
        """Create dvc stages related to experiment."""

        stages = [
            "train",
            "search",
            "ensemble",
            "validate",
            "figures",
            "timeit",
            "summary",
        ]

        #  Define common dvc run args
        cmd_header = "dvc run "
        deps = self._dataset_config["dataset_kwargs"]["data"] + [
            self._dataset_config["dataset_kwargs"]["scalers_file"]
        ]
        exp_yaml = "configs/experiment.yaml"
        task_yaml = "configs/{}.yaml".format(self.task_string)

        desc = {
            "train": {
                "-d": deps,
                "-p": [task_yaml + ":estimator"],
                "-o": [self.reports_dir + "/train"],
                "-m": [self.reports_dir + "/evaluate.json"],
                "'python": [
                    "-m vmec_dnn_surrogate.train --actions train evaluate "
                    "--exp_config={} --task_config={}'".format(exp_yaml, task_yaml)
                ],
            },
            "search": {
                "-d": deps,
                "-p": [task_yaml + ":estimator,hp", exp_yaml + ":config.search"],
                "--plots": [self.reports_dir + "/search.csv"],
                "-o": [self.reports_dir + "/search", self.reports_dir + "/hp.pdf"],
                "'python": [
                    "-m vmec_dnn_surrogate.train --actions search make_figures "
                    "--exp_config={exp} --task_config={task} --figures_to_plot=hp "
                    "--residual_space=None "
                    "&& mv -f {figures_dir}/hp.pdf {reports_dir}/hp.pdf'".format(
                        exp=exp_yaml,
                        task=task_yaml,
                        figures_dir=self.figures_dir,
                        reports_dir=self.reports_dir,
                    )
                ],
            },
            "ensemble": {
                "-d": deps + [self.reports_dir + "/search.csv"],
                "-p": [exp_yaml + ":config.ensemble"],
                "-o": [
                    self.reports_dir + "/ensemble.csv",
                    self.reports_dir + "/ensemble",
                ],
                "'python": [
                    "-m vmec_dnn_surrogate.train --actions ensemble "
                    "--exp_config={} --task_config={} --resume True "
                    "--residual_space=None'".format(exp_yaml, task_yaml)
                ],
            },
            "validate": {
                "-d": deps,
                "-p": [task_yaml + ":estimator", exp_yaml + ":config.validate"],
                "-m": [self.reports_dir + "/validation.json"],
                "--plots": [self.reports_dir + "/validation.csv"],
                "-o": [
                    self.reports_dir + "/validate",
                    self.reports_dir + "/cv-results.pdf",
                ],
                "'python": [
                    "-m vmec_dnn_surrogate.train --actions validate "
                    "--exp_config={} --task_config={} --resume True "
                    "--residual_space=None "
                    "&& python -m vmec_dnn_surrogate.make_figures --figures cv-results "
                    "--files {}/validation.csv --out_dir={}'".format(
                        exp_yaml, task_yaml, self.reports_dir, self.reports_dir
                    )
                ],
            },
            #  The 0th model of validation is used.
            "timeit": {
                "-d": deps + [self.reports_dir + "/validate"],
                "-o": [self.reports_dir + "/timeit.json"],
                "'CUDA_VISIBLE_DEVICES="
                " taskset --cpu-list 1 python": [
                    "-m vmec_dnn_surrogate.train --actions timeit "
                    "--exp_config={} --task_config={} --num_samples=1024 "
                    "--checkpoint={}/validate/0/{}.h5'".format(
                        exp_yaml, task_yaml, self.reports_dir, self.task_string
                    )
                ],
            },
            "summary": {
                "-p": [task_yaml + ":estimator"],
                "-o": [self.reports_dir + "/hp-" + self.task_string + ".tex"],
                "'python": [
                    "-m vmec_dnn_surrogate.train --actions summary "
                    "--exp_config={} --task_config={} --num_samples=32'".format(
                        exp_yaml, task_yaml
                    )
                ],
            },
        }

        for stage in stages:
            cmd = cmd_header
            cmd += "--name {}-{} ".format(stage, self.task_string)
            cmd += "--no-exec "
            cmd += "--force "

            #  Add cmd line options
            for arg, value in desc[stage].items():
                for v in value:
                    cmd += "{} {} ".format(arg, v)

            self._logger.info("Creating dvc stage...")
            self._logger.info(cmd)
            system(cmd)

    def _select_best_model_to_add(
        self, ensemble, models, eval_spec, validation_iterations=1
    ):
        """Given current ensemble, select single best model to add given
        greedy ensemble selection with replacement.

        Procedure as from:

        Caruana, Rich, et al. "Ensemble selection from libraries of models."
        Proceedings of the twenty-first international conference on Machine learning. 2004.

        Args:
            ensemble (list): List of current models index in the enseble.
            models (list): List of models hp.
            eval_spec (dict): Evaluation spec object on how to compute validation and test
                losses.
            validation_iterations (int): Number of validataion data splits to perform.

        Returns:
            A dictionary of:
                best (int): The index of the best model to add to the ensemble.
                loss (float): The ensemble validation loss given the best model.
                **metrics (float): Ensemble metric lossess on the test set
                    given the best model.
        """

        #  Hack to use trained model from search.
        action_ = self.action
        self._action = "search"

        #  Force ensemble in experiment.
        self._estimator_config["ensemble"] = True

        losses = []

        for model in models:

            proposed_ensemble = ensemble + [model["estimator_index"]]

            #  Build enseble configuration
            ensemble_hp = {
                "hp_{}".format(k): [
                    m[k]
                    for i in proposed_ensemble
                    for m in models
                    if m["estimator_index"] == i
                ]
                for k in models[0].keys()
            }
            del ensemble_hp["hp_estimator_index"]

            #  Set ensemble
            self._estimator_config["ensemble_hp"] = ensemble_hp
            self._current_ensemble_indices = proposed_ensemble

            self._logger.info("Proposed ensemble: %s" % proposed_ensemble)

            val_losses = []
            for j in range(validation_iterations):

                self._logger.info("Evaluating ensemble over %dth inner splits..." % j)

                self.dataset.update_partition()
                eval_df = self._build_df(
                    self.eval_input_fn,
                    to_vmec_shape=self.fourier_modes is not None,
                    include_originals=self.fourier_modes is not None,
                    include_predictions=True,
                )

                #  Use first eval_spec entry as validation loss.
                val_losses.append(
                    list(self._evaluate(data=eval_df, eval_spec=eval_spec).values())[0]
                )

            self._logger.info("Evaluating ensemble over test set...")

            #  Clear test data set cache.
            self._test_df = None

            #  Use average of validation losses, include test loss.
            losses.append(
                {
                    **self._evaluate(data=self.test_df, eval_spec=eval_spec),
                    "val_loss": np.average(val_losses),
                    "test_df": self.test_df,
                }
            )

        #  Restore hack from above.
        self._action = action_

        #  Find best model to add based on validation loss.
        best_index = np.argmin([loss["val_loss"] for loss in losses])

        return {
            "best": models[best_index]["estimator_index"],
            **losses[best_index],
        }

    def ensemble(self, log_stdout=True):
        """Selects best model with replacement for ensemble.

        It uses the NES algorithm, as from:

        Zaidi, Sheheryar, et al. "Neural ensemble search for performant and calibrated
        predictions." arXiv preprint arXiv:2006.08573 (2020).

        It outputs k proposed enseble with their test loss on the k-folds test set
        defined in the VMECDataset.
        """

        #  Get dataframe of hp for current search.
        trials_hp = get_hp_from_trials_df(
            data=self.trials_df.copy(),
            hp=list(get_hp_space(self.experiment_config).keys())
            + ["estimator_index", "cv_iteration"],
        )

        #  Ensemble selection parameters.
        inner_iterations = 1
        cv_iterations = self.experiment_config["config"]["validate"][
            "validation_iterations"
        ]
        search_iterations = self.experiment_config["config"]["search"][
            "search_iterations"
        ]
        ensemble_size = self.experiment_config["config"]["ensemble"]["ensemble_size"]
        eval_spec = self.experiment_config["config"]["ensemble"]["ensemble_eval_spec"]

        #  This is an hack which makes some assumptions on the ensemble
        #  selection procedure.
        #  Reset data set partitions generator, and set total number of
        #  inner iterations is, as:
        #    train_validation_splits * ensemble_size * num_of_trials_per_cv
        self.dataset.set_partition_fn(
            inner_iterations=inner_iterations * ensemble_size * search_iterations
        )

        iterations = []

        for i in range(cv_iterations):

            self._logger.info("Building ensemble for %d cv iteration...", i)

            #  Start with an empty ensemble.
            ensemble = []

            for j in range(ensemble_size):

                self._logger.info(
                    "Current ensemble %d has %d base learners..." % (i, len(ensemble))
                )

                #  Select best model among ones from the respective cv iteration.
                models = trials_hp[trials_hp["cv_iteration"] == i].to_dict("records")
                best = self._select_best_model_to_add(
                    ensemble=ensemble,
                    models=models,
                    eval_spec=eval_spec,
                    validation_iterations=inner_iterations,
                )

                #  Build ensemble_hp to store.
                ensemble.append(best.pop("best"))
                ensemble_hp = [
                    trials_hp[trials_hp["estimator_index"] == e].to_dict("records")[0]
                    for e in ensemble
                ]

                ensemble_index = i * ensemble_size + j
                ensemble_dir = join(self.reports_dir, self.action, str(ensemble_index))

                #  Save test_df to disk for later usage.
                test_df = best.pop("test_df")
                rmtree(ensemble_dir, ignore_errors=True)
                makedirs(ensemble_dir)
                test_df.to_pickle(
                    join(
                        ensemble_dir,
                        "test.pkl",
                    )
                )

                iterations.append(
                    {
                        "ensemble": ensemble_hp,
                        "ensemble_size": len(ensemble),
                        "cv_iteration": i,
                        "ensemble_index": ensemble_index,
                        **best,
                    }
                )

                if log_stdout:
                    for metric, value in best.items():
                        self._logger.info("{:30s}: {:.4e}".format(metric, value))

                #  This is an hack in order to consume the dataset generator in
                #  case of failed models during search.
                for h in range(search_iterations - len(models)):
                    for _ in range(inner_iterations):
                        self.dataset.update_partition()

            #  Make sure we consumed the generator.
            assert self.dataset._partition_counter == (i + 1) * (
                inner_iterations * search_iterations * ensemble_size
            )

        #  Save ensembles to csv file.
        DataFrame(iterations).to_csv(join(self.reports_dir, "ensemble.csv"))

    def _evaluate_df(self, data, eval_spec, folder, key, file_name=None):
        """Evaluate metrics and append to given data.

        Args:
            data (pd.DataFrame): The pandas dataframe from where to read test pickle files.
            eval_spec (dict): Dictionary on which metrics to use.
            folder (str): Folder where to read the test pickle files.
            key (str): The data columns to use to index the test pickle files.
            file_name (str): If not None, save DataFrame to file.

        Returns:
            The pandas DataFrame with the new metrics evaluated.
        """

        metrics = []
        for i, row in data.iterrows():

            path = join(folder, str(row[key]), "test.pkl")
            self._logger.info("Reading back {} test data...".format(path))

            test_df = read_pickle(path)
            metrics.append(self._evaluate(test_df, eval_spec))

        for k in metrics[0].keys():
            data[k] = [metric[k] for metric in metrics]

        if file_name:
            data.to_csv(file_name)

        return data

    def timeit(self, log_stdout=True, log_json=True):
        """Retrieve trained model and time its prediction time.

        TODO(@amerlo):
        * We should compute inference on the target device, so on the GPU
        in case we perform the inference it with.
        """

        checkpoint_file_path = self.experiment_config["config"]["checkpoint"]

        def _timeit(func, args, iterations=10, **kwargs):
            t0 = time.time()
            for _ in range(iterations):
                func(args, **kwargs)
            return (time.time() - t0) / iterations

        data = self.test_input_fn()

        self._logger.info("Warm-up model...")
        self.estimator.evaluate(data, verbose=_KERAS_VERBOSE)
        self.estimator.load_weights(checkpoint_file_path)

        iterations = 100
        metrics = {"predict": [], "predict_on_batch": [], "model": []}
        metrics_json = {}

        samples = [
            {f: tf.expand_dims(tf.constant(d[0][f]), axis=0) for f in self.features}
            for d in data.unbatch().shuffle(iterations * 10).take(iterations)
        ]

        for sample in samples:

            #  Use model predict function
            metrics["predict"].append(
                _timeit(
                    self.estimator.predict, sample, batch_size=1, verbose=_KERAS_VERBOSE
                )
            )

            #  Use model predict on batch
            metrics["predict_on_batch"].append(
                _timeit(self.estimator.predict_on_batch, sample)
            )

            #  Call model directly
            metrics["model"].append(_timeit(self.estimator, sample))

        #  Compute mean and ci with bootstrap
        for metric, times in metrics.items():

            mean, ci = bootstrap_mean_and_ci(times, ci=0.95, n_boot=10000)
            metrics_json[metric] = {
                "mean": float(mean),
                "ci": float(ci),
                "std": float(np.std(times)),
            }

            if log_stdout:
                self._logger.info("{:30s}: {:.4e} +/- {:.4e}".format(metric, mean, ci))

        if log_json:
            write_file(
                metrics_json,
                join(
                    self.reports_dir,
                    "timeit.json",
                ),
            )

    @property
    def actions(self):
        """Get the list of supported actions."""

        return [
            "make_scalers",
            "search",
            "train",
            "evaluate",
            "summary",
            "make_figures",
            "validate",
            "timeit",
            "assess",
            "explain",
            "make_stages",
            "ensemble",
        ]

    @property
    def action(self):
        """Current experiment action."""
        return self._action

    @property
    def train_input_fn(self):
        """Train dataset."""

        return self.get_input_fn(
            mode=tf.estimator.ModeKeys.TRAIN,
            shuffle=True,
            take_samples=self._config["num_train_samples"],
        )

    @property
    def eval_input_fn(self):
        """Evaluation dataset."""

        return self.get_input_fn(mode=tf.estimator.ModeKeys.EVAL)

    @property
    def test_input_fn(self):
        """Test dataset."""

        return self.get_input_fn(mode=tf.estimator.ModeKeys.PREDICT)

    @property
    def test_df(self):
        """Return dataframe with true and predicted test samples.

        If `test_df` has been specified via experiment config, then
        load dataset from file, otherwise build it from experiment object.
        """

        if self._test_df is None:
            if self._config["test_df"] is not None and self._config["resume"]:
                #  Support * in file paths
                if "*" in self._config["test_df"]:
                    file_paths = glob.glob(self._config["test_df"])
                else:
                    file_paths = self._config["test_df"]
                dfs = []
                for path in file_paths:
                    _df = read_pickle(path)
                    _df["index"] = path
                    dfs.append(_df)
                self._test_df = concat(dfs, sort=False, ignore_index=True)

                #  Compute additional metrics and save file
                if (
                    self.experiment_config["config"]["evaluate"]["residual_space"]
                    is not None
                ):
                    self._evaluate(
                        self._test_df,
                        eval_spec={},
                    )

                #  Add iota profile on full grid
                if "iota" in self._test_df:
                    self._test_df["iotaf"] = self._test_df["iota"].apply(
                        get_label_on_full_grid
                    )
                    self._test_df["iotaf_pred"] = self._test_df["iota_pred"].apply(
                        get_label_on_full_grid
                    )

                #  Add shear profile, use only predicted radial indices
                if ("iotaf" in self._test_df) and ("phiedge" in self._test_df):
                    self._test_df["shear"] = self._test_df[
                        ["iotaf", "phiedge", "iota"]
                    ].apply(
                        lambda x: compute_shear(x[0], x[1])[
                            get_label_radial_indices(len(x[2]))
                        ],
                        axis=1,
                    )
                    self._test_df["shear_pred"] = self._test_df[
                        ["iotaf_pred", "phiedge", "iota_pred"]
                    ].apply(
                        lambda x: compute_shear(x[0], x[1])[
                            get_label_radial_indices(len(x[2]))
                        ],
                        axis=1,
                    )

            else:
                self._test_df = self._build_df(
                    self.test_input_fn,
                    to_vmec_shape=self.fourier_modes is not None,
                    include_originals=self.fourier_modes is not None,
                    include_predictions=True,
                )

        return self._test_df

    @property
    def assess_df(self):
        """Dataframe for the assess dataset action."""

        if self._assess_df is None:
            assess_file = join(self.reports_dir, "assess.csv")
            if self._config["resume"] and isfile(assess_file):
                self._assess_df = read_csv(assess_file)

        return self._assess_df

    @property
    def trials_df(self):
        """Dataframe for the search action."""

        if self._trials_df is None:
            trials_file = join(self.reports_dir, "search.csv")
            if self._config["resume"] and isfile(trials_file):
                self._trials_df = read_csv(trials_file)

                #  Compute additional metrics and save file
                if (
                    self.experiment_config["config"]["evaluate"]["residual_space"]
                    is not None
                ):
                    self._trials_df = self._evaluate_df(
                        self._trials_df,
                        folder=join(self.reports_dir, "search"),
                        key="estimator_index",
                        eval_spec=self.experiment_config["config"]["evaluate"][
                            "eval_spec"
                        ],
                        file_name=join(self.reports_dir, "search-full.csv"),
                    )

        return self._trials_df

    @property
    def ensemble_df(self):
        """Dataframe for the ensemble action."""

        if self._ensemble_df is None:
            ensemble_file = join(self.reports_dir, "ensemble.csv")
            if self._config["resume"] and isfile(ensemble_file):
                self._ensemble_df = read_csv(ensemble_file)

                #  Compute additional metrics and save file
                if (
                    self.experiment_config["config"]["evaluate"]["residual_space"]
                    is not None
                ):
                    self._ensemble_df = self._evaluate_df(
                        self._ensemble_df,
                        folder=join(self.reports_dir, "ensemble"),
                        key="ensemble_index",
                        eval_spec=self.experiment_config["config"]["evaluate"][
                            "eval_spec"
                        ],
                        file_name=join(self.reports_dir, "ensemble-full.csv"),
                    )

        return self._ensemble_df

    @property
    def experiment_config(self):
        """Build experiment config."""

        return {
            "config": self._config,
            "task": self._task,
            "dataset": self._dataset_config,
            "estimator": self._estimator_config,
            "hp": self._hp,
            "reports": self._reports,
            "figures": self._figures,
        }

    @property
    def task_string(self):
        """Experiment task string."""

        base_task_string = "{}-{}-r{}".format(
            self._task["beta_scenario"],
            self._task["label"].lower(),
            self._task["radial_points"],
        )

        if self._task["fourier_modes"] is None:
            return base_task_string

        if isinstance(self._task["fourier_modes"], int):
            return base_task_string + "f{}".format(
                self._task["fourier_modes"],
            )

        return base_task_string + "p{}t{}".format(
            self._task["fourier_modes"][0],
            self._task["fourier_modes"][1],
        )

    @property
    def reports_dir(self):
        """Experiment reports directory."""
        return join(self._config["reports_dir"], self.task_string)

    @property
    def figures_dir(self):
        """Experiment figures directory."""
        return join(self._config["figures_dir"], self.task_string)

    @property
    def model_dir(self):
        """Experiment estimator/model directory."""
        return join(self.reports_dir, self.action, str(self._current_estimator_index))

    @property
    def estimator(self):
        """Experiment estimator."""
        return self._estimator

    @property
    def dataset(self):
        """Experiment dataset."""
        return self._dataset

    @property
    def features(self):
        """Experiment features."""
        return self._task["features"]

    @property
    def label(self):
        """Experiment label."""
        return self._task["label"]

    @property
    def labels(self):
        """Experiment labels."""

        try:
            return FIELD_COMPONENTS[self.label]
        except KeyError:
            return [self.label]

    @property
    def labels_orig(self):
        """Experiment original labels."""
        return ["{}_orig".format(label) for label in self.labels]

    @property
    def labels_pred(self):
        """Experiment predicted labels."""
        return ["{}_pred".format(label) for label in self.labels]

    @property
    def conv_features(self):
        """Experiment convolutional features."""

        if "conv_features" not in self._estimator_config:
            return []

        return [
            f for f in self.features if f in self._estimator_config["conv_features"]
        ]

    @property
    def dense_features(self):
        """Experiment dense features."""
        return [f for f in self.features if f not in self.conv_features]

    @property
    def metadata(self):
        """Experiment metadata to keep track of."""
        return self.experiment_config["config"]["metadata"]

    @property
    def radial_points(self):
        """Number of radial points used."""
        return self._task["radial_points"]

    @property
    def fourier_modes(self):
        """Number of fourier modes used."""

        if isinstance(self._task["fourier_modes"], int):
            return [self._task["fourier_modes"], self._task["fourier_modes"]]

        return self._task["fourier_modes"]

    @property
    def label_shape(self):
        """Shape for the output experiment label."""

        decompose_label = self.experiment_config["estimator"].get(
            "decompose_label", False
        )

        if decompose_label:
            return (
                self.experiment_config["estimator"]["num_components"],
                len(self.labels),
            )

        if self.fourier_modes is None:
            return (self.radial_points, len(self.labels))

        return (
            self.radial_points,
            self.fourier_modes[0],
            self.fourier_modes[1] * 2 + 1,
            len(self.labels),
        )

    @property
    def use_nyquist_resolution(self):
        """Use full Fourier resolution for supported labels."""

        if "use_nyquist_resolution" not in self._estimator_config:
            return False

        return self._estimator_config["use_nyquist_resolution"]

    @property
    def orig_label_shape(self):
        """Shape for the original output experiment label."""

        modes = get_num_fourier_modes_from_label(
            self.label, self.use_nyquist_resolution
        )

        return (NUM_OF_RADIAL_POINTS, modes[0], modes[1] * 2 + 1)

    @property
    def label_shape_string(self):
        """String representing the experiment label shape."""
        return "x".join(map(str, self.label_shape[:-1]))

    @property
    def batch_size(self):
        """Experiment batch size."""
        return self._estimator_config["batch_size"]

    @property
    def num_epochs(self):
        """Number of epochs."""

        num_epochs = self._config["num_epochs"]
        if is_imported("hvd"):
            num_epochs = int(num_epochs / hvd.size())

        return num_epochs

    @property
    def current_ensemble_indices(self):
        """Estimator indices for current ensemble."""

        return self._current_ensemble_indices

    @property
    def num_estimators(self):
        """Number of estimators for ensemble learning."""

        if not self.experiment_config["estimator"]["ensemble"]:
            return 1

        return len(list(self.experiment_config["estimator"]["ensemble_hp"].values())[0])

    @property
    def best_test_sample(self):
        """Index of the best regressed test sample in test_df."""

        if not hasattr(self, "_best_test_sample"):
            if "residuals" in self.test_df:
                self._best_test_sample = self.test_df["residuals"].idxmin()
            else:
                self._best_test_sample = None

        return self._best_test_sample

    @property
    def median_test_sample(self):
        """Index of the median regressed test sample in test_df."""

        if not hasattr(self, "_median_test_sample"):
            if "residuals" in self.test_df:
                self._median_test_sample = (
                    (self.test_df["residuals"] - self.test_df["residuals"].median())
                    .abs()
                    .argsort()[0]
                )
            else:
                self._median_test_sample = None

        return self._median_test_sample

    @property
    def worst_test_sample(self):
        """Index of the worst regressed test sample in test_df."""

        if not hasattr(self, "_worst_test_sample"):
            if "residuals" in self.test_df:
                self._worst_test_sample = self.test_df["residuals"].idxmax()
            else:
                self._worst_test_sample = None

        return self._worst_test_sample

    @classmethod
    def from_config(cls, config):
        """Load an experiment from config.

        Args:
            config (str/dict): Configuration dictionary or string.

        Returns:
            An `Experiment` object.
        """

        if not isinstance(config, dict):
            raise ValueError("The config parameter must be a dict or a string")

        return cls(**config)


if __name__ == "__main__":
    import doctest

    doctest.testmod()
