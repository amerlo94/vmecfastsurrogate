"""Set up an `Experiment` and train the model."""

import logging
import argparse
import random

import numpy as np
import tensorflow as tf

from vmec_dnn_surrogate.experiments import Experiment
from vmec_dnn_surrogate.lib.utils import update_dict, read_file, parse_bool, parse_none


def build_experiment_config(args):
    """Build input dict for vmec_dnn_surrogate.Experiment.

    It updates the yaml files configuration with the command line
    argument values.

    Args:
        args: Command line arguments.

    Returns:
        A dictionary describing the vmec_dnn_surrogate.Experiment configuration.
    """

    config = {**read_file(args.exp_config), **read_file(args.task_config)}

    return update_dict(config, vars(args))


def main():  # pylint: disable=too-many-statements
    """Instantiate and run experiment."""

    parser = argparse.ArgumentParser(description="VMEC training experiment")
    parser.add_argument(
        "--exp_config",
        default="configs/experiment.yaml",
        help="File path for experiment configuration",
        type=str,
    )
    parser.add_argument(
        "--task_config",
        default="configs/null-space-r5f4.yaml",
        help="File path for task configuration",
        type=str,
    )
    #  Config
    parser.add_argument(
        "--reports_dir",
        default=None,
        help="Folder path where to store reports",
        type=str,
    )
    parser.add_argument(
        "--figures_dir",
        default=None,
        help="Folder path where to store figures",
        type=str,
    )
    parser.add_argument(
        "--figures_to_plot",
        default=None,
        help="Figures to be plotted",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "--resume", default=None, help="Resume experiment from dir", type=parse_bool
    )
    parser.add_argument(
        "--test_df",
        default=None,
        help="Path for the test dataframe files",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "--checkpoint",
        default=None,
        help="Path for the h5 model to load",
        type=str,
    )
    parser.add_argument(
        "--actions",
        help="Experiment actions to perform",
        type=str,
        default=None,
        nargs="+",
    )
    parser.add_argument("--num_epochs", default=None, help="Number of epochs", type=int)
    parser.add_argument(
        "--num_samples", default=None, help="Maximum number of samples", type=int
    )
    parser.add_argument("--test_size", default=None, help="Test size ratio", type=float)
    parser.add_argument("--val_size", default=None, help="Val size ratio", type=float)
    parser.add_argument(
        "--train_file_name",
        default=None,
        help="Name for the training metric file",
        type=str,
    )
    #  Evaluate
    parser.add_argument(
        "--residual_space",
        default=None,
        help="Space where to compute the residuals",
        type=parse_none,
    )
    parser.add_argument(
        "--eval_file_name",
        default=None,
        help="Name for the evaluate metric file",
        type=str,
    )
    parser.add_argument(
        "--validation_file_name",
        default=None,
        help="Name for the validation metric file",
        type=str,
    )
    #  Search
    parser.add_argument(
        "--search_iterations",
        default=None,
        help="Number of search iterations",
        type=int,
    )
    #  Validate
    parser.add_argument(
        "--validation_mode", default=None, help="Experiment validation mode", type=str
    )
    parser.add_argument(
        "--validation_iterations",
        default=None,
        help="Number of validation iterations",
        type=int,
    )
    parser.add_argument(
        "--inner_iterations",
        default=None,
        help="Number of inner cross-validation iterations",
        type=int,
    )
    #  Assess
    parser.add_argument(
        "--assess_mode", default=None, help="Experiment assess mode", type=str
    )
    parser.add_argument(
        "--assess_iterations",
        default=None,
        help="Number of assess iterations",
        type=int,
    )
    #  Dataset
    parser.add_argument(
        "--data",
        default=None,
        help="Folders where to read data",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "--test_data",
        default=None,
        help="Folders where to read test data",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "--scalers_file", default=None, help="JSON scalers file", type=str
    )
    #  Task
    parser.add_argument("--task_name", default=None, help="Task name", type=str)
    parser.add_argument(
        "--features", default=None, help="Features to train on", type=str, nargs="+"
    )
    parser.add_argument("--label", default=None, help="Label to train on", type=str)
    parser.add_argument(
        "--radial_points",
        default=None,
        help="Number of radial points to use",
        type=int,
    )
    parser.add_argument(
        "--fourier_modes",
        default=None,
        help="Number of fourier modes to use",
        type=int,
    )
    #  Estimator
    parser.add_argument(
        "--estimator_name", default=None, help="Name of the model to run", type=str
    )
    parser.add_argument(
        "--use_coil_currents_ratio",
        default=None,
        help="Use coil currents ratio",
        type=parse_bool,
    )
    parser.add_argument(
        "--early_stopping_patience",
        default=None,
        help="Number of epochs to wait for early stopping",
        type=int,
    )
    parser.add_argument(
        "--optimizer", default=None, help="Name of the optimizer to use", type=str
    )
    parser.add_argument(
        "--learning_rate", default=None, help="Optimizer learning rate", type=float
    )
    parser.add_argument(
        "--min_lr", default=None, help="Optimizer minimum learning rate", type=float
    )
    parser.add_argument(
        "--decay_rate", default=None, help="Optimizer decay rate", type=float
    )
    parser.add_argument(
        "--decay_steps", default=None, help="Optimizer decay steps", type=int
    )
    parser.add_argument(
        "--batch_size", default=None, help="Batch size to use", type=int
    )
    parser.add_argument("--dropout", default=None, help="Dropout rate", type=float)
    #  DeepDense args
    parser.add_argument(
        "--hidden_units",
        default=None,
        help="Number of hidden units for dense estimator",
        type=int,
        nargs="+",
    )
    parser.add_argument(
        "--activation_fn", default=None, help="Layers activation function", type=str
    )
    parser.add_argument(
        "--output_activation_fn",
        default=None,
        help="Output layer activation function",
        type=str,
    )
    #  Conv3D args
    parser.add_argument(
        "--reducer_hidden_units",
        default=None,
        help="Number of hidden units for Conv3D reducer tree",
        type=int,
        nargs="+",
    )
    parser.add_argument(
        "--reducer_activation_fn",
        default=None,
        help="Reducer layers activation function",
        type=str,
    )
    parser.add_argument(
        "--upsampler_activation_fn",
        default=None,
        help="Upsampler layers activation function",
        type=str,
    )
    parser.add_argument(
        "--upsampler_layers", default=None, help="Number of upsampler layers", type=int
    )
    parser.add_argument(
        "--link_shape",
        default=None,
        help="Link shape from reducer to upsampler layers",
        type=tuple,
    )
    parser.add_argument(
        "--upsampler_filters", default=None, help="Number of layer filters", type=int
    )
    parser.add_argument(
        "--upsampler_kernel",
        default=None,
        help="Upsampler layers kernel size",
        type=int,
    )
    parser.add_argument(
        "--upsampler_strides",
        default=None,
        help="Upsampler layers stride size",
        type=int,
    )
    parser.add_argument(
        "--fix_seed",
        default=False,
        help="Fix random seed for numpy and tensorflow",
        type=bool,
    )

    args = parser.parse_args()
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logging.info("VMEC training experiment ...")

    if args.fix_seed:
        random.seed(42)
        np.random.seed(42)
        tf.random.set_seed(42)

    experiment_config = build_experiment_config(args)
    experiment = Experiment.from_config(experiment_config)
    experiment.run()


if __name__ == "__main__":
    main()
