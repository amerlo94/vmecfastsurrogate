"""
Script to make figures of vmec data set.

Examples:
    $ python vmec_dnn_surrogate/make_figures.py \
    $ --data=minerva-vmec-4ee8c3281ec4f28cd47efe1f612335b1 --figures=phiedge
"""

import logging
import argparse
from os import makedirs
from os.path import isdir, join, basename
import re

import numpy as np
from pandas import read_csv, concat

from vmec_dnn_surrogate.lib.config import (
    FIGURES_PATH,
    FIELD_COMPONENTS,
)
from vmec_dnn_surrogate.lib.figures import get_figure, _CMAP
from vmec_dnn_surrogate.lib.dataset import (
    VmecDataset,
    to_downsampled_shape,
    get_label_radial_indices,
)
from vmec_dnn_surrogate.lib.utils import (
    to_dataframe,
    get_all_labels,
)

FIGURES = {
    "phiedge": {"figure": "Hist", "features": ["phiedge"], "plot_kwargs": {}},
    "curtor": {
        "figure": "Hist",
        "features": ["curtor"],
        "plot_kwargs": {"xlabel": [r"$I_{toroidal}$ [A]"]},
    },
    "pressureOnAxis": {
        "figure": "Hist",
        "features": ["pressureScale"],
        "columns": ["pressureScale"],
        "plot_kwargs": {"xlabel": [r"$p_{axis}$ [Pa]"]},
    },
    "beta": {
        "figure": "Hist",
        "features": ["averageBeta"],
        "plot_kwargs": {"xlabel": [r"$\langle \beta \rangle$"]},
    },
    "beta0": {
        "figure": "Hist",
        "features": ["axisBeta"],
        "plot_kwargs": {"xlabel": [r"$\beta_0$"]},
    },
    "iota0": {
        "figure": "Hist",
        "columns": ["iota"],
        "features": ["iota_axis"],
        "plot_kwargs": {"xlabel": [r"$\iota_0$"]},
    },
    "iotab": {
        "figure": "Hist",
        "columns": ["iota"],
        "features": ["iota_edge"],
        "plot_kwargs": {"xlabel": [r"$\iota_{b}$"]},
    },
    "coilCurrents": {
        "figure": "Hist",
        "features": ["coilCurrents"],
        "figure_kwargs": {"figsize": [16, 16]},
        "plot_kwargs": {},
    },
    "features-correlation": {
        "figure": "Correlation",
        "features": ["phiedge", "I1", "I2", "I3", "I4", "I5", "IA", "IB"],
        "columns": ["phiedge", "coilCurrents"],
        "plot_kwargs": {
            "cmap": _CMAP,
            "center": 0,
            "linewidths": 0.2,
            "cbar_kws": {"shrink": 0.5},
        },
    },
    "finite-features-pairplot": {
        "figure": "Pairplot",
        "features": ["phiedge", "curtor", "averageBeta"],
        "columns": ["phiedge", "curtor", "averageBeta"],
        "plot_kwargs": {
            "diag_kind": "kde",
            "corner": True,
            "plot_kws": {"alpha": 0.6},
        },
    },
    "null-features-pairplot": {
        "figure": "Pairplot",
        "features": ["phiedge", "i2", "i3", "i4", "i5", "iA", "iB"],
        "columns": ["phiedge", "coilCurrents"],
        "plot_kwargs": {
            "corner": True,
            "hue": "configuration",
            "to_plot": {
                "configuration": ["reference"] * 8,
                "phiedge": [-2.4, -1.825, -2.2, -1.72, -2.0, -2.0, -1.77, -1.92],
                "i2": [1.0, 1.0, 1.0, 0.99, 1.0, 1.04, 0.97, 0.99],
                "i3": [1.0, 1.0, 1.0, 1.01, 0.97, 1.04, 0.92, 0.93],
                "i4": [1.0, 1.0, 1.0, 1.11, 0.92, 1.13, 0.88, 0.75],
                "i5": [1.0, 1.0, 1.0, 1.12, 0.92, 1.13, 0.85, 0.74],
                "iA": [0.0, -0.23, 0.25, 0.1, -0.13, 0.0, 0.0, -0.17],
                "iB": [0.0, -0.23, 0.25, -0.21, 0.13, 0.0, 0.0, 0.17],
            },
            "palette": {
                "data": (0.40, 0.66, 0.81, 0.2),
                "reference": (0.94, 0.54, 0.38, 1.0),
            },
        },
    },
    "BCosOnAxis": {
        "figure": "Hist",
        "features": ["BCos_axis"],
        "columns": ["BCos"],
        "plot_kwargs": {"xlabel": [r"$\langle B_0 \rangle_{axis}$ [T]"]},
    },
    "B0": {
        "figure": "Hist",
        "features": ["B0"],
        "columns": ["B0"],
        "plot_kwargs": {"xlabel": [r"$\langle B_0 \rangle$ [T]"]},
    },
    "phiedge-against-B0": {
        "figure": "Scatter",
        "features": ["phiedge", "B0"],
        "columns": ["phiedge", "B0"],
    },
    "i2-against-i3-with-B0": {
        "figure": "Scatter",
        "features": ["i2", "i3", "B0"],
        "columns": ["B0", "coilCurrents"],
        "plot_kwargs": {"cbar_label": [r"$\langle B_0 \rangle$ [T]"]},
    },
    "i4-against-i5-with-B0": {
        "figure": "Scatter",
        "features": ["i4", "i5", "B0"],
        "columns": ["B0", "coilCurrents"],
        "plot_kwargs": {"cbar_label": [r"$\langle B_0 \rangle$ [T]"]},
    },
    "iA-against-iB-with-B0": {
        "figure": "Scatter",
        "features": ["iA", "iB", "B0"],
        "columns": ["B0", "coilCurrents"],
        "plot_kwargs": {"cbar_label": [r"$\langle B_0 \rangle$ [T]"]},
    },
    "RCosOnAxis": {
        "figure": "Hist",
        "features": ["RCos_axis"],
        "columns": ["RCos"],
        "plot_kwargs": {"xlabel": [r"$R_{axis}$ [m]"]},
    },
    "PlasmaVolume": {
        "figure": "Hist",
        "features": ["plasmaVolume"],
        "plot_kwargs": {"xlabel": [r"$V_{plasma}$ [$m^3$]"]},
    },
    "MinorRadius": {
        "figure": "Hist",
        "features": ["minorRadius"],
        "plot_kwargs": {"xlabel": [r"$a$ [m]"]},
    },
    "phiedge-against-plasmaVolume": {
        "figure": "Scatter",
        "features": ["phiedge", "plasmaVolume"],
        "plot_kwargs": {},
    },
    "phiedge-against-plasmaVolume-with-beta": {
        "figure": "Scatter",
        "features": ["phiedge", "plasmaVolume", "averageBeta"],
        "plot_kwargs": {},
    },
    "pressureOnAxis-against-curtor-with-beta": {
        "figure": "Scatter",
        "features": ["pressureScale", "curtor", "averageBeta"],
        "columns": ["pressureScale", "curtor", "averageBeta"],
        "plot_kwargs": {"xlabel": r"$p(0)$ [pA]", "ylabel": r"$I_{toroidal}$ [A]"},
    },
    "iA-against-iB-with-iotaAtEdge": {
        "figure": "Scatter",
        "features": ["iA", "iB", "iota_edge"],
        "columns": ["coilCurrents", "iota"],
        "plot_kwargs": {},
    },
    "iA-against-iB-with-iotaOnAxis": {
        "figure": "Scatter",
        "features": ["iA", "iB", "iota_axis"],
        "columns": ["coilCurrents", "iota"],
        "plot_kwargs": {},
    },
    "null-RCos": {
        "figure": "Scatter",
        "features": ["i2", "i3", "i4", "i5", "iA", "iB", "phiedge", "RCos"],
        "columns": ["coilCurrents", "phiedge", "RCos"],
        "figure_kwargs": {"figsize": [20, 20]},
        "plot_kwargs": {
            "apply": {
                "x": None,
                "y": None,
                "hue": "lambda x: x[-1, [1, 2, 3, 4]]",
            },
            "cmap": "coolwarm",
            "kind": "tsne",
            "xlabel": "t-SNE 1",
            "ylabel": "t-SNE 2",
            "cbar_label": [
                r"$R_{0,1}$ (m)",
                r"$R_{0,2}$ (m)",
                r"$R_{0,3}$ (m)",
                r"$R_{0,4}$ (m)",
            ],
            "cbar_limits": [(0.23, 0.31), (-0.16, 0), (-4e-3, 4e-3), (-4e-3, 0)],
            "annotate": {
                "apply": "lambda x: x.std()",
                "fmt": r"$\sigma = {:.1e}$ m",
            },
            "tsne_kws": {
                "n_iter": 5000,
                "perplexity": 50,
                "scale": True,
            },
        },
    },
    "null-LambdaSin": {
        "figure": "Scatter",
        "features": ["i2", "i3", "i4", "i5", "iA", "iB", "phiedge", "LambdaSin"],
        "columns": ["coilCurrents", "phiedge", "LambdaSin"],
        "figure_kwargs": {"figsize": [8, 8]},
        "plot_kwargs": {
            "apply": {
                "x": None,
                "y": None,
                "hue": "lambda x: x[-1, [1, 2, 3, 4]]",
            },
            "cmap": "coolwarm",
            "kind": "tsne",
            "xlabel": "t-SNE 1",
            "ylabel": "t-SNE 2",
            "cbar_label": [
                r"$\lambda_{0,1}$ (rad)",
                r"$\lambda_{0,2}$ (rad)",
                r"$\lambda_{0,3}$ (rad)",
                r"$\lambda_{0,4}$ (rad)",
            ],
            "cbar_limits": [
                (5e-2, 1.2e-1),
                (-4e-3, 1.2e-2),
                (-7e-3, 0.0),
                (-2e-3, 2e-3),
            ],
            "annotate": {
                "apply": "lambda x: x.std()",
                "fmt": r"$\sigma = {:.1e}$ m",
            },
            "tsne_kws": {
                "n_iter": 5000,
                "perplexity": 50,
                "scale": True,
            },
        },
    },
    "finite-RCos": {
        "figure": "Scatter",
        "features": [
            "pressureScale",
            "curtor",
            "phiedge",
            "pressure",
            "toroidalCurrent",
            "RCos",
        ],
        "figure_kwargs": {"figsize": [20, 20]},
        "plot_kwargs": {
            "apply": {
                "x": None,
                "y": None,
                "hue": "lambda x: x[-1, [1, 2, 3, 4]]",
            },
            "cmap": "coolwarm",
            "kind": "tsne",
            "xlabel": "t-SNE 1",
            "ylabel": "t-SNE 2",
            "cbar_label": [
                r"$R_{0,1}$ (m)",
                r"$R_{0,2}$ (m)",
                r"$R_{0,3}$ (m)",
                r"$R_{0,4}$ (m)",
            ],
            "cbar_limits": [(0.23, 0.31), (-0.16, 0), (-4e-3, 4e-3), (-4e-3, 0)],
            "annotate": {
                "apply": "lambda x: x.std()",
                "fmt": r"$\sigma = {:.1e}$ m",
            },
            "tsne_kws": {
                "n_iter": 5000,
                "perplexity": 50,
                "scale": True,
            },
        },
    },
    "finite-LambdaSin": {
        "figure": "Scatter",
        "features": [
            "pressureScale",
            "curtor",
            "phiedge",
            "pressure",
            "toroidalCurrent",
            "LambdaSin",
        ],
        "figure_kwargs": {"figsize": [8, 8]},
        "plot_kwargs": {
            "apply": {
                "x": None,
                "y": None,
                "hue": "lambda x: x[-1, [1, 2, 3, 4]]",
            },
            "cmap": "coolwarm",
            "kind": "tsne",
            "xlabel": "t-SNE 1",
            "ylabel": "t-SNE 2",
            "cbar_label": [
                r"$\lambda_{0,1}$ (rad)",
                r"$\lambda_{0,2}$ (rad)",
                r"$\lambda_{0,3}$ (rad)",
                r"$\lambda_{0,4}$ (rad)",
            ],
            "cbar_limits": [
                (5e-2, 1.2e-1),
                (-4e-3, 1.2e-2),
                (-7e-3, 0.0),
                (-2e-3, 3e-3),
            ],
            "annotate": {
                "apply": "lambda x: x.std()",
                "fmt": r"$\sigma = {:.1e}$ m",
            },
            "tsne_kws": {
                "n_iter": 5000,
                "perplexity": 50,
                "scale": True,
            },
        },
    },
    "finite-BCos": {
        "figure": "Scatter",
        "features": [
            "pressureScale",
            "curtor",
            "BCos",
        ],
        "figure_kwargs": {"figsize": [20, 20]},
        "plot_kwargs": {
            "apply": {
                "x": None,
                "y": None,
                "hue": "lambda x: x[-1, [37, 38, 0, 1]]",
            },
            "cmap": "coolwarm",
            "cbar_label": [
                r"$B_{1,0}$ (m)",
                r"$B_{1,1}$ (m)",
                r"$B_{0,0}$ (m)",
                r"$B_{0,1}$ (m)",
            ],
            "annotate": {
                "apply": "lambda x: x.std()",
                "fmt": r"$\sigma = {:.1e}$ m",
            },
        },
    },
    "pressure-and-toroidalCurrent": {
        "figure": "Profile",
        "features": ["pressure", "toroidalCurrent"],
        "figure_kwargs": {"figsize": [9, 12]},
        "plot_kwargs": {"marker": "o", "scale_index": [0, None], "alpha": 0.1},
    },
    "iota": {
        "figure": "Profile",
        "features": ["iota"],
        "figure_kwargs": {"figsize": [8, 6]},
        "plot_kwargs": {
            "title": [r"Iota Profile"],
            "ylabel": [None],
            "alpha": 0.1,
            "include_units": False,
        },
    },
    "iota-std": {
        "figure": "FnFigure",
        "figure_kwargs": {"figsize": [8, 6]},
        "features": ["iota"],
        "plot_kwargs": {
            "map_fn": lambda x: x.std(axis=0),
            "kind": "profile",
            "include_units": False,
        },
    },
    "pressureScaledAggregated": {
        "figure": "Profile",
        "features": ["pressure_scaled"],
        "columns": ["pressure"],
        "figure_kwargs": {"figsize": [12, 9]},
        "plot_kwargs": {
            "marker": "o",
            "ylabel": [r"Pressure Profile"],
            "aggregate": True,
        },
    },
    "pressureScaled": {
        "figure": "Profile",
        "features": ["pressure_scaled"],
        "columns": ["pressure"],
        "figure_kwargs": {"figsize": [5, 3.75]},
        "plot_kwargs": {
            "title": [r"Normalized Pressure Profile"],
            "ylabel": [r"$p(s) / p_0$"],
            "interp": True,
            "alpha": 0.1,
            "include_units": False,
        },
    },
    "toroidalCurrent": {
        "figure": "Profile",
        "features": ["toroidalCurrent"],
        "columns": ["toroidalCurrent"],
        "figure_kwargs": {"figsize": [5, 3.75]},
        "plot_kwargs": {
            "title": [r"Normalized Toroidal Current"],
            "ylabel": [r"$I(s) / I_{\text{tor}}$"],
            "interp": True,
            "alpha": 0.1,
            "include_units": False,
        },
    },
    "Space-profile-fourier": {
        "figure": "Profile",
        "features": ["RCos", "LambdaSin", "ZSin"],
        "figure_kwargs": {"figsize": [16, 10]},
        "plot_kwargs": {
            "modes": 12,
            "num_of_radial_points": 99,
            "space": "fourier",
            "m": [0],
            "n": [1],
            "alpha": 0.1,
            "scale_index": "max",
            "ylabel": [
                r"$\frac{R}{R_{max}}$",
                r"$\frac{\lambda}{\lambda_{max}}$",
                r"$\frac{Z}{Z_{max}}$",
            ],
        },
    },
    "RCos-profile": {
        "figure": "Profile",
        "features": ["RCos"],
        "figure_kwargs": {"figsize": [8, 6]},
        "plot_kwargs": {
            "space": "fourier",
            "m": [0, 1],
            "n": [0, 1],
            "modes": 12,
            "num_of_radial_points": 99,
            "alpha": 0.1,
            "scale_index": "max",
            "ylabel": [r"$\bar{R}_{mn}$"],
        },
    },
    "Space-closest-fourier": {
        "figure": "Closest",
        "features": ["coilCurrents", "phiedge", "RCos", "LambdaSin", "ZSin"],
        "plot_kwargs": {
            "features": ["coilCurrents", "phiedge"],
            "space": "fourier",
            "s": get_label_radial_indices(10),
            "m": range(6),
            "n": range(-4, 5),
            "metric": "rmsdstd",
        },
    },
    "Bsup-closest-fourier": {
        "figure": "Closest",
        "features": [
            "phiedge",
            "curtor",
            "pressure",
            "pressureScale",
            "toroidalCurrent",
            "BsupUCos",
            "BsupVCos",
        ],
        "plot_kwargs": {
            "features": [
                "phiedge",
                "curtor",
                "pressure",
                "pressureScale",
                "toroidalCurrent",
            ],
            "space": "fourier",
            "s": get_label_radial_indices(10),
            "m": range(4),
            "n": range(-4, 5),
            "metric": "rmsdstd",
        },
    },
    "Space-and-phiedge-correlation": {
        "figure": "CorrelationMap",
        "features": ["phiedge", "RCos", "LambdaSin", "ZSin"],
        "figure_kwargs": {"figsize": [10, 16]},
        "plot_kwargs": {"space": "fourier", "cmap": "vlag"},
    },
    "Space-and-curtor-correlation": {
        "figure": "CorrelationMap",
        "features": ["curtor", "RCos", "LambdaSin", "ZSin"],
        "figure_kwargs": {"figsize": [10, 16]},
        "plot_kwargs": {"space": "fourier", "cmap": "vlag"},
    },
    "Space-and-pressureScale-correlation": {
        "figure": "CorrelationMap",
        "features": ["pressureScale", "RCos", "LambdaSin", "ZSin"],
        "figure_kwargs": {"figsize": [10, 16]},
        "plot_kwargs": {"space": "fourier", "cmap": "vlag"},
    },
    "Space-and-iA+iB-correlation": {
        "figure": "CorrelationMap",
        "columns": ["coilCurrents", "RCos", "LambdaSin", "ZSin"],
        "features": ["iA+iB", "RCos", "LambdaSin", "ZSin"],
        "figure_kwargs": {"figsize": [10, 16]},
        "plot_kwargs": {"space": "fourier", "cmap": "vlag"},
    },
    "Space-and-iotaAtEdge-correlation": {
        "figure": "CorrelationMap",
        "columns": ["iota", "RCos", "LambdaSin", "ZSin"],
        "features": ["iota_edge", "RCos", "LambdaSin", "ZSin"],
        "figure_kwargs": {"figsize": [10, 16]},
        "plot_kwargs": {"space": "fourier", "s": [-1], "cmap": "vlag"},
    },
    "Bsup-and-iotaAtEdge-correlation": {
        "figure": "CorrelationMap",
        "columns": ["iota", "BsupUCos", "BsupVCos"],
        "features": ["iota_edge", "BsupUCos", "BsupVCos"],
        "figure_kwargs": {"figsize": [10, 16]},
        "plot_kwargs": {"space": "fourier", "radial": [-1], "cmap": "vlag"},
    },
    "Bsub-map-fourier": {
        "figure": "Map",
        "features": ["BsubSSin", "BsubUCos", "BsubVCos"],
        "figure_kwargs": {"figsize": [10, 16]},
        "plot_kwargs": {"space": "fourier", "s": [-1], "vmin": 1e-6, "vmax": 1},
    },
    "Bsup-map-fourier": {
        "figure": "Map",
        "features": ["BsupUCos", "BsupVCos"],
        "figure_kwargs": {"figsize": [10, 16]},
        "plot_kwargs": {"space": "fourier", "s": [-1], "vmin": 1e-6, "vmax": 1},
    },
    "Space-map-fourier": {
        "figure": "Map",
        "features": ["RCos", "LambdaSin", "ZSin"],
        "figure_kwargs": {"figsize": [6.66, 6.66 * 4 / 3]},
        "plot_kwargs": {
            "space": "fourier",
            "s": [-1],
            "vmin": 1e-6,
            "vmax": 1,
            "cmap": "magma",
        },
    },
    "BCos-map-fourier": {
        "figure": "Map",
        "features": ["BCos"],
        "figure_kwargs": {"figsize": [10, 10]},
        "plot_kwargs": {"space": "fourier", "s": [-1]},
    },
    "Space-map-real": {
        "figure": "Map",
        "features": ["RCos", "LambdaSin", "ZSin"],
        "figure_kwargs": {"figsize": [10, 16]},
        "plot_kwargs": {
            "s": [-1],
            "space": "real",
            "num_poloidal_angles": 9,
            "num_toroidal_angles": 18,
            "levels": 20,
            "hspace": 0.35,
            "cmap": "RdBu_r",
        },
    },
    "BCos-map-real": {
        "figure": "Map",
        "features": ["BCos"],
        "figure_kwargs": {"figsize": [10, 10]},
        "plot_kwargs": {
            "s": [-1],
            "space": "real",
            "poloidal_angles": 36,
            "toroidal_angles": 18,
            "levels": 10,
            "cmap": "plasma",
        },
    },
    "BCos-std-map-fourier": {
        "figure": "FnFigure",
        "figure_kwargs": {"figsize": [10, 8]},
        "features": ["BCos"],
        "plot_kwargs": {
            "map_fn": lambda x: x.std(axis=0),
            "space": "fourier",
            "s": [-1],
            "cmap": "magma",
            "log": True,
            "include_units": False,
        },
    },
    "BCos-std-map-real": {
        "figure": "FnFigure",
        "figure_kwargs": {"figsize": [10, 8]},
        "features": ["BCos"],
        "plot_kwargs": {
            "map_fn": lambda x: x.std(axis=0),
            "space": "real",
            "cmap": "magma",
            "log": False,
            "poloidal_angles": 36,
            "toroidal_angles": 18,
            "s": [-1],
        },
    },
    "Space-variability-map-fourier": {
        "figure": "FnFigure",
        "figure_kwargs": {"figsize": [10, 16]},
        "features": ["RCos", "LambdaSin", "ZSin"],
        "plot_kwargs": {
            "map_fn": lambda x: x.std(axis=0)
            / abs(np.quantile(x, 0.75, axis=0) - np.quantile(x, 0.25, axis=0)),
            "space": "fourier",
            "s": [-1],
            "log": False,
            "annotate": True,
            "cmap": "Blues",
        },
    },
    "Space-std-map-fourier": {
        "figure": "FnFigure",
        "figure_kwargs": {"figsize": [10, 16]},
        "features": ["RCos", "LambdaSin", "ZSin"],
        "columns": ["RCos", "LambdaSin", "ZSin"],
        "plot_kwargs": {
            "map_fn": lambda x: x.std(axis=0),
            "space": "fourier",
            "s": [-1],
            "cmap": "plasma",
            "log": False,
        },
    },
    "Space-std-map-real": {
        "figure": "FnFigure",
        "figure_kwargs": {"figsize": [10, 16]},
        "features": ["RCos", "LambdaSin", "ZSin"],
        "plot_kwargs": {
            "map_fn": lambda x: x.std(axis=0),
            "space": "real",
            "cmap": "plasma",
            "log": False,
            "poloidal_angles": 18,
            "toroidal_angles": 9,
        },
    },
    "Space-corr-map-fourier": {
        "figure": "FnFigure",
        "figure_kwargs": {"figsize": [10, 16]},
        "features": ["RCos", "LambdaSin", "ZSin"],
        "columns": ["RCos", "LambdaSin", "ZSin"],
        "plot_kwargs": {
            "map_fn": lambda x: np.array(
                [np.corrcoef(x[:, -287], x[:, i])[0, 1] for i in range(x.shape[1])]
            ),
            "space": "fourier",
            "s": [-1],
            "cmap": "coolwarm",
            "log": False,
        },
    },
    "Space-variability-map-real": {
        "figure": "FnFigure",
        "figure_kwargs": {"figsize": [10, 16]},
        "features": ["RCos", "LambdaSin", "ZSin"],
        "plot_kwargs": {
            "space": "real",
            "num_poloidal_angles": 18,
            "num_toroidal_angles": 18,
            "map_fn": lambda x: x.std(axis=0)
            / abs(np.quantile(x, 0.75, axis=0) - np.quantile(x, 0.25, axis=0)),
            "s": [-1],
        },
    },
    "Space-surface": {
        "figure": "Surface",
        "features": ["RCos", "ZSin"],
        "figure_kwargs": {"figsize": [16, 16]},
        "plot_kwargs": {"num_toroidal_angles": 9},
    },
    "Space-resolution-map": {
        "figure": "MetricFigure",
        "figure_kwargs": {"figsize": [12, 16]},
        "columns": ["RCos", "LambdaSin", "ZSin"],
        "features": [
            "RCos",
            "LambdaSin",
            "ZSin",
            "RCos_r5f8",
            "LambdaSin_r5f8",
            "ZSin_r5f8",
        ],
        "plot_kwargs": {
            "metric": "rmse",
            "space": "real",
            "num_poloidal_angles": 9,
            "num_toroidal_angles": 18,
            "hspace": 0.35,
            "cmap": "inferno",
            "s": [-1],
            "levels": 20,
            "annotate": True,
            "annotate_fmt": r"$rmse = {:.1e}$",
        },
    },
    "fourier-pairplot": {
        "figure": "Pairplot",
        "features": ["RCos", "LambdaSin"],
        "plot_kwargs": {
            "space": "fourier",
            "s": [98],
            "m": [0],
            "n": [1, 2, 3, 4],
            "diag_kind": "kde",
            "corner": True,
        },
    },
    "models-comparison": {
        "figure": "BoxPlot",
        "features": ["fourier_rmsdiqr", "resolution", "estimator"],
        "plot_kwargs": {"y": "fourier_rmsdiqr", "hue": "estimator", "x": "resolution"},
    },
    "kfold-cv-results": {
        "figure": "BoxPlot",
        "figure_kwargs": {"figsize": [12, 9]},
        "features": ["real_rmsdstd", "cv_iteration", "task"],
        "plot_kwargs": {
            "y": "real_rmsdstd",
            "hue": "cv_iteration",
            "x": "task",
            "bootstrap": 10000,
        },
    },
    "cv-results": {
        "figure": "PointPlot",
        "figure_kwargs": {"figsize": [12, 9]},
        "features": ["real_rmsdstd", "cv_iteration", "task"],
        "plot_kwargs": {
            "y": "real_rmsdstd",
            "x": "task",
            "join": False,
            "n_boot": 10000,
        },
    },
}


def make_figure(figure_name, data, folder):
    """Make a generic figure.

    Args:
        figure_name (str): String figure identifier.
        data (pd.DataFrame): Pandas dataframe with data to plot.
        folder (str): Path for the folder where to save the figure.
    """

    logger = logging.getLogger()

    figure = FIGURES[figure_name]
    figure_class = get_figure(figure.pop("figure"))
    figure_features = figure.pop("features")
    figure_kwargs = figure.pop("figure_kwargs", {})
    plot_kwargs = figure.pop("plot_kwargs", {})

    figure_path = join(folder, figure_name)
    figure_path = "{}.pdf".format(figure_path)

    fig = figure_class(**figure_kwargs)
    fig.plot(data[figure_features], **plot_kwargs)
    fig.save(figure_path)

    logger.info("Plot saved in %s", figure_path)


def add_features(dataframe, features):
    """Extract and add additioanl features.

    Do not enhance dataframe if feature is not required by figures.

    Args:
        dataframe (pd.DataFrame): Input dataframe.
        features (list): List of features to add.

    Returns:
        pd.DataFrame: Enhanced dataframe.
    """

    logger = logging.getLogger()
    logger.info("Enhance dataset with additional features...")

    #  Coil currents
    if "coilCurrents" in dataframe.columns:
        if set(["I1", "I2", "I3", "I4", "I5", "IA", "IB"]) & set(features) != set():
            dataframe["I1"] = dataframe["coilCurrents"].apply(lambda x: x[0])
            dataframe["I2"] = dataframe["coilCurrents"].apply(lambda x: x[1])
            dataframe["I3"] = dataframe["coilCurrents"].apply(lambda x: x[2])
            dataframe["I4"] = dataframe["coilCurrents"].apply(lambda x: x[3])
            dataframe["I5"] = dataframe["coilCurrents"].apply(lambda x: x[4])
            dataframe["IA"] = dataframe["coilCurrents"].apply(lambda x: x[5])
            dataframe["IB"] = dataframe["coilCurrents"].apply(lambda x: x[6])

        #  Coil currents combinations
        if "IA+IB" in features or "IA_IB" in features:
            dataframe["IA+IB"] = dataframe[["IA", "IB"]].apply(lambda x: x[0] + x[1], 1)
            dataframe["IA_IB"] = dataframe[["IA", "IB"]].apply(lambda x: x[0] - x[1], 1)

        #  Ratios
        if set(["i1", "i2", "i3", "i4", "i5", "iA", "iB"]) & set(features) != set():
            dataframe["i2"] = dataframe["coilCurrents"].apply(lambda x: x[1] / x[0])
            dataframe["i3"] = dataframe["coilCurrents"].apply(lambda x: x[2] / x[0])
            dataframe["i4"] = dataframe["coilCurrents"].apply(lambda x: x[3] / x[0])
            dataframe["i5"] = dataframe["coilCurrents"].apply(lambda x: x[4] / x[0])
            dataframe["iA"] = dataframe["coilCurrents"].apply(lambda x: x[5] / x[0])
            dataframe["iB"] = dataframe["coilCurrents"].apply(lambda x: x[6] / x[0])

        if "iA+iB" in features or "iA_iB" in features:
            dataframe["iA+iB"] = dataframe["coilCurrents"].apply(
                lambda x: (x[5] + x[6]) / x[0]
            )
            dataframe["iA_iB"] = dataframe["coilCurrents"].apply(
                lambda x: (x[5] - x[6]) / x[0]
            )

    #  Iota at the edge
    if "iota" in dataframe.columns:
        if "iota_axis" in features or "iota_edge" in features:
            dataframe["iota_axis"] = dataframe["iota"].apply(lambda x: x[0])
            dataframe["iota_edge"] = dataframe["iota"].apply(lambda x: x[-1])

    #  Pressure
    if "pressure" in dataframe.columns:
        dataframe["pressure_axis"] = dataframe["pressure"].apply(lambda x: x[0])
        dataframe["pressure_edge"] = dataframe["pressure"].apply(lambda x: x[-1])
        dataframe["pressure_scaled"] = dataframe["pressure"].apply(lambda x: x / x[0])

    #  Toroidal Current
    if "toroidalCurrent" in dataframe.columns:
        dataframe["toroidalCurrent_axis"] = dataframe["toroidalCurrent"].apply(
            lambda x: x[0]
        )
        dataframe["toroidalCurrent_edge"] = dataframe["toroidalCurrent"].apply(
            lambda x: x[-1]
        )

    #  VMEC labels
    for label in get_all_labels():
        if label in dataframe.columns:

            label_axis = "{}_axis".format(label)
            if label_axis in features:
                #  TODO(@amerlo): BCos has all null values at index 0.
                index = 1 if label == "BCos" else 0
                dataframe[label_axis] = dataframe[label].apply(lambda x: x[index][0])

            label_edge = "{}_edge".format(label)
            if label_edge in features:
                dataframe[label_edge] = dataframe[label].apply(lambda x: x[-1][0])

            for f in features:
                resolution = re.match(r"{}_r([0-9]*)f([0-9]*)".format(label), f)
                if resolution:
                    dataframe[f] = dataframe[label].apply(
                        lambda x: to_downsampled_shape(
                            x, int(resolution.group(1)), int(resolution.group(2))
                        ),
                        1,
                    )

    return dataframe


def use_nyquist(data, label):
    """Check if we have to use nyquist resolution.

    TODO(@amerlo): This function has to be removed in case data are generated
    with the newest VMEC version.
    """

    if "finite" in "".join(data) and (
        "BCos" in label or "Bsup" in label or "Bsub" in label
    ):
        return True
    return False


def make_dataset_figures(data, figures, figures_path, num_samples):
    """Make figures of input dataset.

    Args:
        batch (str): The bacth name for which plot figures.
        figures (list): List of figures to generate.
        figures_path (str): Folder where to store figures.
        num_samples (int): Number of samples to retrieve from data.
    """

    columns = []
    for figure in figures:
        columns.extend(FIGURES[figure].pop("columns", FIGURES[figure]["features"]))
    columns = list(set(columns))

    #  Retrieve correct label for columns
    label = "Space"
    for c in columns:
        for label_, f in FIELD_COMPONENTS.items():
            if c in f:
                label = label_
                break
    dataset_features = list(set(columns) - set(label))

    dataset = VmecDataset(data=data, num_samples=num_samples)
    ds = dataset.input_fn(
        features=dataset_features,
        label=label,
        return_raw_runs=True,
        use_nyquist_resolution=use_nyquist(data, label),
    )

    dataframe = to_dataframe(ds.as_numpy_iterator())

    logger = logging.getLogger()
    logger.info("%s data has been read back", data)
    logger.info("%d runs have been found", len(dataframe))

    features = []
    for figure in figures:
        features.extend(FIGURES[figure]["features"])
    features = list(set(features))
    dataframe = add_features(dataframe, features)

    for figure in figures:
        make_figure(figure, dataframe, figures_path)


def get_data_from_experiment(file_path):
    """Read data from tasks file.

    Args:
        file_path (str): Tasks file to plot.

    Returns:
        A pandas DataFrame.
    """

    #  TODO(@amerlo): Read back all experiment data here
    dataframe = read_csv(file_path)

    return dataframe


def make_experiments_figures(files, figures, figures_path):
    """Make experiements figures.

    Args:
        files (list): List of tasks files to plot from.
        figures_path (str): Folder where to store figures.
    """

    #  Read data from experiments
    dataframe = concat([get_data_from_experiment(f) for f in files], sort=True)

    for figure in figures:
        make_figure(figure, dataframe, figures_path)


def main():
    """Main function for figures generation."""

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    #  TODO(@amerlo): Support multiple data folders
    parser = argparse.ArgumentParser(description="VMEC figures generation.")
    parser.add_argument(
        "--data",
        default=None,
        help="Folder where to read data",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "--files",
        default=None,
        help="Tasks files to plot",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "--num_samples", default=None, help="Number of samples to use", type=int
    )
    parser.add_argument(
        "--figures",
        default=FIGURES.keys(),
        help="Figures to plot",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "--out_dir",
        default=None,
        help="Folder where to save experiement figures",
        type=str,
    )
    args = parser.parse_args()
    logging.info("VMEC dataset figures script.")

    if args.files is not None:

        experiments_figures_path = (
            args.out_dir
            if args.out_dir is not None
            else join(FIGURES_PATH, "experiment")
        )
        if not isdir(experiments_figures_path):
            makedirs(experiments_figures_path)

        make_experiments_figures(args.files, args.figures, experiments_figures_path)

    else:

        if len(args.data) != 1:
            data_figures_path = join(FIGURES_PATH, "experiment")
        else:
            data_figures_path = join(FIGURES_PATH, basename(args.data[0]))

        if not isdir(data_figures_path):
            makedirs(data_figures_path)

        if args.data is None:
            raise ValueError("Please specify at least one data folder")

        make_dataset_figures(
            args.data, args.figures, data_figures_path, args.num_samples
        )


if __name__ == "__main__":
    main()
