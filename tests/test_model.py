import unittest

from vmec_dnn_surrogate.lib.models.conv3d import get_num_of_filters


class Get_Num_Of_Filters_Test(unittest.TestCase):
    """Test cases for the get_num_of_filters() function."""

    def test_scaling(self):
        """Test upscaling/downscaling number of filters."""

        test_cases = [
            (2, 16, 0.5, [16, 8]),
            (2, 16, 2, [16, 32]),
            (2, [16, 16], 0.5, [16, 16]),
            (2, [16, 16], 2, [16, 16]),
            (1, 16, 2, [16]),
            (1, 16, 0.5, [16]),
            (1, [16], 2, [16]),
            (4, 64, 0.5, [64, 32, 16, 8]),
        ]

        for (layers, filters, factor, out) in test_cases:
            self.assertEqual(get_num_of_filters(layers, filters, factor), out)


if __name__ == "__main__":
    unittest.main()
