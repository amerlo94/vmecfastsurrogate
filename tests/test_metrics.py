import unittest

import numpy as np

from vmec_dnn_surrogate.lib.metrics import get_metric


class Metrics_Test(unittest.TestCase):
    """Test cases for the metric functions."""

    samples = 10
    outputs = 6
    metrics = ["mse", "mae", "mape", "rmse", "rmsdiqr", "rmsdstd", "r2score"]

    test_samples = [
        (
            np.ones((samples, outputs), dtype=np.float32),
            np.ones((samples, outputs), dtype=np.float32),
            {
                "mse": (0.0, np.zeros(outputs)),
                "mae": (0.0, np.zeros(outputs)),
                "mape": (0.0, np.zeros(outputs)),
                "rmse": (0.0, np.zeros(outputs)),
                "rmsdiqr": (0.0, np.zeros(outputs)),
                "rmsdstd": (0.0, np.zeros(outputs)),
                "r2score": (1.0, np.ones(outputs)),
            },
        ),
        (
            np.zeros((samples, outputs), dtype=np.float32),
            np.zeros((samples, outputs), dtype=np.float32),
            {
                "mse": (0.0, np.zeros(outputs)),
                "mae": (0.0, np.zeros(outputs)),
                "mape": (0.0, np.zeros(outputs)),
                "rmse": (0.0, np.zeros(outputs)),
                "rmsdiqr": (0.0, np.zeros(outputs)),
                "rmsdstd": (0.0, np.zeros(outputs)),
                "r2score": (1.0, np.ones(outputs)),
            },
        ),
        (
            np.array([[1, 2], [2, 3]], dtype=np.float32),
            np.array([[2, 1], [1, 5]], dtype=np.float32),
            {
                "mse": (1.75, np.array([1, 2.5])),
                "mae": (1.25, np.array([1, 1.5])),
                "mape": (0.725, np.array([0.75, 0.7])),
                "rmse": (1.290569415, np.array([1, 1.58113883])),
                "rmsdiqr": (1.3952847075210475, np.array([2, 0.7905694])),
                "rmsdstd": (1.3952847075210475, np.array([2, 0.7905694])),
                "r2score": (-1.3125, np.array([-3.0, 0.375])),
            },
        ),
    ]

    def test_average_results(self):
        """Test metrics averate output."""

        for metric in self.metrics:
            for (value, ref, out) in self.test_samples:
                np.testing.assert_almost_equal(
                    get_metric(metric)(value, ref, multioutput="uniform_average"),
                    out[metric][0],
                )

    def test_raw_results(self):
        """Test metrics raw values output."""

        for metric in self.metrics:
            for (value, ref, out) in self.test_samples:
                np.testing.assert_almost_equal(
                    get_metric(metric)(value, ref, multioutput="raw_values"),
                    out[metric][1],
                )


if __name__ == "__main__":
    unittest.main()
