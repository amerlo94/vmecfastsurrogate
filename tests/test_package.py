#!/usr/bin/env python

"""Tests for `vmec_dnn_surrogate` package."""

import unittest
import vmec_dnn_surrogate


class TestPackage(unittest.TestCase):
    """Tests for `vmec_dnn_surrogate` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_version_type(self):
        """Assure that version type is str."""

        self.assertIsInstance(vmec_dnn_surrogate.__version__, str)
