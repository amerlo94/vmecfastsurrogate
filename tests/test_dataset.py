import unittest

from random import randint
import numpy as np
import tensorflow as tf

from vmec_dnn_surrogate.lib.dataset import (
    partition_fn,
    build_sample,
    downsample_label_tensor,
    get_label_radial_indices,
)


class Samples:
    """Base class."""

    def setUp(self):
        """Set partition_fn."""

        self.val_size = 0.1
        self.random_state = 0
        self.num_of_inner_validation_runs = 10

        self.partition_fn = partition_fn(
            self.samples,
            self.validation_mode,
            self.test_size,
            self.val_size,
            self.num_of_inner_validation_runs,
            0,
        )

    @property
    def test_size(self):
        """Ratio of test size."""

        return 0.2

    @property
    def num_of_outer_validation_runs(self):
        """Number of validation runs."""

        return int(1 / self.test_size)


class Int_Samples(Samples):
    """Int samples to test."""

    def setUp(self):
        """Set partition_fn."""
        self.samples = np.array(range(100, 200))
        super(Int_Samples, self).setUp()


class String_Samples(Samples):
    """String samples to test."""

    def setUp(self):
        """Set partition_fn."""
        self.samples = np.array(
            ["".join([chr(randint(97, 122)) for _ in range(10)]) for _ in range(100)]
        )
        super(String_Samples, self).setUp()


class Base_Checks:
    """Base checks which every case should pass."""

    def test_out_of_set_samples(self):
        """Test if partitions keep only data set samples."""

        for _ in range(self.num_of_outer_validation_runs):
            for _ in range(self.num_of_inner_validation_runs):

                partition = next(self.partition_fn)
                self.assertEqual(
                    set(partition["train"]).difference(set(self.samples)), set()
                )
                self.assertEqual(
                    set(partition["val"]).difference(set(self.samples)), set()
                )
                self.assertEqual(
                    set(partition["test"]).difference(set(self.samples)), set()
                )

    def test_partition_size(self):
        """Test partitions size."""

        test_size = int(len(self.samples) * self.test_size)
        val_size = int(len(self.samples) * self.val_size)
        train_size = len(self.samples) - test_size - val_size

        for _ in range(self.num_of_outer_validation_runs):
            for _ in range(self.num_of_inner_validation_runs):

                partition = next(self.partition_fn)
                self.assertEqual(len(partition["val"]), val_size)
                self.assertEqual(len(partition["test"]), test_size)

                #  In bootstrap we may use resample
                if self.validation_mode != "bootstrap":
                    self.assertEqual(len(partition["train"]), train_size)

    def test_samples_proliferation(self):
        """Test for samples proliferation in partitions."""

        for _ in range(self.num_of_outer_validation_runs):
            for _ in range(self.num_of_inner_validation_runs):

                partition = next(self.partition_fn)
                self.assertEqual(set(partition["train"]) & set(partition["val"]), set())
                self.assertEqual(
                    set(partition["train"]) & set(partition["test"]), set()
                )
                self.assertEqual(set(partition["test"]) & set(partition["val"]), set())

    def tearDown(self):
        """Delete partition_fn object."""

        del self.partition_fn


class Inner_Checks:
    """Check for proliferation across inner runs."""

    def test_inner_samples_proliferation(self):
        """Test if samples from the test partitions are consistent in inner loop."""

        for i in range(self.num_of_outer_validation_runs):
            test = next(self.partition_fn)["test"]
            for j in range(self.num_of_inner_validation_runs - 1):
                self.assertEqual(
                    (set(test) | set(next(self.partition_fn)["test"])), set(test)
                )


class Same_Checks:
    """Check for the same/static validation mode."""

    def test_same_partition(self):
        """Test if samples in partitions are the same."""

        partition = next(self.partition_fn)

        for _ in range(self.num_of_outer_validation_runs - 1):
            for _ in range(self.num_of_inner_validation_runs):

                partition_ = next(self.partition_fn)
                self.assertEqual((partition_["test"] == partition["test"]).all(), True)


class Resample_KFold_Checks:
    """Check for resampled partitions for KFold mode."""

    def test_resampling_proliferation(self):
        """Test samples proliferation in resampled partitions."""

        test_sets = []

        for _ in range(self.num_of_outer_validation_runs):

            test_set = next(self.partition_fn)["test"]

            for _ in range(self.num_of_inner_validation_runs - 1):
                _ = next(self.partition_fn)

            test_sets.append(test_set)

        for i, outer in enumerate(test_sets):
            for j, inner in enumerate(test_sets):

                if i == j:
                    continue

                self.assertEqual(
                    set(outer) & set(inner),
                    set(),
                )


class Resample_Repeated_KFold_Checks:
    """Check for resampled partitions for repeated KFold mode."""

    def test_inner_resampling_proliferation(self):
        """Test samples proliferation in inner resampled partitions."""

        for _ in range(self.num_of_inner_validation_runs):

            test_sets = []

            for _ in range(self.num_of_outer_validation_runs):
                test_sets.append(next(self.partition_fn)["test"])

            for i, outer in enumerate(test_sets):
                for j, inner in enumerate(test_sets):

                    if i == j:
                        continue

                    self.assertEqual(
                        set(outer) & set(inner),
                        set(),
                    )

    def test_outer_resampling_proliferation(self):
        """Test that any two test partitions are not equal."""

        test_sets = []

        for _ in range(self.num_of_inner_validation_runs):

            for _ in range(self.num_of_outer_validation_runs):
                test_sets.append(next(self.partition_fn)["test"])

        for i, outer in enumerate(test_sets):
            for j, inner in enumerate(test_sets):

                if i == j:
                    continue

                self.assertNotEqual(set(outer), set(inner))


class Same_PartitionFn_Test(
    Int_Samples, Base_Checks, Inner_Checks, Same_Checks, unittest.TestCase
):
    """Test same/static validation mode in partition_fn."""

    def setUp(self):
        """Set partition_fn args."""
        self.validation_mode = "same"
        super(Same_PartitionFn_Test, self).setUp()


class Bootstrap_PartitionFn_Test(
    Int_Samples, Base_Checks, Inner_Checks, unittest.TestCase
):
    """Test bootstrap validation mode in partition_fn."""

    def setUp(self):
        """Set partition_fn args."""
        self.validation_mode = "bootstrap"
        super(Bootstrap_PartitionFn_Test, self).setUp()


class KFold_PartitionFn_Test(
    Int_Samples, Base_Checks, Inner_Checks, Resample_KFold_Checks, unittest.TestCase
):
    """Test KFold validation mode in partition_fn."""

    def setUp(self):
        """Set partition_fn args."""
        self.validation_mode = "KFold"
        super(KFold_PartitionFn_Test, self).setUp()


class Repeated_KFold_PartitionFn_Test(
    Int_Samples, Base_Checks, Resample_Repeated_KFold_Checks, unittest.TestCase
):
    """Test repeated KFold validation mode in partition_fn."""

    def setUp(self):
        """Set partition_fn args."""
        self.validation_mode = "repeatedKFold"
        super(Repeated_KFold_PartitionFn_Test, self).setUp()


class Same_Strings_PartitionFn_Test(
    String_Samples, Base_Checks, Same_Checks, unittest.TestCase
):
    """Test same/static validation mode in partition_fn with strings."""

    def setUp(self):
        """Set partition_fn args."""
        self.validation_mode = "same"
        super(Same_Strings_PartitionFn_Test, self).setUp()


class Bootstrap_Strings_PartitionFn_Test(
    String_Samples, Base_Checks, Inner_Checks, unittest.TestCase
):
    """Test bootstrap validation mode in partition_fn with strings."""

    def setUp(self):
        """Set partition_fn args."""
        self.validation_mode = "bootstrap"
        super(Bootstrap_Strings_PartitionFn_Test, self).setUp()


class KFold_Strings_PartitionFn_Test(
    String_Samples, Base_Checks, Inner_Checks, unittest.TestCase
):
    """Test KFold validation mode in partition_fn with strings."""

    def setUp(self):
        """Set partition_fn args."""
        self.validation_mode = "KFold"
        super(KFold_Strings_PartitionFn_Test, self).setUp()


class Repeated_KFold_Strings_PartitionFn_Test(
    String_Samples, Base_Checks, Resample_Repeated_KFold_Checks, unittest.TestCase
):
    """Test repeated KFold validation mode in partition_fn with strings."""

    def setUp(self):
        """Set partition_fn args."""
        self.validation_mode = "repeatedKFold"
        super(Repeated_KFold_Strings_PartitionFn_Test, self).setUp()


class Build_Sample_Test(unittest.TestCase):
    """Test of the build_sample() method."""

    def test_output_tuple_null_modes(self):
        """Test output tuple for null modes kwarg."""

        test_samples = [
            (
                {
                    "x": tf.constant([1]),
                    "y": tf.constant(np.arange(6).reshape(1, 6), dtype=tf.float32),
                    "z": tf.constant(
                        np.arange(6).reshape(1, 6) * -1.0, dtype=tf.float32
                    ),
                },
                (
                    {"x": tf.constant([1])},
                    tf.constant(
                        [[[0, 0], [1, -1], [2, -2], [3, -3], [4, -4], [5, -5]]],
                        dtype=tf.float32,
                    ),
                ),
            ),
            (
                {
                    "x": tf.constant([1]),
                    "y": tf.constant(np.arange(6).reshape(6, 1), dtype=tf.float32),
                    "z": tf.constant(
                        np.arange(6).reshape(6, 1) * -1.0, dtype=tf.float32
                    ),
                },
                (
                    {"x": tf.constant([1])},
                    tf.constant(
                        [
                            [[0, 0]],
                            [[1, -1]],
                            [[2, -2]],
                            [[3, -3]],
                            [[4, -4]],
                            [[5, -5]],
                        ],
                        dtype=tf.float32,
                    ),
                ),
            ),
        ]

        for (in_, out) in test_samples:
            #  Test tuple features
            self.assertEqual(build_sample(in_, ["x"], ["y", "z"])[0], out[0])
            #  Test tuple label
            self.assertTrue(
                tf.math.reduce_all(
                    tf.math.equal(build_sample(in_, ["x"], ["y", "z"])[1], out[1])
                )
            )

    def test_output_tuple_modes(self):
        """Test cases for the Fourier series labels."""

        array = np.arange(6).reshape(1, 6)
        sample = {
            "x": tf.constant([1]),
            "y": tf.constant(array[:, 1:], dtype=tf.float32),
            "z": tf.constant(array[:, 1:] * -1.0, dtype=tf.float32),
        }

        test_samples = [
            (
                sample,
                [2, 1],
                (
                    {"x": tf.constant([1])},
                    tf.constant(
                        [[[0, 0], [1, -1], [2, -2]], [[3, -3], [4, -4], [5, -5]]],
                        dtype=tf.float32,
                    ),
                ),
            ),
        ]

        for (in_, modes, out) in test_samples:
            #  Test tuple features
            self.assertEqual(
                build_sample(in_, ["x"], ["y", "z"], modes=modes)[0],
                out[0],
            )
            #  Test tuple label
            self.assertTrue(
                tf.math.reduce_all(
                    tf.math.equal(
                        build_sample(in_, ["x"], ["y", "z"], modes=modes)[1],
                        out[1],
                    )
                )
            )


class Downsample_Label_Tensor_Test(unittest.TestCase):
    """Test of the downsample_label_tensor() method."""

    def test_output_label_null_modes(self):
        """Test output for non Fourier series labels."""

        array = np.arange(10).reshape(10, 1)
        tensor = tf.constant(array)

        test_samples = [
            (tensor, None, tensor),
            (tensor, [1, 2], tf.constant(array[[1, 2], :])),
            (tensor, [0, 9], tf.constant(array[[0, 9], :])),
        ]

        for (in_, radial, out) in test_samples:
            self.assertTrue(
                tf.math.reduce_all(
                    tf.math.equal(
                        downsample_label_tensor(
                            features=None, label=in_, radial_indices=radial
                        )[1],
                        out,
                    )
                )
            )

    def test_output_label_modes(self):
        """Test output for Fourier series labels."""

        #  Test cases is a list of (radial, m_pol, n_tor, comp)
        test_cases = [(10, 3, 2, 3), (10, 19, 18, 1)]

        for test in test_cases:
            self._test_output_label_modes(*test)

    def _test_output_label_modes(self, radial, m_pol, n_tor, comp):
        """Test output for Fourier series labels."""

        array = np.arange(radial * m_pol * (2 * n_tor + 1) * comp).reshape(
            radial, m_pol, 2 * n_tor + 1, comp
        )
        tensor = tf.constant(array)

        #  Create test samples
        radial_locations = np.random.randint(radial - 1, size=int(radial / 2))
        modes = [
            np.random.randint(m_pol - 1),
            np.random.randint(n_tor - 1),
        ]

        test_samples = [
            (tensor, None, None, None, tensor),
            (
                tensor,
                radial_locations,
                None,
                None,
                tf.constant(array[radial_locations, ...]),
            ),
            (
                tensor,
                [0, radial - 1],
                None,
                None,
                tf.constant(array[[0, radial - 1], ...]),
            ),
            (tensor, None, None, [m_pol, n_tor], tensor),
            (tensor, None, [m_pol - 1, n_tor - 1], None, tensor),
            (
                tensor,
                None,
                modes,
                [m_pol, n_tor],
                tf.constant(
                    array[:, : modes[0], n_tor - modes[1] : n_tor + modes[1] + 1, :]
                ),
            ),
            (
                tensor,
                radial_locations,
                modes,
                [m_pol, n_tor],
                tf.constant(
                    array[
                        radial_locations,
                        : modes[0],
                        n_tor - modes[1] : n_tor + modes[1] + 1,
                        :,
                    ]
                ),
            ),
        ]

        for (in_, radial, modes, orig_modes, out) in test_samples:
            print(modes, orig_modes)
            self.assertTrue(
                tf.math.reduce_all(
                    tf.math.equal(
                        downsample_label_tensor(
                            features=None,
                            label=in_,
                            radial_indices=radial,
                            modes=modes,
                            orig_modes=orig_modes,
                        )[1],
                        out,
                    )
                )
            )


class Get_Label_Radial_Indices_Test(unittest.TestCase):
    """Test of the get_label_radial_indices() method."""

    full = np.linspace(start=0, stop=98, num=99, dtype=int)

    def test_radial_indices(self):
        """Test correct radial indices."""

        test_cases = [
            (None, self.full),
            (99, self.full),
            (1, np.array([98])),
            (2, np.array([0, 98])),
            (5, np.array([0, 7, 25, 56, 98])),
            (
                25,
                np.array(
                    [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        7,
                        9,
                        11,
                        14,
                        18,
                        21,
                        25,
                        29,
                        34,
                        39,
                        44,
                        50,
                        56,
                        62,
                        69,
                        76,
                        83,
                        91,
                        98,
                    ]
                ),
            ),
        ]

        for (in_, out) in test_cases:
            np.testing.assert_equal(get_label_radial_indices(in_), out)
