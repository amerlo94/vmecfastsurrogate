"""Tests for VMEC routines."""

import os
import unittest

import numpy as np
import netCDF4

from vmec_dnn_surrogate.lib.vmec import compute_shear


class TestVmecQoi(unittest.TestCase):
    """
    Test evaluation of physics quantities of interest.
    """

    test_vmec_ids = [
        "minerva-vmec-a6fdfed8e63efd7a0e4f667933389c18",
        "w7x.0972_0926_0880_0852_+0000_+0000.01.00jh",
    ]

    def test_compute_dshear_with_iotas(self):
        for vmec_id in self.test_vmec_ids:
            path = os.path.join("tests", "resources", f"wout_{vmec_id}.nc")
            wout = netCDF4.Dataset(path)
            #  Iota on half-mesh is needed to compare the dshear
            iota = wout["iotas"][:]
            phiedge = wout["phi"][-1]
            #  Magnetc shear as in the Mercier stability criterion
            #  thus Dshear = (shear ** 2) / 4
            dshear = wout["DShear"][:]
            dshear = np.sqrt(4 * dshear)
            shear = compute_shear(iota, phiedge)
            np.testing.assert_almost_equal(shear, dshear)

    def test_compute_dshear_with_iotaf(self):
        for vmec_id in self.test_vmec_ids:
            path = os.path.join("tests", "resources", f"wout_{vmec_id}.nc")
            wout = netCDF4.Dataset(path)
            #  Iotaf to iotaf should be handled inside get_shear
            iota = wout["iotaf"][:]
            phiedge = wout["phi"][-1]
            #  Magnetc shear as in the Mercier stability criterion
            #  thus Dshear = (shear ** 2) / 4
            dshear = wout["DShear"][:]
            dshear = np.sqrt(4 * dshear)
            shear = compute_shear(iota, phiedge, to_iotas=True)
            np.testing.assert_almost_equal(shear, dshear)


if __name__ == "__main__":
    unittest.main()
