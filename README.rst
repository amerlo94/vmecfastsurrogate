====================================
Proof of concept of a fast surrogate model of the VMEC code via neural networks in Wendelstein 7-X scenarios
====================================

``vmec_dnn_surrogate`` is a framework for computing VMEC MHD equilibria with neural networks. It contains code for data generation, network training, and evaluation. Linux is highly recommended.

Source code for the `Proof of concept of a fast surrogate model of the VMEC code via neural networks in Wendelstein 7-X scenarios <https://doi.org/10.1088/1741-4326/ac1a0d>`_ paper.

Install
-------

Clone this repository and install it:

.. code-block:: bash

                $ pip install -e .[all]

This project requires the ``vmec`` branch from the `w7x <https://gitlab.mpcdf.mpg.de/dboe/w7x.git>`_ repository.
Please clone the ``w7x`` repository locally, checkout the ``vmec`` branch and include it under your ``PYTHONPATH``.

Data
----

All the VMEC runs used to train the models are versioned with `dvc <https://dvc.org/>`_.
Please contact `Andrea Merlo <mailto:andrea.merlo@ipp.mpg.de>`_ to access the remote
shared data folder.

Then use dvc to pull all the data sets.

.. code-block:: bash

                $ dvc pull

Training and Evaluation
-----------------------

This project makes use of `dvc <https://dvc.org/>`_. Check the project DAG via:

.. code-block:: bash

                $ dvc dag

Please check dvc `Data Pipelines <https://dvc.org/doc/start/data-pipelines#data-pipelines>`_
documentation to learn more about how to use it.

Each task has its own set of pipeline stages,
where only the tasks with the same beta scenario share the same ``make_scalers`` stage.

To reproduce a stage, do:

.. code-block:: bash

                $ dvc repro <stage_name>

The main figures used to describe the data set can be reproduced with the ``figures-null`` and ``figures-finite`` dvc stages.

The data for the resolution scan can be reproduced with the ``iota-resolution-scan``, ``space-resolution-scan`` and ``bcos-resolution-scan``.

For each task, the main results can then be reproduced via the following dvc stages:

* ``search-<task-string>`` will perform a HP search as defined in the task yaml configuration file;
* ``train-<task-string>`` will train and evaluate a single model;
* ``validate-<task-string>`` will cross-validate the model by using the selected cross-validation strategy;
* ``figures<task-string>`` will generate the result figures for that task. Please make sure to update the validation fold used in this stage as it is tied to the results obtained during validation;
* ``timeit-<task-string>`` will asses the inference time for the model used in that task;
* ``summary-<task-string>`` will generate summary statistics for the model used in that task;

Finally, the ``compile_results`` dvc stage is used to collect the results from multiple tasks and build summary tables and figures.

For a complete description, please refer to the ``dvc.yaml`` file.
