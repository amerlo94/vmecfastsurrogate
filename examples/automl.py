# pylint: disable=invalid-name
"""Showcase AutoML capabilities."""

import matplotlib.pyplot as plt
import seaborn as sns

import numpy as np
import pandas as pd
import sklearn.metrics
import sklearn.model_selection
import autosklearn.regression

from w7x.vmec import FourierCoefficients
from vmec_dnn_surrogate.lib.dataset import VmecDataset


def rmsdstd(ref, value):
    assert value.shape == ref.shape
    assert len(ref.shape) == 1
    rmse = np.sqrt(np.square(ref - value).mean())
    return rmse / ref.std()


data = "data/null/minerva-vmec-cee5aebc91af68a250b0fc4f2df9b761"
features = ["phiedge", "coilCurrents"]
output = "Space"
num_samples = 2048

#  Select Fourier coefficients
s = -1  # radial location
m = 0  # poloidal modes
n = 0  # toroidal modes
component = 0  # field component index

#  Read data
dataset = VmecDataset(data=data, num_samples=num_samples)
X, Y = dataset.input_fn(
    features=features,
    label=output,
    use_coil_currents_ratio=True,
    return_raw_runs=True,
    return_X_y=True,
)

#  Perform regression on single output coefficients
index = FourierCoefficients.get_indices(0, m, n, radial=99, modes=12)
y = Y[:, s, index, component].flatten()

X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(
    X, y, random_state=42
)

#  Build and fit
automl = autosklearn.regression.AutoSklearnRegressor(
    time_left_for_this_task=120,
    per_run_time_limit=30,
    resampling_strategy="cv",
    resampling_strategy_arguments={"folds": 5},
    tmp_folder="/tmp/vmec_automl_example_tmp",
    output_folder="/tmp/vmec_automl_example_out",
)
automl.fit(X_train, y_train, feat_type=["Numerical"] * X.shape[1])

print(automl.show_models())
print(automl.sprint_statistics())

#  Compute predictions and metrics
predictions = automl.predict(X_test)
print("R2 score: {:.3f}".format(sklearn.metrics.r2_score(y_test, predictions)))
print("rmsdstd : {:.3f}".format(rmsdstd(y_test, predictions)))

#  Save cross validation results
pd.DataFrame(automl.cv_results_).to_csv("reports/examples/automl_cv_results.csv")

#  Plot results
fig = sns.jointplot(
    x=predictions,
    y=y_test,
    height=8,
    space=0.4,
)
axes = fig.ax_joint
axes.plot(y_test, y_test, "--", linewidth=1.5, color="r")
plt.show()
