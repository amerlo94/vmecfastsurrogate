# pylint: disable=invalid-name
"""Short example on how to read back data from dataset.

Achtung: the range on how to filter the data are stored in lib/dataset.py,
please check that your data satisfy them, or update them in case.
"""

from vmec_dnn_surrogate.lib.dataset import VmecDataset
from vmec_dnn_surrogate.lib.utils import to_dataframe

data = "data/test/minerva-vmec-3ff1c3281ec4f28cd47efe1f612335b1"
features = ["coilCurrents", "phiedge"]

dataset = VmecDataset(data=data)
ds = dataset.input_fn(features=features, label="Space", return_raw_runs=True)

df = to_dataframe(ds.as_numpy_iterator())
print(df.head())
