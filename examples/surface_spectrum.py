# pylint: disable=invalid-name
"""Short example on the Fourier spectrum of simple surface."""

import matplotlib.pyplot as plt

from w7x.vmec import (
    FourierCoefficients,
    SurfaceCoefficients,
)

#  Standard surfaces
line = {
    "label": r"$Z = \beta_1$",
    "r_cos": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    "z_sin": [0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
}

circle = {
    "label": r"$R = \alpha_1$, $Z = \beta_1$",
    "r_cos": [0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
    "z_sin": [0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
}

surfaces = [
    {
        "label": r"$R = \alpha_1 + 0.5 \alpha_2$, $Z = \beta_1$",
        "r_cos": [0.0, 1.0, 0.5, 0.0, 0.0, 0.0],
        "z_sin": [0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
    },
    {
        "label": r"$R = \alpha_1 + 0.33 \alpha_2$, $Z = \beta_1 - 0.33 \beta_3$",
        "r_cos": [0.0, 1.0, 0.33, 0.0, 0.0, 0.0],
        "z_sin": [0.0, 1.0, -0.33, 0.0, 0.0, 0.0],
    },
    {
        "label": r"$R = - \alpha_1 + 0.1 \alpha_2$, $Z = 1.5 \beta_1 + 0.15 \beta_2$",
        "r_cos": [0.0, -1.0, 0.1, 0.0, 0.0, 0.0],
        "z_sin": [0.0, 1.5, 0.15, 0.0, 0.0, 0.0],
    },
    {
        "label": r"$R = \alpha_1 + 0.1 \alpha_2$, $Z = 0.8 \beta_1 - 0.1 \beta_2$",
        "r_cos": [0.0, 1.0, 0.1, 0.0, 0.0, 0.0],
        "z_sin": [0.0, 0.8, -0.1, 0.0, 0.0, 0.0],
    },
]

fig = plt.figure()

for i, s in enumerate(surfaces):

    axes = fig.add_subplot(2, 2, i + 1)

    r_cos = FourierCoefficients(
        coefficients=s["r_cos"],
        poloidalModeNumbers=[0, 1, 2, 3, 4, 5],
        toroidalModeNumbers=[0],
        numRadialPoints=1,
    )
    z_sin = FourierCoefficients(
        coefficients=s["z_sin"],
        poloidalModeNumbers=[0, 1, 2, 3, 4, 5],
        toroidalModeNumbers=[0],
        numRadialPoints=1,
    )

    surface = SurfaceCoefficients(RCos=r_cos, ZSin=z_sin)
    surface.plot(
        0,
        linestyle="--",
        axes=axes,
        label=s["label"],
    )

    #  Add standard figure on top
    r_cos = FourierCoefficients(
        coefficients=circle["r_cos"],
        poloidalModeNumbers=[0, 1, 2, 3, 4, 5],
        toroidalModeNumbers=[0],
        numRadialPoints=1,
    )
    z_sin = FourierCoefficients(
        coefficients=circle["z_sin"],
        poloidalModeNumbers=[0, 1, 2, 3, 4, 5],
        toroidalModeNumbers=[0],
        numRadialPoints=1,
    )

    surface = SurfaceCoefficients(RCos=r_cos, ZSin=z_sin)
    surface.plot(
        0,
        linestyle="--",
        axes=axes,
        label=circle["label"],
    )

    axes.set_xlabel("R (m)")
    axes.set_ylabel("Z (m)")
    axes.set_xlim([-2, 2])
    axes.set_ylim([-2, 2])
    axes.set_aspect("equal")

fig.tight_layout()
plt.show()
