# pylint: disable=invalid-name
"""Simple example to train a keras model on the VMEC dataset."""

import os

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import sklearn.metrics
import sklearn.model_selection
import tensorflow as tf
from sklearn.preprocessing import StandardScaler

from w7x.vmec import FourierCoefficients
from vmec_dnn_surrogate.lib.dataset import VmecDataset


def nrmse(ref, value):
    """Normalized root-mean-squared error."""

    assert value.shape == ref.shape
    rmse = np.sqrt(np.square(ref - value).mean(axis=0))
    return (rmse / ref.std(axis=0)).mean()


def tf_r2(y, y_pred):
    """Tensorflow implementation of the R2 score."""

    mse = tf.reduce_sum(tf.square(y - y_pred))
    std = tf.reduce_sum(tf.square(y - tf.reduce_mean(y, axis=0)))
    return 1.0 - tf.math.divide(mse, std)


#  Set random seeds
np.random.seed(42)
tf.random.set_seed(42)

data = "data/finite/minerva-vmec-a7f8402be122729492366725c95eee81"
features = [
    "phiedge",
    "curtor",
    "pressureScale",
    "toroidalCurrent",
    "pressure",
]

output = "Space"
num_samples = 2056

#  Select Fourier coefficients
s = -1  # radial location
m = 0  # poloidal modes
n = 0  # toroidal modes
component = 0  # field component index

#  Read data
dataset = VmecDataset(data=data, num_samples=num_samples)

#  Cache data set to file for faster access
tmp_file = "/tmp/vmec-data.npz"
if not os.path.isfile(tmp_file):
    X, Y = dataset.input_fn(
        features=features,
        label=output,
        use_coil_currents_ratio=True,
        return_raw_runs=True,
        return_X_y=True,
    )
    np.savez(tmp_file, X=X, Y=Y)
else:
    data = np.load(tmp_file)
    X = data["X"]
    Y = data["Y"]

#  Perform regression on single output coefficients
index = FourierCoefficients.get_indices(0, m, n, radial=99, modes=12)
y = Y[:, s, index, component]

X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(
    X, y, random_state=42
)

#  Scale X data
X_scaler = StandardScaler()
X_train = X_scaler.fit_transform(X=X_train)
X_test = X_scaler.transform(X=X_test)

#  Scale Y data
y_scaler = StandardScaler()
y_train = y_scaler.fit_transform(X=y_train).flatten()

# Select hp
units = 32
hidden_layers = 3
bt = False
dropout = 0
l2 = 0.01
batch_size = 32
lr = 3e-3
epochs = 1000


#  Define dense block
def add_dense_block(model, units, dp, l2, bt):
    """Add a dense block to the model."""

    model.add(
        tf.keras.layers.Dense(
            units,
            kernel_initializer=tf.compat.v1.glorot_uniform_initializer(),
            use_bias=not bt,
            kernel_regularizer=tf.keras.regularizers.L2(l2=l2),
        )
    )
    if bt:
        model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Activation("relu"))
    model.add(tf.keras.layers.Dropout(dp))


#  Build and fit
model = tf.keras.Sequential()
model.add(
    tf.keras.layers.Dense(
        units,
        use_bias=not bt,
        kernel_initializer=tf.compat.v1.glorot_uniform_initializer(),
        input_shape=X.shape[1:],
        kernel_regularizer=tf.keras.regularizers.L2(l2=l2),
    )
)
if bt:
    model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.Activation("relu"))
model.add(tf.keras.layers.Dropout(dropout))

#  Add hidden layers
for _ in range(hidden_layers):
    add_dense_block(model, units, dp=dropout, l2=l2, bt=bt)

model.add(
    tf.keras.layers.Dense(
        y.shape[1],
        kernel_initializer=tf.compat.v1.glorot_uniform_initializer(),
        bias_initializer=tf.keras.initializers.Zeros(),
        kernel_regularizer=tf.keras.regularizers.L2(l2=l2),
    )
)

model.compile(
    optimizer=tf.keras.optimizers.Adagrad(learning_rate=lr),
    loss="mse",
    metrics=[tf_r2],
)
model.summary()

#  Check for random weight initialization
model.evaluate(X_train, y_train)

#  Fit
history = model.fit(
    X_train,
    y_train,
    batch_size=batch_size,
    epochs=epochs,
    verbose=1,
    validation_split=0.1,
)

#  Check performance on training data
y_hat = model.predict(X_train)
losses = model.evaluate(X_train, y_train)
print("loss on train    : {:.3f}".format(losses[0]))
print("r2 on train      : {:.3f}".format(sklearn.metrics.r2_score(y_train, y_hat)))
print("nrmse on train   : {:.3f}".format(nrmse(y_train.reshape(-1, 1), y_hat)))

#  Compute predictions and scale back
predictions = model.predict(X_test)
predictions = y_scaler.inverse_transform(X=predictions.reshape(-1, 1))
print("r2 on test       : {:.3f}".format(sklearn.metrics.r2_score(y_test, predictions)))
print("nrmse on test    : {:.3f}".format(nrmse(y_test, predictions)))

#  Plot results
plt.plot(history.history["loss"])
plt.plot(history.history["tf_r2"])
plt.plot(history.history["val_loss"])
plt.xlabel("epochs")
plt.legend(["loss", "tf_r2", "val_loss"], loc="upper left")
plt.yscale("log")

#  Plot scatterplot
fig = sns.jointplot(
    x=predictions.flatten(),
    y=y_test.flatten(),
    height=8,
    space=0.4,
)
axes = fig.ax_joint
axes.plot(y_test, y_test, "--", linewidth=1.5, color="r")
plt.show()
