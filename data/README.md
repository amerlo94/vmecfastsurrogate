# Data folder

The raw data runs are not tracked via git, but via dvc. Please refer to the project
`README.rst` file on how to set-up dvc and pull these data.

## Structure

The data folder keeps the following subfolders:

* `null/`: null beta scenario runs;
* `finite/`: finite beta scenario runs;
* `hybrid/`: hybrid beta scenario runs;
* `test/`: test runs;
* `misc/`: deprecated runs. **Achtung**: these runs should not be used.

## Access data
Batches created via the w7x package could be retrieved with few lines of code.

```python
from w7x.batch import Vmec

batch_id = "w7x-vmec-0089f903a815c98c3ea1fe89b48acfab"
batch = w7x.batch.Vmec(batch_id, folder="data/")
```

Whereas the ones created via Minerva could be accessed in the following way.

```python
from w7x.batch import Vmec
from os import listdir
from os.path import join

batch_id = "minerva-vmec-31577b41dff430e00baf8da2bee464a2"
folder = "data"
runs = listdir(join(folder, batch_id))
batch = Vmec.from_runs(runs, batch_id, folder)
```

## VMEC Batches

Batch id with starts with the 'w7x-vmec-' string has been created with the w7x
python package, whrease the one that starts with 'minerva-vmec' has been
created with Minerva.

### minerva-vmec-a7f8402be122729492366725c95eee81
9675 samples, finite beta configuration and standard magnetic configuration.
This data set contains runs with pressure on axis up to 2e5 Pa, and a net
toroidal current with I_tor up to +- 10 kA. The toroidal current profile has been set
in order to mimic ECCD expected ones.

```java
// Uniform
phiedge.setLow(-2.5);
phiedge.setHigh(-1.6);
phiedge.setActive(true);

// Uniform
totalToroidalCurrent.setLow(-1e4);
totalToroidalCurrent.setHigh(1e4);
totalToroidalCurrent.setActive(true);

// Toroidal Current Profile
toroidalCurrentAtBoundary.setValue(1.0);
toroidalCurrentOnAxis.setValue(0.0);
toroidalCurrentGradientOnAxis.setValue(0.0);
toroidalCurrentGradientAtBoundary.setValue(0.0);

// Uniform
pressureScale.setLow(1e-6);
pressureScale.setHigh(2e5);
pressureScale.setActive(true);

// Pressure Profile
pressureAtBoundary.setValue(0.0);
pressureOnAxis.setValue(1.0);
pressureGradientOnAxis.setValue(0.0);

// Uniform
nonPlanar.setValue(13068);
nonPlanar.setActive(false);
planar.setValue(-700);
planer.setActive(false);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - bee09b694ee6ef77f77a1e92aff701d9f0528e7f
* [vmec_dnn_surrogate](https://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate/) - 4279e37f7c4e6c871287163e6904c3215dd1934d


### minerva-vmec-d46fa4458570d5dbceea264a4fdcaf31
9918 samples, finite beta configuration and standard magnetic configuration.
This data set contains runs with pressure on axis up to 3e5 Pa, but since the low
rate of convergence of VMEC runs, this upper limit for the pressure has been
lowered to 2e5 Pa.

```java
// Uniform
phiedge.setLow(-2.5);
phiedge.setHigh(-1.6);
phiedge.setActive(true);

// Uniform
totalToroidalCurrent.setLow(1e-3);
totalToroidalCurrent.setHigh(1e4);
totalToroidalCurrent.setActive(true);

// Toroidal Current Profile
toroidalCurrentAtBoundary.setValue(0.05);
toroidalCurrentOnAxis.setValue(0.0);
toroidalCurrentGradientOnAxis.setValue(0.0);
toroidalCurrentGradientAtBoundary.setValue(0.0);

// Uniform
pressureScale.setLow(1e-6);
pressureScale.setHigh(3e5);
pressureScale.setActive(true);

// Pressure Profile
pressureAtBoundary.setValue(0.0);
pressureOnAxis.setValue(1.0);
pressureGradientOnAxis.setValue(0.0);

// Uniform
nonPlanar.setValue(13068);
nonPlanar.setActive(false);
planar.setValue(-700);
planer.setActive(false);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - dc572d9f55077c234d7f28a1de44e2142044783b
* [vmec_dnn_surrogate](https://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate/) - c56f13a51c4d39a3831b0ad122bfef5f7565f8d5

### minerva-vmec-cee5aebc91af68a250b0fc4f2df9b761
10339 samples, null beta configuration with fixed I_1 coil currents. Around standard
configuration with I_1 = 13,77 kA and phiedge = -2.011. The other coil currents
are set given ratio boundaries.

```java
// Uniform
phiedge.setLow(-2.5);
phiedge.setHigh(-1.6);
phiedge.setActive(true);

// Loguniform
totalToroidalCurrentLog.setValue(-3);
totalToroidalCurrentLog.setActive(false);

// Loguniform
pressureScaleLog.setValue(-6);
pressureScaleLog.setActive(false);

// Uniform
nonPlanarRatio.setLow(0.6);
nonPlanarRatio.setHigh(1.3);
planar.setLow(-1.0);
planar.setHigh(1.0);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - bd0e7d4e24ee317ccc5ea426499ddef910fbadec
* [vmec_dnn_surrogate](https://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate/) - fc4a4860ad033ff50f0a988cf62735056f95cdec

### minerva-vmec-0443d7599c2ef1518b4879e49de62a3b
12077 samples, null beta configuration with ranges around reference W7X magnetic
configurations. Use the 'nullProfiles' switch in the Minerva project. This dataset
holds configurations for which the nonPlanar coil currents is up to 1.8 MA.

```java
// Uniform
phiedge.setLow(-2.5);
phiedge.setHigh(-1.6);
phiedge.setActive(true);

// Loguniform
totalToroidalCurrentLog.setValue(-3);
totalToroidalCurrentLog.setActive(false);

// Loguniform
pressureScaleLog.setValue(-6);
pressureScaleLog.setActive(false);

// Uniform
nonPlanar.setLow(9.0);
nonPlanar.setHigh(18.0);
planar.setLow(-10.0);
planar.setHigh(10.0);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - bd0e7d4e24ee317ccc5ea426499ddef910fbadec
* [vmec_dnn_surrogate](https://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate/) - fc6dada004814a803523fbdf369ac489c4040f6b

### minerva-vmec-817aa3ad665f6757164ac5a8ad1fcc01
9911 samples, finite beta configuration for standard magnetic configuration with
reference non-planar coil currents values.

```java
// Uniform
phiedge.setLow(-2.5);
phiedge.setHigh(-1.6);
phiedge.setActive(true);

// Loguniform
totalToroidalCurrentLog.setLow(-1.0);
totalToroidalCurrentLog.setHigh(4.7);
totalToroidalCurrentLog.setActive(true);

// Toroidal Current Profile
toroidalCurrentAtBoundary.setValue(0.05);
toroidalCurrentOnAxis.setValue(0.0);
toroidalCurrentGradientOnAxis.setValue(0.0);
toroidalCurrentGradientAtBoundary.setValue(0.0);

// Loguniform
pressureScaleLog.setLow(-3.0);
pressureScaleLog.setHigh(5.5);
pressureScaleLog.setActive(true);

// Pressure Profile
pressureAtBoundary.setValue(0.0);
pressureOnAxis.setValue(1.0);
pressureGradientOnAxis.setValue(0.0);

// Uniform
nonPlanar.setValue(14.5);
nonPlanar.setActive(false);
planar.setValue(0.0);
planer.setActive(false);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - 381f88a5f5a5e92a312641f14e51c4a07c93c2b6
* [vmec_dnn_surrogate](https://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate/) - 45597d814e903c507638fdbb1b3a2616a1ee6f88

### minerva-vmec-72e9fa090a4e2081256966be7896fc81
10895 samples, finite beta configuration for standard magnetic configuration.

```java
// Uniform
phiedge.setLow(-2.5);
phiedge.setHigh(-1.6);
phiedge.setActive(true);

// Loguniform
totalToroidalCurrentLog.setLow(-1.0);
totalToroidalCurrentLog.setHigh(4.7);
totalToroidalCurrentLog.setActive(true);

// Toroidal Current Profile
toroidalCurrentAtBoundary.setValue(0.05);
toroidalCurrentOnAxis.setValue(0.0);
toroidalCurrentGradientOnAxis.setValue(0.0);
toroidalCurrentGradientAtBoundary.setValue(0.0);

// Loguniform
pressureScaleLog.setLow(-3.0);
pressureScaleLog.setHigh(5.5);
pressureScaleLog.setActive(true);

// Pressure Profile
pressureAtBoundary.setValue(0.0);
pressureOnAxis.setValue(1.0);
pressureGradientOnAxis.setValue(0.0);

// Uniform
nonPlanar.setValue(15.0);
nonPlanar.setActive(false);
planar.setValue(0.0);
planer.setActive(false);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - 9a77f4d523bf842d9ec6702323dd26aa288cbada
* [vmec_dnn_surrogate](https://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate/) - 91299f8a82567e3a1fe7b183c9951b341fb853f8

### minerva-vmec-aaacd93186890e74f2dd5229143113a1
10074 samples, finite beta configuration for standard magnetic configuration.

```java
// Uniform
phiedge.setLow(-2.5);
phiedge.setHigh(-1.6);
phiedge.setActive(true);

// Loguniform
totalToroidalCurrentLog.setLow(-1.0);
totalToroidalCurrentLog.setHigh(4.7);
totalToroidalCurrentLog.setActive(true);

// Toroidal Current Profile
toroidalCurrentAtBoundary.setValue(0.05);
toroidalCurrentOnAxis.setValue(0.0);
toroidalCurrentGradientOnAxis.setValue(0.0);
toroidalCurrentGradientAtBoundary.setValue(0.0);

// Loguniform
pressureScaleLog.setLow(-3.0);
pressureScaleLog.setHigh(5.5);
pressureScaleLog.setActive(true);

// Pressure Profile
pressureAtBoundary.setValue(0.0);
pressureOnAxis.setValue(1.0);
pressureGradientOnAxis.setValue(0.0);

// Uniform
nonPlanar.setValue(15.0);
nonPlanar.setActive(false);
planar.setValue(0.0);
planer.setActive(false);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - a98b9e1fbff058808a5af3c0c649ca9715a4cf8b
* [vmec_dnn_surrogate](https://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate/) - f48085fe7a08fc701e7066118a2e794b0d627b45

### minerva-vmec-111306072bf7c9b9030d302a1e0a629f
15000 samples, null beta configuration.

```java
// Uniform
phiedge.setLow(-2.0);
phiedge.setHigh(-1.6);

// Loguniform
totalToroidalCurrentLog.setValue(-3.0);
totalToroidalCurrentLog.setActive(false);

// Loguniform
pressureScaleLog.setValue(-6.0);
pressureScaleLog.setActive(false);

// Uniform
nonPlanar.setLow(9.0);
nonPlanar.setHigh(16.0);
planar.setLow(-10.0);
planar.setHigh(10.0);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - d3136dc140def88033de1c774ae39e3910989ffb
* [research](https://gitlab.mpcdf.mpg.de/amerlo/research/) - 2991809ddc6d6a5782374eac2371e7ced332ff5d

### minerva-vmec-4ee8c3281ec4f28cd47efe1f612335b1
8656 samples, null beta configuration.

```java
// Uniform
phiedge.setLow(-2.0);
phiedge.setHigh(-1.6);

// Loguniform
totalToroidalCurrentLog.setValue(-3.0);
totalToroidalCurrentLog.setActive(false);

// Loguniform
pressureScaleLog.setValue(-6.0);
pressureScaleLog.setActive(false);

// Uniform
nonPlanar.setLow(9.0);
nonPlanar.setHigh(16.0);
planar.setLow(-10.0);
planar.setHigh(10.0);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - d3136dc140def88033de1c774ae39e3910989ffb
* [research](https://gitlab.mpcdf.mpg.de/amerlo/research/) - 4e29bde53859cd9790f5b17a6f25314ee9fac7c0

### minerva-vmec-eefa45ff76e8203cce8aa46e673af9d1
8790 samples, null beta configuration.

```java
// Uniform
phiedge.setLow(-2.0);
phiedge.setHigh(-1.6);

// Loguniform
totalToroidalCurrentLog.setValue(-3.0);
totalToroidalCurrentLog.setActive(false);

// Loguniform
pressureScaleLog.setValue(-6.0);
pressureScaleLog.setActive(false);

// Uniform
nonPlanar.setLow(9.0);
nonPlanar.setHigh(16.0);
planar.setLow(-10.0);
planar.setHigh(10.0);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - d3136dc140def88033de1c774ae39e3910989ffb
* [research](https://gitlab.mpcdf.mpg.de/amerlo/research/) - da7f7ea82d8bf822250453a57fcbaf80f1079372

### minerva-vmec-ffff84b578951555b10aa8e9d6717e92
8965 samples, hybrid beta configuration, toroidal current profile null on axis.

```java
// Uniform
phiedge.setLow(-2.0);
phiedge.setHigh(-1.6);

// Loguniform
totalToroidalCurrentLog.setLow(-1.0);
totalToroidalCurrentLog.setHigh(4.9);

// Toroidal Current Profile
toroidalCurrentAtBoundary.setValue(1.0);
toroidalCurrentOnAxis.setValue(1e-3);
toroidalCurrentGradientOnAxis.setValue(1e-3);

// Loguniform
pressureScaleLog.setLow(-3.0);
pressureScaleLog.setHigh(5.5);

// Uniform
nonPlanar.setLow(9.0);
nonPlanar.setHigh(16.0);
planar.setLow(-10.0);
planar.setHigh(10.0);

// GPHigdonSwallKernGaussianKernel1D
pressureLengthScale.setL1(1.0);
pressureLengthScale.setL1(1.0);
pressureGpKernel.setSigmaf(5.0);

// GPHigdonSwallKernGaussianKernel1D
toroidalCurrentLengthScale.setL1(1.0);
toroidalCurrentLengthScale.setL1(1.0);
toroidalCurrentGpKernel.setSigmaf(5.0);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - 23d83418b1f0dcd894db9dee1434f042dedc585b
* [research](https://gitlab.mpcdf.mpg.de/amerlo/research/) - a0f60b052bb2f57d7281a906898dda282f006723

### minerva-vmec-f75a7982f9ad947c9f16db3dd8c2e34m
9473 samples, hybrid beta configuration, smoother profiles given by lower Sigmaf.

```java
// Uniform
phiedge.setLow(-2.0);
phiedge.setHigh(-1.6);

// Loguniform
totalToroidalCurrentLog.setLow(-1.0);
totalToroidalCurrentLog.setHigh(4.9);

// Toroidal Current Profile
toroidalCurrentAtBoundary.setValue(1.0);
toroidalCurrentOnAxis.setValue(1e-3);
toroidalCurrentGradientOnAxis.setValue(1e-6);
toroidalCurrentGradientAtBoundary.setValue(1e-6);

// Loguniform
pressureScaleLog.setLow(-3.0);
pressureScaleLog.setHigh(5.5);

// Uniform
nonPlanar.setLow(9.0);
nonPlanar.setHigh(16.0);
planar.setLow(-10.0);
planar.setHigh(10.0);

// GPHigdonSwallKernGaussianKernel1D
pressureLengthScale.setL1(1.0);
pressureLengthScale.setL1(1.0);
pressureGpKernel.setSigmaf(3.0);

// GPHigdonSwallKernGaussianKernel1D
toroidalCurrentLengthScale.setL1(1.0);
toroidalCurrentLengthScale.setL1(1.0);
toroidalCurrentGpKernel.setSigmaf(3.0);
```

Repositories status:

* [minerva-w7x-vmec-ml](https://gitlab.minerva-central.net/minerva/minerva-w7x-vmec-ml) - e31464960b55237a239752dc9a8c3ef8caaab470
* [research](https://gitlab.mpcdf.mpg.de/amerlo/research/) - 443146a0aa1dccb1b6cc2d130480cf113e5a1cca

### w7x-vmec-18a2e931dc1ee5938ad7300bacad52f5
15000 samples, null beta configuration.

```python
SPACE = {
    "magnetic_config": (
        w7x.batch.Distribution(
            np.random.uniform,
            [0.6, 0.6, 0.6, 0.6, 0.6, -0.3, -0.3, 0.0, 0.0],
            [1.4, 1.4, 1.4, 1.4, 1.4, 0.3, 0.3, 0.0, 0.0],
        ),
        "rw",
    ),
    "phiedge": w7x.batch.Distribution(np.random.uniform, -2.0, -1.6),
    "curtor": 0,
    "current_profile": ([0, 0, 0, 0, 0], "sum_atan"),
    "pressure_profile": ([1e-6, -1e-6], "power_series"),
}
```

### w7x-vmec-bfe4cdf21d60ab1895a3b8583ccebd37
10000 samples, hybrid beta configuration.

```python
SPACE = {
    "magnetic_config": (
        w7x.batch.Distribution(
            np.random.uniform,
            [0.6, 0.6, 0.6, 0.6, 0.6, -0.3, -0.3, 0.0, 0.0],
            [1.4, 1.4, 1.4, 1.4, 1.4, 0.3, 0.3, 0.0, 0.0],
        ),
        "rw",
    ),
    "phiedge": w7x.batch.Distribution(np.random.uniform, -2.2, -1.6),
    "curtor": w7x.batch.Distribution(np.random.uniform, 0, 5e4),
    "current_profile": (
        w7x.batch.Distribution(np.random.uniform, [0.0, 0.0], [1.0, -1.0]),
        "power_series",
    ),
    "pressure_profile": (
        w7x.batch.Distribution(np.random.uniform, [0, 1], [1e5, 51]),
        "peaking",
    ),
}
```

### w7x-vmec-1f78b89b2a8e2ecda289c0ea07bc9960
10000 samples, null beta configuration, with slighlty high phiedge values.

```python
SPACE = {
    "magnetic_config": (
        w7x.batch.Distribution(
            np.random.uniform,
            [0.6, 0.6, 0.6, 0.6, 0.6, -0.3, -0.3, 0.0, 0.0],
            [1.4, 1.4, 1.4, 1.4, 1.4, 0.3, 0.3, 0.0, 0.0],
        ),
        "rw",
    ),
    "phiedge": w7x.batch.Distribution(np.random.uniform, -2.2, -1.6),
    "curtor": 0.0,
    "current_profile": ([0.0, 0.0], "power_series"),
    "pressure_profile": ([1e-6, -1e-6], "power_series"),
}
```

## Test VMEC Batches

### minerva-vmec-3ff1c3281ec4f28cd47efe1f612335b1
1419 samples, subset of minerva-vmec-4ee8c3281ec4f28cd47efe1f612335b1.

### minerva-vmec-31577b41dff430e00baf8da2bee464a2
Collection of test Minerva runs. Use only for testing.

### w7x-vmec-c74a0b7944be0aa0454a78d5ef92b15f
100 samples, with large values for i5, i6.

### w7x-vmec-0089f903a815c98c3ea1fe89b48acfab
1000 samples, with large values for i5, i6.
