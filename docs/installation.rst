.. highlight:: shell

============
Installation
============


Stable release
--------------

To install vmec_dnn_surrogate, run this command in your terminal:

.. code-block:: console

    $ pip install vmec_dnn_surrogate

This is the preferred method to install vmec_dnn_surrogate, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for vmec_dnn_surrogate can be downloaded from the `remote repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _remote repo: https://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate
.. _tarball: https://gitlab.mpcdf.mpg.de/amerlo/vmec_dnn_surrogate/tarball/master
