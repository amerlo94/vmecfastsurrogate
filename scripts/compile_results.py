"""Script to compile tabular results and figures."""

import json
import argparse
import logging
import itertools
from os import system
from os.path import join, isfile, basename
from shutil import copyfile
import random

import numpy as np
import pandas as pd

from vmec_dnn_surrogate.lib.dataset import get_flux_points
from vmec_dnn_surrogate.lib.utils import (
    commit_and_save,
    bootstrap_mean_and_ci,
    to_values,
)

pd.set_option("display.max_colwidth", None)

random.seed(42)
np.random.seed(42)

DATA = "data"
REPORTS = "reports"
FIGURES = "figures"
NULL = "minerva-vmec-cee5aebc91af68a250b0fc4f2df9b761"
FINITE = "minerva-vmec-a7f8402be122729492366725c95eee81"


def get_std(filepath, label):
    """Extract the average variance for the given label."""

    if isinstance(filepath, list):
        return np.average([get_std(file_, label) for file_ in filepath])

    if not isfile(filepath):
        return None

    data = np.squeeze(to_values(pd.read_pickle(filepath), label))
    return data.mean()


TASKS = {
    "null-iota-r20": {
        "task_name": r"\nullIota",
        "best_search_estimator_index": 28,
        "nrmse_best_esimator": 0.0140332011505961,
        "std": get_std(join(FIGURES, NULL, "iota-std.pkl"), "iota"),
    },
    "finite-iota-r20": {
        "task_name": r"\finiteIota",
        "best_search_estimator_index": 30,
        "nrmse_best_esimator": 0.045224241912365,
        "std": get_std(join(FIGURES, FINITE, "iota-std.pkl"), "iota"),
    },
    "null-space-r10p6t4": {
        "task_name": r"\nullSurfaces",
        "best_search_estimator_idex": 30,
        "nrmse_best_esimator": 0.14399,
        "std": get_std(
            join(FIGURES, NULL, "Space-std-map-real.pkl"), ["RCos", "LambdaSin", "ZSin"]
        ),
    },
    "finite-space-r10p6t4": {
        "task_name": r"\finiteSurfaces",
        "best_search_estimator_index": 21,
        "nrmse_best_esimator": 0.19501,
        "std": get_std(
            join(FIGURES, FINITE, "Space-std-map-real.pkl"),
            ["RCos", "LambdaSin", "ZSin"],
        ),
    },
    "null-bcos-r10p6t12": {
        "task_name": r"\nullB",
        "best_search_estimator_index": 28,
        "nrmse_best_esimator": 0.035157,
        "std": get_std(join(FIGURES, NULL, "BCos-std-map-real.pkl"), "BCos"),
    },
    "finite-bcos-r10p6t12": {
        "task_name": r"\finiteB",
        "best_search_estimator_index": 18,
        "nrmse_best_esimator": 0.10215,
        "std": get_std(join(FIGURES, FINITE, "BCos-std-map-real.pkl"), "BCos"),
    },
}
CMDS = ["dvc repro -sf timeit-" + task for task in TASKS]
RESULTS = [
    (join(REPORTS, "examples", "all_results.tex"), join(DATA, "all_results.tex")),
    (join(REPORTS, "examples", "rmse_results.csv"), join(DATA, "rmse_results.csv")),
    (join(REPORTS, "examples", "iota_rmse.tex"), join(DATA, "iota_rmse.tex")),
    (join(REPORTS, "examples", "space_rmse.tex"), join(DATA, "space_rmse.tex")),
    (join(REPORTS, "examples", "bcos_rmse.tex"), join(DATA, "bcos_rmse.tex")),
    (
        join(REPORTS, "examples", "iota-resolution-scan-rmse-2048.csv"),
        join(DATA, "iota-resolution-scan-rmse-2048.csv"),
    ),
    (
        join(REPORTS, "examples", "space-resolution-scan-rmse-64.csv"),
        join(DATA, "space-resolution-scan-rmse-64.csv"),
    ),
    (
        join(REPORTS, "examples", "bcos-resolution-scan-rmse-64.csv"),
        join(DATA, "bcos-resolution-scan-rmse-64.csv"),
    ),
    (
        join(REPORTS, "examples", "null-iota-r20-profile-nrmse.csv"),
        join(DATA, "iota-null-profile-nrmse.csv"),
    ),
    (
        join(REPORTS, "examples", "finite-iota-r20-profile-nrmse.csv"),
        join(DATA, "iota-finite-profile-nrmse.csv"),
    ),
]

RESULTS = RESULTS + [
    (
        join(REPORTS, f"{task}", f"hp-{task}.tex"),
        join(DATA, f"hp-{task}.tex"),
    )
    for task in TASKS
]

RESULTS = RESULTS + [
    (
        join(REPORTS, "examples", f"{task}-profile-rmse.csv"),
        join(DATA, f"{figure}-profile-rmse.csv"),
    )
    for task, figure in zip(
        TASKS,
        [
            "iota-null",
            "iota-finite",
            "space-null",
            "space-finite",
            "bcos-null",
            "bcos-finite",
        ],
    )
]

RESULTS = RESULTS + [
    (
        join(REPORTS, "examples", f"{task}-profile-sample.csv"),
        join(DATA, f"{figure}-profile-sample.csv"),
    )
    for task, figure in zip(
        TASKS,
        [
            "iota-null",
            "iota-finite",
            "space-null",
            "space-finite",
            "bcos-null",
            "bcos-finite",
        ],
    )
]

RESULTS = RESULTS + [
    (
        join(
            FIGURES,
            "minerva-vmec-a7f8402be122729492366725c95eee81",
            f"{figure}.pdf",
        ),
        join(FIGURES, f"{figure}.pdf"),
    )
    for figure in ["pressureScaled", "toroidalCurrent", "Space-map-fourier"]
]

IOTA_FIGURES_TYPES = list(itertools.product(*[["null", "finite"], ["rmsdstd", "rmse"]]))
RESULTS = RESULTS + [
    (
        join(FIGURES, f"{data}-iota-r20", f"iota-profile-{metric}.pdf"),
        join(FIGURES, f"{data}-iota-profile-{metric}.pdf"),
    )
    for data, metric in IOTA_FIGURES_TYPES
]

SPACE_FIGURES_TYPES = list(
    itertools.product(
        *[
            ["null", "finite"],
            ["map-real-rmse-lcfs", "profile-real-rmse", "map-fourier-nrmse-lcfs"],
        ]
    )
)
RESULTS = RESULTS + [
    (
        join(FIGURES, f"{data}-space-r10p6t4", f"Space-{fig}.pdf"),
        join(FIGURES, f"{data}-surfaces-{fig}.pdf"),
    )
    for data, fig in SPACE_FIGURES_TYPES
]

SURFACE_FIGURES_TYPES = list(
    itertools.product(*[["null", "finite"], ["0", "36"], ["worst", "median", "best"]])
)
RESULTS = RESULTS + [
    (
        join(FIGURES, f"{data}-space-r10p6t4", f"Space-surface-{angle}-{kind}.pdf"),
        join(FIGURES, f"{data}-surfaces-{angle}-{kind}.pdf"),
    )
    for data, angle, kind in SURFACE_FIGURES_TYPES
]

BCOS_FIGURES_TYPES = list(
    itertools.product(
        *[
            ["null", "finite"],
            ["profile-real-rmse", "map-real-rmse-lcfs", "map-fourier-nrmse-lcfs"],
        ]
    )
)
RESULTS = RESULTS + [
    (
        join(FIGURES, f"{data}-bcos-r10p6t12", f"bcos-{fig}.pdf"),
        join(FIGURES, f"{data}-bcos-{fig}.pdf"),
    )
    for data, fig in BCOS_FIGURES_TYPES
]

#  A `quantity` is a function which takes as input a vector value with shape
#  (#num_samples, #radial_locations, ...), and gives a 1d vector on which to
#  compute bootstrapped estimators.
IOTA_QUANTITIES = {
    "axis": lambda x: x[:, 0],
    "midr": lambda x: x[:, 12],
    "edge": lambda x: x[:, -1],
}
IOTA_AVERAGE = {"iota": lambda x: x.mean(axis=-1)}
SURFACE_QUANTITIES = {
    "r_axis": lambda x: x[:, 0, :, :, 0].reshape(len(x), -1).mean(axis=-1),
    "r_midr": lambda x: x[:, 6, :, :, 0].reshape(len(x), -1).mean(axis=-1),
    "r_edge": lambda x: x[:, -1, :, :, 0].reshape(len(x), -1).mean(axis=-1),
    "l_axis": lambda x: x[:, 1, :, :, 1].reshape(len(x), -1).mean(axis=-1),
    "l_midr": lambda x: x[:, 6, :, :, 1].reshape(len(x), -1).mean(axis=-1),
    "l_edge": lambda x: x[:, -1, :, :, 1].reshape(len(x), -1).mean(axis=-1),
    "z_axis": lambda x: x[:, 0, :, :, 2].reshape(len(x), -1).mean(axis=-1),
    "z_midr": lambda x: x[:, 6, :, :, 2].reshape(len(x), -1).mean(axis=-1),
    "z_edge": lambda x: x[:, -1, :, :, 2].reshape(len(x), -1).mean(axis=-1),
}
SURFACE_AVERAGE = {
    "r": lambda x: x[:, :, :, :, 0].reshape(len(x), -1).mean(axis=-1),
    "lambda": lambda x: x[:, :, :, :, 1].reshape(len(x), -1).mean(axis=-1),
    "z": lambda x: x[:, :, :, :, 2].reshape(len(x), -1).mean(axis=-1),
}
B_QUANTITIES = {
    "b_axis": lambda x: x[:, 1, :, :].reshape(len(x), -1).mean(axis=-1),
    "b_midr": lambda x: x[:, 6, :, :].reshape(len(x), -1).mean(axis=-1),
    "b_edge": lambda x: x[:, -1, :, :].reshape(len(x), -1).mean(axis=-1),
}
B_AVERAGE = {"b": lambda x: x.reshape(len(x), -1).mean(axis=-1)}


def get_task_labels(task_string):
    """Get task output labels given the task string."""

    if "iota" in task_string:
        return "iota"

    if "space" in task_string:
        return ["RCos", "LambdaSin", "ZSin"]

    if "bcos" in task_string:
        return ["BCos"]

    raise ValueError("Unknown task {}".format(task_string))


def si_format(value, ci=None):  # pylint: disable=invalid-name
    """Format value to be used by the siunitx package."""

    def _format(arg):
        if isinstance(arg, float):
            return "{:.6f}".format(arg)
        return str(arg)

    if not ci:
        return r"\num[round-mode=places,round-precision=1]{" + _format(value) + "}"

    #  Hack
    ci = "{:#.2g}".format(ci)
    if "." in ci:
        value = "{value:.{precision}f}".format(
            value=value, precision=len(ci.split(".")[1])
        )
    else:
        value = "{:.0f}".format(value)

    return r"\num{" + value + r" \pm " + ci + "}"


def get_profile_quantities(filepath, quantities):
    """Extract given quantities from profile data."""

    if not isfile(filepath):
        return None

    rmse = np.squeeze(to_values(pd.read_pickle(filepath)))
    return {
        loc: dict(
            zip(
                ["mean", "ci"],
                bootstrap_mean_and_ci(q(rmse), ci=0.95, n_boot=10000),
            )
        )
        for loc, q in quantities.items()
    }


def get_mean_and_ci(data, column):
    """Extract mean and ci for the given column.

    Args:
        data (pd.DataFrame): A pandas DataFrame.
        column (string): The column from where to get values.

    Returns:
        A dictionary with the mean and ci values.
    """

    return dict(
        zip(
            ["mean", "ci"],
            bootstrap_mean_and_ci(data[column].values, ci=0.95, n_boot=10000),
        )
    )


def get_profile_data(filepath, coordinate=0):
    """Extract profile mean and ci from given file."""

    def _build_df(data, coordinate):

        mean_ = []
        ci_ = []
        for loc in range(data.shape[1]):
            mean, ci = bootstrap_mean_and_ci(  # pylint: disable=invalid-name
                data[:, loc].reshape(data.shape[0], -1).mean(axis=1),
                ci=0.95,
                n_boot=10000,
            )
            mean_.append(mean)
            ci_.append(ci)

        return pd.DataFrame(
            {
                "y": mean_,
                "radial": range(data.shape[1]),
                "x": get_flux_points(data.shape[1]),
                "ci": ci_,
                "coordinate": coordinate,
            }
        )

    if not isfile(filepath):
        return None

    data = np.squeeze(to_values(pd.read_pickle(filepath)))

    #  Concatenate surface coordinates
    if len(data.shape) == 5:
        return pd.concat(
            [_build_df(data[..., i], coordinate=i) for i in range(data.shape[4])],
            sort=False,
            ignore_index=True,
        )

    return _build_df(data, coordinate)


def get_sample_data(filepath, coef=None, scale=False):
    """
    Extract sample data from filepath.

    Columns in file are expected to be true, pred, true, pred, ... .

    Args:
        filepath (str): The files from where to read the data.
        coef (list): List of data index to read back. They are usullay Fourier mode indices.
        scale (bool): Whether to scale the data.

    Returns:
        A pandas Dataframe.
    """

    if not isinstance(filepath, list):
        filepath = [filepath]

    #  Read back data
    data = {}
    for f in filepath:
        name = basename(f).split(".")[0]
        data[name] = np.squeeze(to_values(pd.read_pickle(f)))

    #  Create proto Dataframe
    elem = list(data.values())[0]
    df = pd.DataFrame(
        {
            "s": get_flux_points(elem.shape[0]),
            "x": range(elem.shape[0]),
        }
    )

    #  Populate Dataframe
    for k, d in data.items():
        if coef is not None:
            for i, kind in enumerate(["true", "pred"]):
                for c in coef:
                    df[k + "-" + str(c) + "-" + kind] = (
                        data[k][:, c, i] / data[k][-1, c, 0]
                        if scale
                        else data[k][:, c, i]
                    )
        else:
            for i, kind in enumerate(["true", "pred"]):
                df[k + "-" + kind] = data[k][:, i]

    return df


def get_task_data(task_string, data):
    """Retrieve task data from disk and update data dict."""

    #  Get inference time from timeit.json
    timeit_filepath = join(REPORTS, task_string, "timeit.json")
    with open(timeit_filepath, "r") as file_handle:
        timeit = file_handle.read()
    data["timeit"] = json.loads(timeit)

    #  Get validation data from validation.csv
    validation_filepath = join(REPORTS, task_string, "validation.csv")
    validation_df = pd.read_csv(validation_filepath)

    data["best_validation_estimator_index"] = validation_df["real_rmsdstd"].idxmin()
    data["worst_validation_estimator_index"] = validation_df["real_rmsdstd"].idxmax()

    data["nrmse"] = get_mean_and_ci(validation_df, "real_rmsdstd")
    data["rmse"] = get_mean_and_ci(validation_df, "real_rmse")
    data["t_train"] = get_mean_and_ci(validation_df, "time")
    data["params"] = validation_df["params"].values[0]

    #  Get iota results
    if "iota" in task_string:
        filepath = join(FIGURES, task_string, "iota-profile-rmse.pkl")
        data["rmse_csv"] = get_profile_quantities(filepath, IOTA_AVERAGE)
        data["rmse_tex"] = get_profile_quantities(filepath, IOTA_QUANTITIES)
        data["rmse_data"] = get_profile_data(filepath)

        filepath = join(FIGURES, task_string, "iota-profile-rmsdstd.pkl")
        data["nrmse_data"] = get_profile_data(filepath)

        filepath = [
            join(FIGURES, task_string, f"iota-profile-real-{kind}.pkl")
            for kind in ["worst", "median", "best"]
        ]
        filepath += [
            join(FIGURES, task_string, f"shear-profile-real-{kind}.pkl")
            for kind in ["worst", "median", "best"]
        ]
        data["sample_data"] = get_sample_data(filepath)

    #  Get space results
    elif "space" in task_string:
        filepath = join(FIGURES, task_string, "Space-profile-real-rmse.pkl")
        data["rmse_csv"] = get_profile_quantities(filepath, SURFACE_AVERAGE)
        data["rmse_tex"] = get_profile_quantities(filepath, SURFACE_QUANTITIES)
        data["rmse_data"] = get_profile_data(filepath)

    #  Get bcos results
    elif "bcos" in task_string:
        filepath = join(FIGURES, task_string, "bcos-profile-real-rmse.pkl")
        data["rmse_csv"] = get_profile_quantities(filepath, B_AVERAGE)
        data["rmse_tex"] = get_profile_quantities(filepath, B_QUANTITIES)
        data["rmse_data"] = get_profile_data(filepath)

        filepath = [
            join(FIGURES, task_string, f"bcos-profile-fourier-{kind}.pkl")
            for kind in ["worst", "median", "best"]
        ]
        data["sample_data"] = get_sample_data(
            filepath, coef=[0, 1, 25, 26], scale=False
        )


def get_all_results_table_entry(data):
    """Given results from one task, return tuple to build the results table."""

    predict_on_batch = data["timeit"]["predict_on_batch"]
    t_inference = si_format(
        predict_on_batch["mean"] * 1e3, predict_on_batch["ci"] * 1e3
    )

    nrmse_best = si_format(data["nrmse_best_esimator"] * 1e2)

    nrmse = si_format(data["nrmse"]["mean"] * 1e2, data["nrmse"]["ci"] * 1e2)

    t_train = si_format(data["t_train"]["mean"] * 1e-2, data["t_train"]["ci"] * 1e-2)

    return (data["task_name"], nrmse, t_train, t_inference, data["params"], nrmse_best)


def get_profile_table_entry(data, key):
    """Given single task, build profile table."""

    return tuple(
        [data["task_name"]]
        + [si_format(v["mean"] * 1e3, v["ci"] * 1e3) for v in data[key].values()]
    )


def get_rmse_table_entry(data):
    """Given single task output, take rmse mean and ci."""

    return tuple([data["mean"], data["ci"]])


def main():  # pylint: disable=too-many-locals, too-many-branches
    """Main function."""

    parser = argparse.ArgumentParser(description="VMEC training experiment")
    parser.add_argument(
        "--target_folder",
        help="File path for the root paper folder",
        type=str,
    )
    parser.add_argument("--force", default=False, action="store_true")
    parser.add_argument(
        "--summary",
        default=False,
        action="store_true",
        help="Print summary statistics to stdout",
    )
    args = parser.parse_args()

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    if args.force:
        for cmd in CMDS:
            system(cmd)

    #  Read results from disk
    for task, data in TASKS.items():
        get_task_data(task_string=task, data=data)

        #  Generate profile figures data
        if "iota" in task and data["nrmse_data"] is not None:
            data["nrmse_data"].to_csv(
                join(REPORTS, "examples", f"{task}-profile-nrmse.csv"),
                index=False,
            )

        if data["rmse_data"] is not None:
            data["rmse_data"].to_csv(
                join(REPORTS, "examples", f"{task}-profile-rmse.csv"),
                index=False,
            )

        #  Save sample data
        if "sample_data" in data:
            data["sample_data"].to_csv(
                join(REPORTS, "examples", f"{task}-profile-sample.csv"), index=False
            )

    #  Generate results table
    all_results = pd.DataFrame.from_records(
        [get_all_results_table_entry(task) for task in TASKS.values()],
        columns=[
            "Task",
            r"\gls{nrmse} [\num{e-2}]",
            r"\tTrain [\SI{e2}{\second}]",
            r"\tPrediction [\SI{e-3}{\second}]",
            r"$\text{\gls{NN}}_{\text{free parameters}}$",
            r"$\text{\gls{nrmse}}_{\text{best}} [\num{e-2}]$",
        ],
    )
    all_results_tex = all_results.to_latex(
        index=False,
        escape=False,
        column_format="".join(["l"] + ["c"] * (len(all_results.columns) - 1)),
    )
    commit_and_save(all_results_tex, join(REPORTS, "examples", "all_results.tex"))

    #  Generate rmse csv results
    rmse_results_entries = []
    for task in TASKS.values():
        rmse_results_entries.extend(
            [
                tuple(["{}-{}".format(task["task_name"], k)]) + get_rmse_table_entry(v)
                for k, v in task["rmse_csv"].items()
            ]
        )
    rmse_results = pd.DataFrame.from_records(
        data=rmse_results_entries,
        columns=[
            "task-output",
            "y",
            "ci",
        ],
    )
    rmse_results.to_csv(join(REPORTS, "examples", "rmse_results.csv"), index_label="x")

    #  Generate iota table
    iota_rmse = pd.DataFrame.from_records(
        [
            get_profile_table_entry(d, key="rmse_tex")
            for k, d in TASKS.items()
            if "iota" in k
        ],
        columns=[
            "Task",
            r"$\text{rmse}_{\gls{ibar}_a} [\num{e-3}]$",
            r"$\text{rmse}_{\gls{ibar}_m} [\num{e-3}]$",
            r"$\text{rmse}_{\gls{ibar}_b} [\num{e-3}]$",
        ],
    )
    iota_rmse_tex = iota_rmse.to_latex(
        index=False,
        escape=False,
        column_format="".join(["l"] + ["c"] * (len(iota_rmse.columns) - 1)),
    )
    commit_and_save(iota_rmse_tex, join(REPORTS, "examples", "iota_rmse.tex"))

    #  Generate surfaces table
    space_rmse = pd.DataFrame.from_records(
        [
            get_profile_table_entry(d, key="rmse_tex")
            for k, d in TASKS.items()
            if "space" in k
        ],
        columns=[
            "Quantity",  # This is an hack to get the correct label for df.T
            r"$\text{rmse}_{R_a} [\SI{e-3}{\meter}]$",
            r"$\text{rmse}_{R_m} [\SI{e-3}{\meter}]$",
            r"$\text{rmse}_{R_b} [\SI{e-3}{\meter}]$",
            r"$\text{rmse}_{\lambda_a}^{*} [\SI{e-3}{\radian}]$",
            r"$\text{rmse}_{\lambda_m} [\SI{e-3}{\radian}]$",
            r"$\text{rmse}_{\lambda_b} [\SI{e-3}{\radian}]$",
            r"$\text{rmse}_{Z_a} [\SI{e-3}{\meter}]$",
            r"$\text{rmse}_{Z_m} [\SI{e-3}{\meter}]$",
            r"$\text{rmse}_{Z_b} [\SI{e-3}{\meter}]$",
        ],
        index="Quantity",
    )
    space_rmse_tex = space_rmse.T.to_latex(
        escape=False,
        column_format="".join(["l"] + ["c"] * (len(space_rmse.columns) - 1)),
    )
    commit_and_save(space_rmse_tex, join(REPORTS, "examples", "space_rmse.tex"))

    #  Generate bcos table
    bcos_rmse = pd.DataFrame.from_records(
        [
            get_profile_table_entry(d, key="rmse_tex")
            for k, d in TASKS.items()
            if "bcos" in k
        ],
        columns=[
            "Task",
            r"$\text{rmse}_{B_a} [\SI{e-3}{\tesla}]$",
            r"$\text{rmse}_{B_m} [\SI{e-3}{\tesla}]$",
            r"$\text{rmse}_{B_b} [\SI{e-3}{\tesla}]$",
        ],
    )
    bcos_rmse_tex = bcos_rmse.to_latex(
        index=False,
        escape=False,
        column_format="".join(["l"] + ["c"] * (len(iota_rmse.columns) - 1)),
    )
    commit_and_save(bcos_rmse_tex, join(REPORTS, "examples", "bcos_rmse.tex"))

    #  Move all results
    if args.target_folder:
        for src, dst in RESULTS:
            dst_path = join(args.target_folder, dst)
            if isfile(src):
                logging.info("Copying %s into %s", src, dst_path)
                copyfile(src, dst_path)
            else:
                logging.warning("File %s has not been found", src)

    #  Log summary
    if args.summary:
        for task, data in TASKS.items():
            logging.info("Task %s", task)
            for key, value in data.items():
                logging.info("{:30s}: {:10s}".format(key, str(value)))


if __name__ == "__main__":
    main()
