# pylint: disable=bad-continuation
"""Short example to investigate the resolution downsampling on the Fourier coefficients.

Investigate rmse as a function of the number of fourier modes and real space grid.
"""

from os.path import join
import argparse

import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import seaborn as sns
import tikzplotlib

from rna.plotting import set_style
from vmec_dnn_surrogate.lib.metrics import get_metric
from vmec_dnn_surrogate.lib.dataset import (
    VmecDataset,
    to_downsampled_shape,
    get_label_radial_indices,
)
from vmec_dnn_surrogate.lib.utils import (
    to_dataframe,
    from_fourier_to_real,
    get_label_component,
    to_values,
)
from vmec_dnn_surrogate.lib.config import (
    FIELD_COMPONENTS,
    NUM_FIELD_PERIODS,
    NYQUIST_RESOLUTION_LABELS,
    NUM_OF_FOURIER_COEFFS,
    NUM_OF_NYQUIST_FOURIER_COEFFS,
    NUM_OF_RADIAL_POINTS,
)

sns.set()
sns.set_style("ticks")
set_style(style="rna")

NULL_ON_AXIS_LABELS = [
    "BCos",
    "BsupUCos",
    "BsupVCos",
    "BsubUCos",
    "BsubVCos",
    "LambdaSin",
]


def _get_resolution_parameters(output):
    """Build resolution parameters to be investigated.

    TODO(@amerlo): force nyquist mode as a script arg.

    Args:
        output (str): The string key representing the target output.

    Returns:
        A tuple for the resolution parameters to use.
    """

    num_fourier_coeffs = NUM_OF_FOURIER_COEFFS
    fourier_modes = [(4, 4), (6, 4), (6, 6), (8, 8), (10, 10), (12, 12)]
    fourier_modes = [(6, 4), (12, 12)]
    if output in NYQUIST_RESOLUTION_LABELS:
        num_fourier_coeffs = NUM_OF_NYQUIST_FOURIER_COEFFS
        fourier_modes = [(6, 4), (6, 12), (8, 10), (8, 12), (9, 12), (12, 12), (19, 18)]

    radial_points = [10, 20]
    grid_points = [(18, 9)]

    #  If investigating real space labels, treat them as a single Fourier coefficient ones.
    if output in ["iota"]:
        num_fourier_coeffs = 1
        labels = [output]
        radial_points = np.linspace(5, 90, 18, dtype=int)
        fourier_modes = [(1, 0)]
        grid_points = [(1, 1)]
    else:
        labels = FIELD_COMPONENTS[output]

    return (
        labels,
        num_fourier_coeffs,
        radial_points,
        fourier_modes,
        grid_points,
    )


def _plot_profile(full, interp, scaled, s, label):
    """Plot data along radial dimension.

    Use only for online visualization.
    """

    plt.plot(
        np.linspace(0, 1, NUM_OF_RADIAL_POINTS),
        full,
        "o",
        label="full",
    )
    plt.plot(
        np.linspace(0, 1, NUM_OF_RADIAL_POINTS),
        interp,
        "o",
        label="interp",
    )
    plt.plot(
        s,
        scaled,
        "o",
        label="scaled",
    )
    plt.legend()
    plt.title(label)
    plt.show()


def main():
    """Instantiate dataset and investigate resolution parameters."""

    parser = argparse.ArgumentParser(description="VMEC resolution investigation script")
    parser.add_argument(
        "--data",
        default=None,
        help="Folders where to read data",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "--output",
        default="Space",
        help="Output label to investigate",
        type=str,
    )
    parser.add_argument(
        "--num_samples",
        default=64,
        help="Number of samples to use",
        type=int,
    )
    parser.add_argument(
        "--metric",
        default="rmse",
        help="Metric to use",
        type=str,
    )
    parser.add_argument(
        "--interpolate",
        default=False,
        help="Compute distance across the full radial resolution",
        type=bool,
    )

    plot_profile = False

    #  Parse target output and get read resolution parameters to be used
    args = parser.parse_args()

    (
        labels,
        num_fourier_coeffs,
        radial_points,
        fourier_modes,
        grid_points,
    ) = _get_resolution_parameters(output=args.output)

    dataset = VmecDataset(data=args.data, num_samples=args.num_samples)
    ds = dataset.input_fn(features=[], label=args.output, return_raw_runs=True)
    dataframe = to_dataframe(ds.as_numpy_iterator())

    print(
        "Investigating resolution parameters for the {} output on {} samples...".format(
            args.output, len(dataframe)
        )
    )

    evaluations = []
    for radial, modes, grid in [
        (r, m, g) for r in radial_points for m in fourier_modes for g in grid_points
    ]:

        print(
            "Evaluating {} Fourier modes and {} radial points on {} grid points.".format(
                modes, radial, grid
            )
        )

        df = dataframe.copy()

        #  Add downsampled label in dataframe
        for label in labels:

            #  Just select radial indices for label with a single fourier coefficient.
            if num_fourier_coeffs == 1:
                label_str = "{}_r{}".format(label, radial)
                df[label_str] = df[label].apply(
                    lambda x: x[get_label_radial_indices(radial)].reshape(radial, 1),
                    1,
                )
                df[label] = df[label].apply(
                    lambda x: x.reshape(NUM_OF_RADIAL_POINTS, 1), 1
                )
            else:
                label_str = "{}_r{}p{}t{}".format(label, radial, modes[0], modes[1])
                df[label_str] = df[label].apply(
                    lambda x: to_downsampled_shape(x, radial, modes),
                    1,
                )
                df[label] = df[label].apply(
                    lambda x: to_downsampled_shape(x, NUM_OF_RADIAL_POINTS, modes=None),
                    1,
                )

            #  Extract values
            full = to_values(df, label)
            scaled = to_values(df, label_str)

            assert full.shape == (len(df), NUM_OF_RADIAL_POINTS, num_fourier_coeffs)
            assert scaled.shape == (
                len(df),
                radial,
                modes[0] * modes[1] * 2 + modes[0] - modes[1],
            )

            if num_fourier_coeffs == 1:
                #  Reshape data to a common representation.
                full = full.reshape(len(df), NUM_OF_RADIAL_POINTS, grid[0], grid[1])
                scaled = scaled.reshape(len(df), radial, grid[0], grid[1])
            else:
                full = from_fourier_to_real(
                    full,
                    poloidal_angles=np.linspace(0, 2 * np.pi, grid[0]),
                    toroidal_angles=np.linspace(
                        0, 2 * np.pi / NUM_FIELD_PERIODS, grid[1]
                    ),
                    component=get_label_component(label),
                )
                scaled = from_fourier_to_real(
                    scaled,
                    poloidal_angles=np.linspace(0, 2 * np.pi, grid[0]),
                    toroidal_angles=np.linspace(
                        0, 2 * np.pi / NUM_FIELD_PERIODS, grid[1]
                    ),
                    component=get_label_component(label),
                    modes=modes,
                )

            assert full.shape == (len(df), NUM_OF_RADIAL_POINTS, grid[0], grid[1])
            assert scaled.shape == (len(df), radial, grid[0], grid[1])

            if args.interpolate:
                #  Interpolate along radial dimension
                scaled_interp = np.zeros(full.shape)
                s = np.linspace(0, 1, NUM_OF_RADIAL_POINTS)[
                    get_label_radial_indices(radial)
                ]

                #  Do not use axis values in case of knonw null values
                first_index = 0
                if label in NULL_ON_AXIS_LABELS:
                    first_index = 1

                for sample, pol, tor in [
                    (s, p, t)
                    for s in range(len(df))
                    for p in range(full.shape[2])
                    for t in range(full.shape[3])
                ]:
                    f = interp1d(
                        s[first_index:],
                        scaled[sample, first_index:, pol, tor],
                        kind="cubic",
                        fill_value="extrapolate",
                    )
                    scaled_interp[sample, :, pol, tor] = f(
                        np.linspace(0, 1, NUM_OF_RADIAL_POINTS)
                    )

                #  Fix null values on axis
                if label in NULL_ON_AXIS_LABELS:
                    scaled_interp[:, 0, :, :] = 0

            if plot_profile:
                sample = 0
                poloidal_point = 0
                toroidal_point = 0
                _plot_profile(
                    full[sample, :, poloidal_point, toroidal_point],
                    scaled_interp[sample, :, poloidal_point, toroidal_point],
                    scaled[sample, :, poloidal_point, toroidal_point],
                    s,
                    label,
                )

            if args.interpolate:
                scaled = scaled_interp
            else:
                full = full[:, get_label_radial_indices(radial), :, :]

            assert full.shape == scaled.shape

            evaluations.append(
                {
                    "label": label,
                    "index": fourier_modes.index(modes),
                    "modes": r"$({} \times {})$".format(modes[0], modes[1]),
                    "radial": str(radial),
                    "grid": f"{grid[0]} x {grid[1]}",
                    "{}".format(args.metric): get_metric(args.metric)(
                        scaled.reshape(len(df), -1), full.reshape(len(df), -1)
                    ),
                }
            )

    #  Save evaluations data and figure
    evaluations_df = pd.DataFrame(evaluations)

    file_name = "{}-resolution-scan-{}-{}".format(
        args.output.lower(), args.metric, args.num_samples
    )
    evaluations_df.to_csv(join("reports", "examples", file_name + ".csv"), index=False)

    #  Plot normalized deviation
    fig, ax = plt.subplots(figsize=(5, 5))
    sns.scatterplot(
        data=evaluations_df,
        x="modes" if args.output != "iota" else "radial",
        y=args.metric,
        hue="radial" if args.output != "iota" else None,
        size="grid" if args.output != "iota" else None,
        style="label" if args.output != "iota" else None,
        ax=ax,
    )

    ax.set_ylim([1e-6, 1e-2] if args.output != "iota" else [1e-6, 1e-2])
    ax.set_yscale("log")
    ax.set_ylabel(args.metric)
    ax.set_xlabel(
        "Number of poloidal and toroidal modes"
        if args.output != "iota"
        else "Number of radial points"
    )

    #  Clean figure and save it
    fig.tight_layout()
    fig.savefig(join("figures", "examples", "{}.pdf".format(file_name)))
    try:
        tikzplotlib.clean_figure()
        tikzplotlib.save(join("figures", "examples", "{}.pgf".format(file_name)))
    except (ValueError, NotImplementedError) as e:
        print(e)


if __name__ == "__main__":
    main()
