"""Short utility script to compute coil currents absolute values.

Values taken from:
Andreeva T. 2002 Vacuum magnetic configurations of Wendelstein 7-X IPP-Report IPP III/270
"""

import argparse
import numpy as np

import w7x


def get_magnetic_configurations():
    """Return dict describing magnetic configuration.

    Absolute values are returned in A.
    """

    return {
        "standard": 1.45e6 * np.array(w7x.config.Defaults.MagneticConfig.standard_rw),
        "low_iota": 1.32e6 * np.array(w7x.config.Defaults.MagneticConfig.low_iota_rw),
        "high_iota": 1.6e6 * np.array(w7x.config.Defaults.MagneticConfig.high_iota_rw),
        "low_mirror": 1.49e6
        * np.array(w7x.config.Defaults.MagneticConfig.low_mirror_rw),
        "high_mirror": 1.44e6
        * np.array(w7x.config.Defaults.MagneticConfig.high_mirror_rw),
        "low_shear": 1.47e6 * np.array(w7x.config.Defaults.MagneticConfig.low_shear_rw),
        "inward_shifted": 1.47e6
        * np.array(w7x.config.Defaults.MagneticConfig.inward_shifted_rw),
        "outward_shifted": 1.46e6
        * np.array(w7x.config.Defaults.MagneticConfig.outward_shifted_rw),
        "limiter": 1.43e6
        * np.array(w7x.config.Defaults.MagneticConfig.limiter_case_rw),
    }


def main():
    """Main function for the script."""

    parser = argparse.ArgumentParser(description="Coil currents utility script")
    parser.add_argument(
        "--margin",
        default=0.05,
        help="Margin (in %) for coil currents boundaries",
        type=float,
    )

    args = parser.parse_args()

    print("Running coil currents utiliy script with {} margin...".format(args.margin))

    values = np.array(list(get_magnetic_configurations().values()))

    print("coil \t min \t\t max")
    for i in range(values.shape[0]):
        print(
            "{} \t {:.3e} \t {:.3e}".format(
                i,
                values[:, i].min() * (1 - args.margin)
                if (values[:, i].min() > 0)
                else values[:, i].min() * (1 + args.margin),
                values[:, i].max() * (1 + args.margin)
                if (values[:, i].max() > 0)
                else values[:, i].max() * (1 - args.margin),
            )
        )


if __name__ == "__main__":
    main()
